#!/bin/sh

if [ `ls raw/iso >/dev/null 2>&1 && echo $?` -gt 0 ] || [ `ls raw/file.iso >/dev/null 2>&1 && echo $?` -gt 0 ]
then
	echo "Please place an original Sonic CD V0.02 image at"
	echo "raw/file.iso"
	echo "and the file contents thereof at"
	echo "raw/iso/"
fi

mkdir -p data/cdhead data/pcm data/logo data/title data/ssel
mkdir -p data/warp data/come data/cdfiles data/open
mkdir -p data/attack data/levels/palettes

cp raw/iso/ABS.TXT data/cdfiles/
cp raw/iso/BIB.TXT data/cdfiles/
cp raw/iso/CPY.TXT data/cdfiles/
cp raw/iso/OPN.STM data/cdfiles/

dd if=raw/file.iso       bs=1 skip=0     count=10199 >raw/cdhead.bin

dd if=raw/cdhead.bin     bs=1 skip=558   count=294   >data/cdhead/sec.bin

dd if=raw/iso/PCM000.BIN bs=1 skip=4056  count=192   >data/pcm/data_18fd8.bin
dd if=raw/iso/PCM000.BIN bs=1 skip=4888  count=21330 >data/pcm/data_19318.bin

dd if=raw/iso/LOGO_M.MMD bs=1 skip=780   count=32    >data/logo/logo_palette.bin
dd if=raw/iso/LOGO_M.MMD bs=1 skip=832   count=1628  >data/logo/logo_nemtiles.bin
dd if=raw/iso/LOGO_M.MMD bs=1 skip=2460  count=352   >data/logo/logo_maps.bin
dd if=raw/iso/LOGO_M.MMD bs=1 skip=2812  count=2792  >data/logo/logo_z80base.bin
dd if=raw/iso/LOGO_M.MMD bs=1 skip=5604  count=1944  >data/logo/logo_z80dat0.bin
dd if=raw/iso/LOGO_M.MMD bs=1 skip=7548  count=56    >data/logo/logo_z80dat1.bin

dd if=raw/iso/TITLE_.MMD bs=1 skip=1238  count=102   >data/title/data_obj_idx_6.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=1472  count=52    >data/title/data_obj_idx_2.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=1598  count=82    >data/title/data_obj_idx_7.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=1768  count=22    >data/title/data_obj_idx_8.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=1894  count=16    >data/title/data_obj_idx_4.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=1998  count=16    >data/title/data_obj_idx_5.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=2088  count=46    >data/title/data_obj_idx_3.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=2208  count=26    >data/title/data_obj_idx_1c0.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=2506  count=128   >data/title/title_palette.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=2634  count=2240  >data/title/title_map_blk0.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=4874  count=2240  >data/title/title_map_blk3.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=7114  count=384   >data/title/title_map_blk1.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=7498  count=384   >data/title/title_map_blk2.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=7882  count=342   >data/title/title_nem_blk1.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=8224  count=12788 >data/title/title_nem_blk0.bin
dd if=raw/iso/TITLE_.MMD bs=1 skip=21012 count=322   >data/title/title_nem_blk2.bin

dd if=raw/iso/OPEN_M.MMD bs=1 skip=1958  count=64    >data/open/open_palette_0.bin
dd if=raw/iso/OPEN_M.MMD bs=1 skip=2022  count=64    >data/open/open_palette_1.bin
dd if=raw/iso/OPEN_M.MMD bs=1 skip=2480  count=7168  >data/open/border_tiles.bin
dd if=raw/iso/OPEN_M.MMD bs=1 skip=9648  count=2240  >data/open/border_mapping.bin
dd if=raw/iso/OPEN_M.MMD bs=1 skip=11888 count=32    >data/open/border_palette.bin

dd if=raw/iso/SSEL__.MMD bs=1 skip=686   count=32    >data/ssel/ssel_palette.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=918   count=6     >data/ssel/data_ff2296.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=924   count=6     >data/ssel/data_ff229c.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=930   count=6     >data/ssel/data_ff22a2.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=936   count=6     >data/ssel/data_ff22a8.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=942   count=6     >data/ssel/data_ff22ae.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=948   count=6     >data/ssel/data_ff22b4.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=954   count=6     >data/ssel/data_ff22ba.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=960   count=6     >data/ssel/data_ff22c0.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=966   count=5     >data/ssel/data_ff22c6.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=971   count=8     >data/ssel/data_ff22cb.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=979   count=13    >data/ssel/data_ff22d3.bin
dd if=raw/iso/SSEL__.MMD bs=1 skip=1462  count=1354  >data/ssel/ssel_nemtiles.bin

dd if=raw/iso/WARP__.MMD bs=1 skip=512   count=12    >data/warp/pal_cycle0.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=524   count=12    >data/warp/pal_cycle1.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=536   count=12    >data/warp/pal_cycle2.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=628   count=224   >data/warp/warp_rand_tiles.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=900   count=16    >data/warp/warp_rand_maps.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1650  count=16    >data/warp/warp_obj0_data0.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1666  count=12    >data/warp/warp_obj0_data1.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1678  count=12    >data/warp/warp_obj0_data2.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1690  count=12    >data/warp/warp_obj0_data3.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1702  count=12    >data/warp/warp_obj0_data4.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1860  count=6     >data/warp/warp_obj1_data0.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1866  count=6     >data/warp/warp_obj1_data1.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1872  count=6     >data/warp/warp_obj1_data2.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=1898  count=128   >data/warp/warp_palette.bin
dd if=raw/iso/WARP__.MMD bs=1 skip=2026  count=1696  >data/warp/warp_tiles.bin

dd if=raw/iso/COME__.MMD bs=1 skip=696   count=32    >data/come/come_palette.bin
dd if=raw/iso/COME__.MMD bs=1 skip=728   count=1284  >data/come/come_nemtiles.bin
dd if=raw/iso/COME__.MMD bs=1 skip=2012  count=1792  >data/come/come_map0.bin
dd if=raw/iso/COME__.MMD bs=1 skip=3804  count=1792  >data/come/come_map1.bin
dd if=raw/iso/COME__.MMD bs=1 skip=5596  count=1792  >data/come/come_map2.bin

dd if=raw/iso/ATTACK.MMD bs=1 skip=1382  count=128   >data/attack/attack_palette.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=1962  count=8     >data/attack/data_ff26aa.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=1970  count=9     >data/attack/data_ff26b2.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=1979  count=9     >data/attack/data_ff26bb.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=1988  count=9     >data/attack/data_ff26c4.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=1997  count=9     >data/attack/data_ff26cd.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2006  count=9     >data/attack/data_ff26d6.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2015  count=9     >data/attack/data_ff26df.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2024  count=9     >data/attack/data_ff26e8.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2033  count=9     >data/attack/data_ff26f1.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2042  count=9     >data/attack/data_ff26fa.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2051  count=9     >data/attack/data_ff2703.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2060  count=9     >data/attack/data_ff270c.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2069  count=9     >data/attack/data_ff2715.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2078  count=9     >data/attack/data_ff271e.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2087  count=9     >data/attack/data_ff2727.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2096  count=9     >data/attack/data_ff2730.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2105  count=9     >data/attack/data_ff2739.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2114  count=9     >data/attack/data_ff2742.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2123  count=9     >data/attack/data_ff274b.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2132  count=9     >data/attack/data_ff2754.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2141  count=9     >data/attack/data_ff275d.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2150  count=9     >data/attack/data_ff2766.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2159  count=9     >data/attack/data_ff276f.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2168  count=9     >data/attack/data_ff2778.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2177  count=9     >data/attack/data_ff2781.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=2186  count=1354  >data/attack/nem_ff278a.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=3540  count=3914  >data/attack/nem_ff2cd4.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=7454  count=136   >data/attack/map_ff3c1e.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=7590  count=640   >data/attack/map_ff3ca6.bin
dd if=raw/iso/ATTACK.MMD bs=1 skip=8230  count=640   >data/attack/map_ff3f26.bin

dd if=raw/iso/R11A__.MMD bs=1 skip=496   count=24    >data/levels/palettes/pal_2001f0.pres.bin
dd if=raw/iso/R11A__.MMD bs=1 skip=520   count=24    >data/levels/palettes/pal_200208.pres.bin

dd if=raw/iso/R11B__.MMD bs=1 skip=496   count=24    >data/levels/palettes/pal_2001f0.past.bin
dd if=raw/iso/R11B__.MMD bs=1 skip=520   count=24    >data/levels/palettes/pal_200208.past.bin

dd if=raw/iso/R11D__.MMD bs=1 skip=578   count=24    >data/levels/palettes/pal_200242.f_bad.bin
dd if=raw/iso/R11D__.MMD bs=1 skip=602   count=24    >data/levels/palettes/pal_20025a.f_bad.bin
