# SCD Disasm
This is a disassembly of Sonic the Hedgehog CD for the Sega Mega CD.  The initial pass is targeting the December 4th, 1992 V0.02 prototype due to the simplicity and general lack of content.  This should serve as a springboard then into disassembling later prototypes and the final release.

## Rationale
Sonic CD is made up of several different files contained in a CD image.  A bootstrap program on the CD is responsible for swapping these files in and out of memory from disc as they are needed.  The levels in particular are spread out one file per time period per act.  This results in numerous copies of the game's engine code duplicated throughout the disc, complicating efforts to modify central engine components universally.

This disassembly attempts to address this matter by extracting common library components and reused functions into centralized files which are then linked into each of the expected disc files.  This is likely how the initial development proceeded, or at least I should hope they weren't modifying multiple copies of the same libraries.

## Contributing
All contributions are welcome.  I know there have been other efforts in the past, including my own regarding the May 10th, 1993 protoype in years past, and would gladly take contributions that minimize the amount of retreading already covered ground.  With that goal in mind, the engine components which mirror those in the Mega Drive Sonic the Hedgehog games will likely be documented and laid out similarly to their counterparts.  This should facilitate movement between this disassembly and the existing community disassemblies of the other titles.

## Data
There is a script in the root, extract.sh, which will extract the necessary binary pieces from their built equivalents.  This tool will only work with a clean copy of the V0.02 Sonic CD build for now, but as other versions are explored, switches could be added to request extraction of a specific version's assets.  As of this typing, this script has holes in it, as I started keeping it after having already processed a few files manually, so I need to go back and fill in missing addresses.  As such, it does not immediately run at present.  Additionally, this script requires the UNIX dd utility, so consult with MinGW, Cygwin, or another UNIX-like environment on Windows.

Any missing complete files from the title must be provided from an owned copy, as the extraction script does not extract the ISO, just pieces from the files.

Be sure to chmod u+w any files from the CD.  When copied around, preserved
read-only characteristics will cause build failures on repeat copying of these files.

## Organization
Each file is present as a separate source file with an included linker script to lay out the image as it is in the release.  No claims are made that this is how the original project would've been organized, this is simply a means to produce a 1:1 equivalent set of binary objects which can then be assembled into an identical ISO.  That said, some decisions, such as where to load a particular binary or the order and layout of files on the disc, are subjective, and need not be 100% in line with the original release to operate.  For instance, the build currently simply squashes all of the necessary files into an arbitrary ISO and then writes in the bootloader.  The ISO filesystem table itself bears little resemblance to the original, but this ISO will run as expected in an emulator, indicating there is hopefully nothing implementation-specific or standards-violating to worry about in Sega's ISO9660 implementation.