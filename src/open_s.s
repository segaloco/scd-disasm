/* ------------------------------------------------------- */
/* open_s - the sub CPU portion of the opening FMV         */
/* ------------------------------------------------------- */
	.include "src/inc/s_subreg.i"

	.include "src/inc/s_bios.i"

	.include "src/inc/os.i"
	.include "src/inc/pcm.i"
	.include "src/inc/files.i"

	.text
/* ------------------------------------------------------- */
sub_5F16	= 0x5F16
	BIOS_ENTRY_entry_03	= 0x03
	BIOS_ENTRY_entry_89	= 0x89
long_5F78	= 0x5F78
/* ------------------------ */
tbl_8C00	= 0x8C00

byte_FF0011	= 0xFF0011
/* ------------------------------------------------------- */
	.globl	open_s
open_s:
	bsrw	open_set_1m
	bsrw	open_wait_std

	movel	#open_do_os_vint, long_5F78

	movew	#FTABLE_ENTRY_OPNSTM, %d0
	jsr	OS_LOADFILE_IDX_ADDR

	movew	#OS_ENTRY_BLOCK_PREP, %d0
	jsr	OS_ENTRY_ADDR

	bcsw	9f /* if (!os_entry(OS_ENTRY_BLOCK_PREP)) { */
		movew	#OS_ENTRY_BLOCK_LOAD, %d0
		jsr	OS_ENTRY_ADDR
		1: /* for (os_entry(OS_ENTRY_BLOCK_LOAD); !dnma_err && !cdc_wait; os_entry(cdc_wait)) { */
			movew	#OS_ENTRY_CDC_WAIT, %d0
			jsr	OS_ENTRY_ADDR

			movew	%sr, %d7
			btst	#E_FLG_DMNA_ERR, (SUBCPU_E_FLG_S).l
			bnew	1f
				lea	tbl_8C00, %a5
				movew	%a5@(8), %d3
				cmpiw	#74, %d3
				blts	2f /* if (tbl_8C00[8] >= 0x74) { */
					bclr	#SCOM_FLAG_BUSY_BIT, (SUBCPU_SCOM_FLG_S).l
				2: /* } */

				movew	%d7, %ccr
				bcsw	1b
		1: /* } */

	open_s_finally:
		movew	#OS_ENTRY_UNWIND, %d0
		jsr	OS_ENTRY_ADDR

		bset	#SCOM_FLAG_ERR_BIT, (SUBCPU_SCOM_FLG_S).l
		1: /* while (!m_done) { */
			btst	#E_FLG_M_DONE, (SUBCPU_E_FLG_S).l
			beqs	1b
		/* } */
		bclr	#SCOM_FLAG_ERR_BIT, (SUBCPU_SCOM_FLG_S).l

		moveb	#0xFF, byte_FF0011

		movew	#BIOS_ENTRY_entry_89, %d0
		jsr	BIOS_ENTRY

		movew	#OS_ENTRY_UNWIND, %d0
		jsr	OS_ENTRY_ADDR

		movew	#BIOS_ENTRY_entry_03, %d0
		jsr	BIOS_ENTRY

		clrb	(SUBCPU_SCOM_FLG_S).l
		rts
	9: /* } else { */
		nop
		bsrw	open_s_finally
	/* } */
/* ------------------------------------------------------- */
open_set_1m:
	bset	#SUBCPU_MODE_1M_BIT, (SUBCPU_MEMMODE_S).l
	1: /* while (!(SUBCPU_MODE_1M_BIT)) { */
		btst	#SUBCPU_MODE_1M_BIT, (SUBCPU_MEMMODE_S).l
		beqs	1b
	/* } */

	moveb	#0, (SUBCPU_SCOM_FLG_S).l
	rts
/* ------------------------ */
open_wait_std:
	1: /* while (!(E_FLG_STD)) { */
		btst	#E_FLG_STD, (SUBCPU_E_FLG_S).l
		beqs	1b
	/* } */

	rts
/* ------------------------------------------------------- */
open_wait_dmna:
	eorib	#SUBCPU_RET, (SUBCPU_MEMMODE_S).l
	1: /* while (SUBCPU_DMNA_BIT) { */
		btst	#SUBCPU_DMNA_BIT, (SUBCPU_MEMMODE_S).l
		bnes	1b
	/* } */

	rts
/* ------------------------------------------------------- */
tbl_FF0000	= 0xFF0000

byte_FF000F	= 0xFF000F
byte_FF0025	= 0xFF0025
byte_FF0027	= 0xFF0027

tbl_FF2001	= 0xFF2001

byte_FF3FFF	= 0xFF3FFF

sub_300ec:
	moveb	#0xFF, byte_FF0011
	moveq	#0, %d4
	1: /* for (k = 0; k < 4; k++) { */
		moveq	#0, %d0
		2: /* for (j = 0; j < 4; j++) { */
			movew	%d4, %d1
			lslw	#2, %d1
			addw	%d0, %d1
			addiw	#128, %d1
			moveb	%d1, byte_FF000F

			movew	#4095, %d2
			lea	tbl_FF2001, %fp
			3: /* for (i = 4095; i > 0; i--) { */
				moveb	#0, %fp@
				addqw	#2, %fp
				dbf	%d2, 3b
			/* } */

			addqw	#1, %d0
			cmpiw	#4, %d0
			blts	2b
		/* } */

		lea	tbl_FF0000, %fp
		moveb	#0xFF, %fp@(byte_FF3FFF-tbl_FF0000)
		addqw	#1, %d4
		cmpiw	#4, %d4
		blts	1b
	/* } */

	rts
/* ------------------------------------------------------- */
sub_3013a:
	movemw	%d5-%d7, %sp@-

	moveb	#0xFC, %fp@(17)
	moveq	#0, %d5
	lea	(tbl_3018a).l, %a5
	2: /* for (j = 0; j < 2; j++) { */
		lea	tbl_FF0000, %fp
		movew	%d5, %d6
		addiw	#192, %d6
		moveb	%d6, %fp@(15)
		addqw	#1, %fp

		moveq	#6, %d7
		1: /* for (i = 6; i > 0; i--) { */
			moveb	%a5@+, %fp@
			addqw	#2, %fp
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			nop
			dbf	%d7, 1b
		/* } */

		addqw	#1, %d5
		cmpiw	#2, %d5
		addqw	#1, %a5
		blts	2b
	/* } */

	movemw	%sp@+, %d5-%d7
	rts
/* ------------------------ */
tbl_3018a:
	.byte	0xFF, 0xF0, 0xEE, 0x03
	.byte	0x00, 0x80, 0x00, 0x00
	.byte	0xFF, 0x0F, 0xEE, 0x03
	.byte	0x00, 0xC0, 0x40, 0x00
/* ---------------------------------------------------------- */
sub_3019a:
	btst	#4, %a5@(6659)
	bnes	2f /* if () { */
		1: /* while () { */
			btst	#E_FLG_DMNA_ERR, (SUBCPU_E_FLG_S).l
			bnew	open_s_finally
			movew	#OS_ENTRY_CDC_WAIT, %d0
			jsr	OS_ENTRY_ADDR
			bccw	open_s_finally

			cmpib	#127, byte_FF0027
			bmis	1b
		/* } */
		bras	3f
	2: /* } else { */
		1: /* while () { */
			btst	#E_FLG_DMNA_ERR, (SUBCPU_E_FLG_S).l
			bnew	open_s_finally
			movew	#OS_ENTRY_CDC_WAIT, %d0
			jsr	OS_ENTRY_ADDR
			bccw	open_s_finally

			cmpib	#0xC0, byte_FF0027
			bpls	1b
		/* } */
	3: /* } */
	/* while () { */
		btst	#E_FLG_DMNA_ERR, (SUBCPU_E_FLG_S).l
		bnew	open_s_finally
		movew	#OS_ENTRY_CDC_WAIT, %d0
		jsr	OS_ENTRY_ADDR
		bccw	open_s_finally

		cmpib	#0xF0, byte_FF0025
		bpls	3b
	/* } */

	rts
/* ------------------------------------------------------- */
open_do_os_vint:
	moveml	%d0-%fp, %sp@-

	movew	#OS_ENTRY_VINT, %d0
	jsr	OS_ENTRY_ADDR

 	moveml	%sp@+, %d0-%fp
	rts
