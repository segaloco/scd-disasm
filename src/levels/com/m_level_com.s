	.ifndef	M_LEVEL_COM
	.equ	M_LEVEL_COM, 1

	.include "src/inc/m_map.i"

	.include "src/inc/misc.i"

/* ------------------------------------------------------- */
	.macro	m_level_start

	btst	#6, 0xa1000d
	beqs	1f
	cmpil	#INIT_STRING, m_level_l_FF13FC
	beqw	3f 
  	1: /* if (!(0xa1000d & 0x40) || m_level_l_FF13FC != 'init') { */
		lea	mmd_level_buffer, %fp
		moveq	#0, %d7
		movew	#((mmd_level_buffer_end-mmd_level_buffer)/LONG_SIZE)-1, %d6
		2: /* for (long of buffer) { */
			movel	%d7, %fp@+

			dbf	%d6, 2b
		/* } */
		
		moveb	0xa10001, %d0
		andib	#0xC0, %d0
		moveb	%d0, m_level_b_FF13F8
		movel	#INIT_STRING, m_level_l_FF13FC
	3: /* } */

	.endm
/* ------------------------------------------------------- */
	.macro	m_level_pal_init	pal_addr
	lea	(\pal_addr).l, %a0
	subqb	#1, m_mmd_fff600+0x5C
	bpls	1f /* if () { */
		moveb	#7, m_mmd_fff600+0x5C

		moveq	#0, %d0
		moveb	m_mmd_fff600+0x32, %d0
		cmpib	#2, %d0
		bnes	2f /* if () { */
			moveq	#0, %d0
			bras	3f
		2: /* } else { */
			addqb	#1, %d0
		3: /* } */
			
		moveb	%d0, m_mmd_fff600+0x32
		lslw	#3, %d0
		lea	SVAR_P_fffb6a, %a1
		movel	%a0@(0, %d0:w), %a1@+
		movel	%a0@(4, %d0:w), %a1@
	1: /* } */

	addaw	#0x18, %a0
	subqb	#1, m_mmd_fff600+0x5D
	bpls	1f /* if () { */
		moveb	#5, m_mmd_fff600+0x5D

		moveq	#0, %d0
		moveb	m_mmd_fff600+0x33, %d0
		cmpib	#2, %d0
		bnes	2f /* if () { */
			moveq	#0, %d0
			bras	3f
		2: /* } else { */
			addqb	#1, %d0
		3: /* } */

		moveb	%d0, m_mmd_fff600+0x33
		andiw	#3, %d0
		lslw	#3, %d0
		lea	SVAR_P_fffb58, %a1
		movel	%a0@(0, %d0:w), %a1@+
		movel	%a0@(4, %d0:w), %a1@
	1: /* } */

	.endm
/* ------------------------------------------------------- */
	.macro	m_sub_2001F0	arg0

	lea	m_mmd_fff600+0x5E, %a5
	lea	m_mmd_fff600+0x34, %a4
	lea	(\arg0).l, %a1
	lea	(\arg0).l, %a2
	subqb	#1, %a5@
	bpls	9f /* if () { */
		moveq	#0, %d0
		moveb	%a1@+, %d0
		moveb	%a1@+, %d1
		addw	%d0, %d0
		lea	SVAR_P_fffb00, %a3
		lea	%a3@(0, %d0:w), %a3
		moveq	#0, %d0
		moveb	%a4@, %d0
		addqb	#1, %d0
		cmpb	%d1, %d0
		bcss	1f /* if () { */
			moveq	#0, %d0
		1: /* } */

		moveb	%d0, %a4@
		addw	%d0, %d0
		moveb	%a1@(0, %d0:w), %a5@
		moveb	%a1@(1, %d0:w), %d0
		extw	%d0
		addw	%d0, %d0
		movew	%a2@(0, %d0:w), %a3@
	9: /* } */

	addaw	#1, %a4
	addaw	#1, %a5

	.endm
/* ------------------------------------------------------- */

	.endif	/* M_LEVEL_COM */
