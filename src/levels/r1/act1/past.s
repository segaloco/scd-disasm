	.include "src/inc/m_map.i"

	.include "src/inc/misc.i"
	
	.include "src/levels/com/m_level_com.s"

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	0
	.word	0
	.long	r11b_vec
	.long	r11b_hint_vec
	.long	r11b_vint_vec
	.align	256, 0

	.section .text
/* ------------------------------------------------------- */
r11b_vec:
	jmp	(vec_20011E).l
	jmp	(main_halt).l

r11b_hint_vec:
	jmp	(vec_201BC2).l

r11b_vint_vec:
	jmp	(vec_20164C).l
/* ------------------------------------------------------- */
	.include "src/common/main_halt.s"
/* ------------------------------------------------------- */
vec_20011E:
	m_level_start

	bsrw	sub_201CD4
	bsrw	sub_201C7C

	moveb	#0, m_mmd_fff600
	moveb	m_mmd_fff600, %d0
	andiw	#0x1C, %d0
	jmp	%pc@(loc_20017C, %d0:w)

loc_20017C:
	braw	loc_2011DC
/* ------------------------------------------------------- */
sub_200180:
	m_level_pal_init	pal_2001F0
	rts
/* ------------------------ */
pal_2001F0:
	.incbin	"data/levels/palettes/pal_2001f0.past.bin"
pal_200208:
	.incbin	"data/levels/palettes/pal_200208.past.bin"
/* ------------------------------------------------------- */
sub_200220:
	.space	0xFBC
/* ------------------------------------------------------- */
loc_2011DC:
	.space	0x470
/* ------------------------------------------------------- */
vec_20164C:
	.space	0x576
/* ------------------------------------------------------- */
vec_201BC2:
	.space	0xBA
/* ------------------------------------------------------- */
sub_201C7C:
	.space	0x58
/* ------------------------------------------------------- */
sub_201CD4:
	.space 0x3E32C
/* ------------------------------------------------------- */
