	.include "src/inc/m_map.i"

	.include "src/inc/misc.i"

	.include "src/levels/com/m_level_com.s"

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	0
	.word	0
	.long	r12d_vec
	.long	r12d_hint_vec
	.long	r12d_vint_vec
	.align	256, 0

	.section .text
/* ------------------------------------------------------- */
r12d_vec:
	jmp	(vec_20011E).l
	jmp	(main_halt).l

r12d_hint_vec:
	jmp	(vec_201C14).l

r12d_vint_vec:
	jmp	(vec_20169E).l
/* ------------------------------------------------------- */
	.include "src/common/main_halt.s"
/* ------------------------------------------------------- */
vec_20011E:
	m_level_start

	bsrw	sub_201D26
	bsrw	sub_201CCE

	moveb	#0, m_mmd_fff600
	moveb	m_mmd_fff600, %d0
	andiw	#0x1C, %d0
	jmp	%pc@(loc_20017C, %d0:w)

loc_20017C:
	braw	loc_20122E

/* ------------------------------------------------------- */
sub_200180:
	m_level_pal_init	pal_200242
	rts

sub_2001F0:
	m_sub_2001F0	sub_200272
	rts
/* ------------------------ */
pal_200242:
	.incbin	"data/levels/palettes/pal_200242.f_bad.bin"
pal_20025A:
	.incbin	"data/levels/palettes/pal_20025a.f_bad.bin"
/* ------------------------------------------------------- */
sub_200272:
	.space	0xFBC
/* ------------------------------------------------------- */
loc_20122E:
	.space	0x470
/* ------------------------------------------------------- */
vec_20169E:
	.space	0x576
/* ------------------------------------------------------- */
vec_201C14:
	.space	0xBA
/* ------------------------------------------------------- */
sub_201CCE:
	.space	0x58
/* ------------------------------------------------------- */
sub_201D26:
	.space	0x3E2DA
