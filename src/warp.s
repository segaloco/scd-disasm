/* ------------------------------------------------------- */
/* warp - time warp between time periods                   */
/*  _zero_index is used to allow %ax@(0) to not be         */
/*  optimized to %ax@                                      */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"
	.include "src/inc/z80.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/m_map.i"
	.include "src/inc/obj_eng.i"
	.include "src/inc/misc.i"

	.include "src/common/m_vint_bits.s"
	.include "src/common/m_obj_eng.s"

vdp_addr_0020	= 0x0020
vdp_addr_2000	= 0x2000
vdp_addr_E000	= 0xE000
vdp_addr_F000	= 0xF000
vdp_addr_F400	= 0xF400

vdp_vsaddr_0002	= 0x0002

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	warp
	.word	(mmd_warp_size/4)-1
	.long	warp
	.long	0
	.long	0
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib
/* ------------------------------------------------------- */
	.equ	TIMER_LENGTH, 0xf0
warp:
	movel	#warp_vint, M_VECTOR_VINT

	lea	mmd_ram_arena_l, %a0
	movew	#mmd_ram_arena_size_l-1, %d7
	1: /* for (long of mmd_ram_arena_l) { */
		movel	#0, %a0@+
		dbf	%d7, 1b
	/* } */

	lea	%pc@(warp_init_vdpreg), %a0
	bsrw	lib_sys_init

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_F400&0x3FFF)<<16)+((vdp_addr_F400>>14)&0x3), VDP_ADDR
	movel	#0, VDP_DATA
	bsrw	warp_load_rand_tiles
	bsrw	warp_load_rand_maps

	lea	%pc@(warp_tiles), %a1
	lea	VDP_DATA, %a2
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_2000&0x3FFF)<<16)+((vdp_addr_2000>>14)&0x3), VDP_ADDR
	movew	#((warp_tiles_end-warp_tiles)/LONG_SIZE)-1, %d7
	1: /* for (long of warp_tiles) { */
		movel	%a1@+, %a2@

		dbf	%d7, 1b
	/* } */

	lea	%pc@(warp_palette), %a0
	lea	m_mmd_palette, %a1
	moveq	#(((warp_palette_end-warp_palette)/VDP_PAL_ENTRY_SIZE)/(LONG_SIZE/VDP_PAL_ENTRY_SIZE))-1, %d7
	1: /* for (long of warp_palette) { */
		movel	%a0@+, %a1@+
		dbf	%d7, 1b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	bsrw	main_wait_int

	movew	#TIMER_LENGTH, m_timer_l
	bsrw	lib_int_disable
	moveb	#0xa9, z80addr_1c0a
	bsrw	lib_int_enable
	1: /* for (m_timer_l = TIMER_LENGTH; m_timer_l > 0; m_timer_l_cycle) { */
		bsrw	warp_pal_cycle
		bsrw	sub_ff2124
		bsrw	warp_obj_engine
		bsrw	main_wait_int

		tstw	m_timer_l
		bnes	1b
	/* } */
	bsrw	lib_int_disable
	moveb	#1, z80addr_1c3c
	bsrw	lib_int_enable

	rts
/* ------------------------ */
warp_pal_cycle:
	movew	(pal_cycle_idx).l, %d0
	movew	%pc@(pal_cycle_tbl, %d0:w), %d1
	cmpw	m_timer_l, %d1
	bnes	9f /* if (m_timer_l == pal_cycle_tbl[d0]) { */
		addqw	#4, (pal_cycle_idx).l
		movew	%pc@(pal_cycle_tbl+2, %d0:w), %d0
		lea	%pc@(pal_cycle_data,  %d0:w), %a1
		movel	%a1@+, m_mmd_palette+0x30
		movel	%a1@+, m_mmd_palette+0x34
		movew	%a1@+, m_mmd_palette+0x38
		movew	%a1@+, m_mmd_palette+0x6a
		bset	#m_vint_palfade_bit, m_vint_call_mask
	9: /* } */

	rts
/* ------------------------ */
pal_cycle_idx:
	.word	0

pal_cycle_tbl:
	.word	0xe6, pal_cycle1-pal_cycle_data
	.word	0xdc, pal_cycle2-pal_cycle_data
	.word	0x14, pal_cycle1-pal_cycle_data
	.word	0x0a, pal_cycle0-pal_cycle_data

pal_cycle_data:
pal_cycle0:
	.incbin	"data/warp/pal_cycle0.bin"
pal_cycle1:
	.incbin	"data/warp/pal_cycle1.bin"
pal_cycle2:
	.incbin	"data/warp/pal_cycle2.bin"
/* ------------------------------------------------------- */
sub_ff2124:
	movew	SVAR_W_fffa44_l, %d0
	andiw	#3, %d0
	bnes	9f /* if (!(SVAR_W_fffa44_l & 0x0003)) { */
		movew	(data_ff2152).l, %d0
		addqw	#1, (data_ff2152).l
		andiw	#3, (data_ff2152).l
		muluw	#0x40, %d0
		lea	m_obj_mem, %a0
		movew	#1, %a0@(0, %d0:w)
	9: /* } */

	rts
/* ------------- */
data_ff2152:
	.word	0
/* ------------------------------------------------------- */
warp_load_rand_tiles:
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_0020&0x3FFF)<<16)+((vdp_addr_0020>>14)&0x3), VDP_ADDR
	lea	%pc@(warp_rand_tiles), %a0
	lea	VDP_DATA, %a1
	movew	#((warp_rand_tiles_end-warp_rand_tiles)/LONG_SIZE)-1, %d7
	1: /* for (long of warp_rand_tiles) { */
		movel	%a0@+, %a1@

		dbf	%d7, 1b
	/* } */

	rts
/* ------------- */
warp_rand_tiles:
	.incbin	"data/warp/warp_rand_tiles.bin"
warp_rand_tiles_end:
/* ------------------------------------------------------- */
WARP_RAND_MAP_SIZE	= (warp_rand_maps_end-warp_rand_maps)
WARP_RAND_OUTPUT_SIZE	= 0x800

warp_load_rand_maps:
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E000&0x3FFF)<<16)+((vdp_addr_E000>>14)&0x3), VDP_ADDR
	bsrs	1f
	bsrs	1f
	bsrs	1f
	nop

1:
	lea	%pc@(warp_rand_maps), %a1
	moveq	#((WARP_RAND_OUTPUT_SIZE/WARP_RAND_MAP_SIZE)/WORD_SIZE)-1, %d1
	moveq	#(WARP_RAND_MAP_SIZE/WORD_SIZE)-1, %d2
	2: /* for (map of warp_rand_maps) { */
		movew	%d1, %d3
		1: /* for (page of vdp_data) { */
			movew	%a1@, VDP_DATA
			dbf	%d3, 1b
		/* } */

		addaw	#2, %a1
		dbf	%d2, 2b
	/* } */

	rts
/* ------------- */
warp_rand_maps:
	.incbin	"data/warp/warp_rand_maps.bin"
warp_rand_maps_end:
/* ------------------------------------------------------- */
warp_vint:
	m_vint_open
	beqw	9f /* if (m_vint_open()) { */
		bsrw	lib_int_disable
		m_vint_vdpwait
		m_vint_dispon
		m_vint_palload

		movel	#(VDP_VSRAM_WRITE)+((vdp_vsaddr_0002&0x3FFF)<<16)+((vdp_vsaddr_0002>>14)&0x3), VDP_ADDR
		movew	(data_ff2364).l, VDP_DATA
		subqw	#8, (data_ff2364).l

		m_vdp_dma_v	vdp_addr_F000, SVAR_L_ffe000, 0x140

		m_vint_finish
		m_vint_tick
	9: /* } */

	m_vint_ret
/* ------------- */
data_ff2364:
	.word	0
/* ------------------------------------------------------- */
warp_obj_engine:
	movew	SVAR_W_fffa44_l, %d0
	andiw	#1, %d0
	bnes	1f /* if (!(SVAR_W_fffa44_l & 0x0001)) { */
		bsrw	warp_obj_init
		movew	#2, %a1@
	1: /* } */

	m_obj_eng_enter

	lea	m_obj_warp_mem, %a0
	moveq	#((m_obj_warp_mem_end-m_obj_warp_mem)/OBJ_SIZE)-1, %d7
	1: /* for (entry of object_table) { */
		movew	%d7, %sp@-
		bsrs	warp_obj_doref
		movew	%sp@+, %d7
		addaw	#OBJ_SIZE, %a0

		dbf	%d7, 1b
	/* } */

	lea	m_obj_mem+(0*OBJ_SIZE), %a0
	bsrw	warp_draw_basestar
	lea	m_obj_mem+(1*OBJ_SIZE), %a0
	bsrw	warp_draw_star
	lea	m_obj_mem+(2*OBJ_SIZE), %a0
	bsrw	warp_draw_star
	lea	m_obj_mem+(3*OBJ_SIZE), %a0
	bsrw	warp_draw_star

	m_obj_eng_exit

9:
	rts
/* ------------------------ */
warp_obj_doref:
	movew	%a0@, %d0
	beqs	9b

	jsr	(warp_obj1).l
	btst	#OBJ_STAT_CLEAR_BIT, %a0@(OBJ_STAT)
	beqs	warp_obj_refresh

	moveal	%a0, %a1
	moveq	#0, %d1
	moveq	#(OBJ_SIZE/LONG_SIZE)-1, %d2
	1: /* for (long of object) { */
		movel	%d1, %a1@+

		dbf	%d2, 1b
	/* } */

	rts
/* ------------------------ */
warp_obj_refresh:
	m_obj_stat_start
	bnes	9b

	m_obj_ani_start
	bmis	9b

	m_obj_ani_loop_start
	m_obj_ani_loop_main
	m_obj_ani_loop_end
	m_obj_ani_loop_next

9:
	m_obj_stat_ret
/* ------------------------ */
warp_obj_init:
	lea	m_obj_warp_mem, %a1
	moveq	#((m_obj_warp_mem_end-m_obj_warp_mem)/OBJ_SIZE)-1, %d7
	1: /* for (entry of object_table) { */
		tstw	%a1@
		beqs	9f

		addaw	#OBJ_SIZE, %a1
		dbf	%d7, 1b
	9: /* } */

	rts
/* ------------------------ */
warp_obj_ani_cycle:
	m_obj_ani_cycle
	rts
/* ------------------------ */
warp_draw_basestar:
	tstw	%a0@((_zero_index).w)
	beqs	9f /* if (obj_index != 0) { */
		bsrs	warp_obj0
		bsrw	warp_obj_refresh
	9: /* } */

	rts
/* ------------------------ */
warp_draw_star:
	tstw	%a0@((_zero_index).w)
	beqs	9f /* if (obj_index != 0) { */
		bsrs	warp_obj0
		movel	m_obj_mem+(OBJ_SIZE*0)+OBJ_ANI_FRAME, %a0@(OBJ_ANI_FRAME)
		bsrw	warp_obj_refresh
	9: /* } */

	rts
/* ------------------------------------------------------- */
warp_obj0:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(tbl_obj0, %d0:w), %d0
	jmp	%pc@(tbl_obj0, %d0:w)
/* ------------- */
tbl_obj0:
	.word	warp_obj0_rout0-tbl_obj0
	.word	warp_obj0_rout1-tbl_obj0
	.word	warp_obj0_rout2-tbl_obj0
	.word	warp_obj0_rout3-tbl_obj0
	.word	warp_obj0_rout4-tbl_obj0
/* ------------- */
warp_obj0_rout0:
	movew	#0x8100, %a0@(OBJ_W_OFF_4)
	movel	#warp_obj0_data, %a0@(OBJ_DATAPTR)
	movew	#0x120, %a0@(OBJ_HPOS)
	movew	#0x1a0, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(warp_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

warp_obj0_rout1:
	subqw	#8, %a0@(OBJ_VPOS)
	cmpiw	#0x108, %a0@(OBJ_VPOS)
	bnes	1f /* if ((obj_vpos -= 8) == 0x108) { */
		addqb	#1, %a0@(OBJ_ROUTINE)
		moveb	#0x78, %a0@(OBJ_COUNTER)
	1: /* } */

	rts

warp_obj0_rout2:
	subqb	#1, %a0@(OBJ_COUNTER)
	bnes	1f /* if (--obj_counter == 0) { */
		addqb	#1, %a0@(OBJ_ROUTINE)
	1: /* } */

	rts

warp_obj0_rout3:
	subqw	#8, %a0@(OBJ_VPOS)
	cmpiw	#0x70, %a0@(OBJ_VPOS)
	bnes	1f /* if ((obj_vpos -= 8) == 0x70) { */
		addqb	#1, %a0@(OBJ_ROUTINE)
	1: /* } */

warp_obj0_rout4:
	rts
/* ------------- */
warp_obj0_data:
	.long	warp_obj0_data_tbl

warp_obj0_data_tbl:
	.byte	(warp_obj0_data_tbl_end-warp_obj0_data_tbl_start)/LONG_SIZE
	.byte	2
warp_obj0_data_tbl_start:
	.long	loc_ff2572
	.long	loc_ff2582
	.long	loc_ff258e
	.long	loc_ff259a
	.long	loc_ff25a6
warp_obj0_data_tbl_end:

loc_ff2572:
	.incbin	"data/warp/warp_obj0_data0.bin"
loc_ff2582:
	.incbin	"data/warp/warp_obj0_data1.bin"
loc_ff258e:
	.incbin	"data/warp/warp_obj0_data2.bin"
loc_ff259a:
	.incbin	"data/warp/warp_obj0_data3.bin"
loc_ff25a6:
	.incbin	"data/warp/warp_obj0_data4.bin"
/* ------------------------------------------------------- */
warp_obj1:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(tbl_obj1, %d0:w), %d0
	jmp	%pc@(tbl_obj1, %d0:w)
/* ------------- */
tbl_obj1:
	.word	tbl_obj1_rout0-tbl_obj1
	.word	tbl_obj1_rout1-tbl_obj1
/* ------------- */
tbl_obj1_rout0:
	movew	#0x6000, %a0@(OBJ_W_OFF_4)
	movel	#warp_obj1_data, %a0@(OBJ_DATAPTR)
	moveq	#0, %d0
	jsr	%pc@(warp_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

	bsrw	lib_rand
	movew	%d0, %d1
	andil	#0x3ffff, %d0
	movel	%d0, %a0@(OBJ_W_OFF_1C)
	andiw	#0x1f, %d1
	movew	m_obj_mem+(OBJ_SIZE*0)+OBJ_HPOS, %a0@(OBJ_HPOS)
	subiw	#0x10, %a0@(OBJ_HPOS)
	addw	%d1, %a0@(OBJ_HPOS)
	movew	m_obj_mem+(OBJ_SIZE*0)+OBJ_VPOS, %a0@(OBJ_VPOS)
	subiw	#0x30, %a0@(OBJ_VPOS)

	movew	#0x2d, %a0@(OBJ_COUNTER)

tbl_obj1_rout1:
	movel	%a0@(OBJ_W_OFF_18), %d0
	addl	%d0, %a0@(OBJ_HPOS)
	movel	%a0@(OBJ_W_OFF_1C), %d0
	addl	%d0, %a0@(OBJ_VPOS)

	subqw	#1, %a0@(OBJ_COUNTER)
	bnes	1f /* if (--obj_counter == 0) { */
		bset	#OBJ_STAT_CLEAR_BIT, %a0@(OBJ_STAT)
	1: /* } */

	rts
/* ------------- */
warp_obj1_data:
	.long	warp_obj1_data_tbl

warp_obj1_data_tbl:
	.byte	(warp_obj1_data_tbl_end-warp_obj1_data_tbl_start)/LONG_SIZE
	.byte	2
warp_obj1_data_tbl_start:
	.long	loc_ff2644
	.long	loc_ff264a
	.long	loc_ff2650
warp_obj1_data_tbl_end:

loc_ff2644:
	.incbin	"data/warp/warp_obj1_data0.bin"
loc_ff264a:
	.incbin	"data/warp/warp_obj1_data1.bin"
loc_ff2650:
	.incbin	"data/warp/warp_obj1_data2.bin"
/* ------------------------------------------------------- */
warp_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x00
	.byte	0x07, 0x78, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x00
	.byte	0x81, 0x3d, 0x00, 0x02
	.byte	0x01, 0x00, 0x00
	.even
/* ------------------------------------------------------- */
warp_palette:
	.incbin	"data/warp/warp_palette.bin"
warp_palette_end:
/* ------------------------------------------------------- */
warp_tiles:
	.incbin	"data/warp/warp_tiles.bin"
warp_tiles_end:
/* ------------------------------------------------------- */