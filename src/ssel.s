/* ------------------------------------------------------- */
/* ssel - stage select                                     */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"
	.include "src/inc/z80.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/m_map.i"
	.include "src/inc/obj_eng.i"

	.include "src/common/m_vint_bits.s"
	.include "src/common/m_obj_eng.s"

vdp_addr_0000	= 0x0000
vdp_addr_C204	= 0xC204
vdp_addr_C284	= 0xC284
vdp_addr_C304	= 0xC304
vdp_addr_C384	= 0xC384
vdp_addr_C404	= 0xC404
vdp_addr_C484	= 0xC484
vdp_addr_C504	= 0xC504
vdp_addr_C584	= 0xC584
vdp_addr_C604	= 0xC604
vdp_addr_C684	= 0xC684
vdp_addr_C704	= 0xC704
vdp_addr_E400	= 0xE400

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	ssel
	.word	(mmd_warp_size/4)-1
	.long	ssel
	.long	0
	.long	0
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib
/* ------------------------------------------------------- */
ssel:
	movel	#ssel_vint, M_VECTOR_VINT
	bsrw	com_yield_subcpu

	lea	mmd_ram_arena_l, %a0
	movew	#mmd_ram_arena_size_l-1, %d7
	1: /* for (long of mmd_ram_arena) { */
		movel	#0, %a0@+
		dbf	%d7, 1b
	/* } */

	lea	%pc@(ssel_init_vdpreg), %a0
	bsrw	lib_sys_init
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E400&0x3FFF)<<16)+((vdp_addr_E400>>14)&0x3), VDP_ADDR
	movel	#0, VDP_DATA
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_0000&0x3FFF)<<16)+((vdp_addr_0000>>14)&0x3), VDP_ADDR
	lea	%pc@(ssel_nemtiles), %a0
	bsrw	lib_nem_vdpdec
	lea	%pc@(ssel_maps), %a0
	movew	#0x8000, %d0
	bsrw	lib_cursor_move

	lea	%pc@(ssel_palette), %a0
	lea	m_mmd_palette, %a1
	moveq	#((ssel_palette_end-ssel_palette)/LONG_SIZE)-1, %d7
	1: /* for (long of ssel_palette) { */
		movel	%a0@+, %a1@+

		dbf	%d7, 1b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	bsrw	lib_palfade_init
	bsrw	main_wait_int
	bsrw	lib_palfade_inb

	2: /* while (!(ctrl_start_a_b_c)) { */
		lea	%pc@(ssel_maps), %a0
		movew	#0x8000, %d0
		bsrw	lib_cursor_move

		lea	%pc@(ssel_choice), %a1
		moveq	#0, %d6
		moveq	#0x0a, %d7

		btst	#CMD_BUFFER_CTRL_UP_BIT, CMD_BUFFER_CTRL
		beqs	1f /* if (ctrl_up) { */
			bsrw	lib_cursor_up_w
		1: /* } */
		btst	#CMD_BUFFER_CTRL_DOWN_BIT, CMD_BUFFER_CTRL
		beqs	1f /* if (ctrl_down) { */
			bsrw	lib_cursor_down_w
		1: /* } */

		movew	%a1@, %d0
		muluw	#0x0a, %d0
		lea	%pc@(ssel_maps2), %a0
		lea	%a0@(0, %d0:w), %a0
		movew	#0x8040, %d0
		bsrw	lib_cursor_move

		bsrw	main_wait_int

		moveb	CMD_BUFFER_CTRL, %d0
		andiw	#0b11110000, %d0
		beqs	2b
	/* } */

	bsrw	lib_palfade_outb
	movew	(ssel_choice).l, %d0
	rts
/* ------------- */
ssel_choice:
	.word	0
/* ------------------------------------------------------- */
ssel_vint:
	m_vint_open
	beqs	9f /* if (m_vint_open()) { */
		m_vint_dispon
		bsrw	lib_int_disable
		m_vint_vdpwait
		m_vint_palload

		m_vint_finish
	9: /* } */

	m_vint_ret
/* ------------------------------------------------------- */
	.include "src/common/com_yield_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_req_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_subflag.s"
/* ------------------------------------------------------- */
ssel_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x00
	.byte	0x06, 0x70, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x00
	.byte	0x81, 0x39, 0x00, 0x02
	.byte	0x01, 0x00, 0x00
    .even
/* ------------------------------------------------------- */
ssel_palette:
	.incbin	"data/ssel/ssel_palette.bin"
ssel_palette_end:
/* ------------------------------------------------------- */
ssel_maps:
	.word	(((ssel_maps_end-ssel_maps_start)/LONG_SIZE)/2)-1

ssel_maps_start:
	.long	loc_ff2296, (VDP_VRAM_WRITE)+((vdp_addr_C204&0x3FFF)<<16)+((vdp_addr_C204>>14)&0x3)
	.long	loc_ff229c, (VDP_VRAM_WRITE)+((vdp_addr_C284&0x3FFF)<<16)+((vdp_addr_C284>>14)&0x3)
	.long	loc_ff22a2, (VDP_VRAM_WRITE)+((vdp_addr_C304&0x3FFF)<<16)+((vdp_addr_C304>>14)&0x3)
	.long	loc_ff22a8, (VDP_VRAM_WRITE)+((vdp_addr_C384&0x3FFF)<<16)+((vdp_addr_C384>>14)&0x3)
	.long	loc_ff22ae, (VDP_VRAM_WRITE)+((vdp_addr_C404&0x3FFF)<<16)+((vdp_addr_C404>>14)&0x3)
	.long	loc_ff22b4, (VDP_VRAM_WRITE)+((vdp_addr_C484&0x3FFF)<<16)+((vdp_addr_C484>>14)&0x3)
	.long	loc_ff22ba, (VDP_VRAM_WRITE)+((vdp_addr_C504&0x3FFF)<<16)+((vdp_addr_C504>>14)&0x3)
	.long	loc_ff22c0, (VDP_VRAM_WRITE)+((vdp_addr_C584&0x3FFF)<<16)+((vdp_addr_C584>>14)&0x3)
	.long	loc_ff22c6, (VDP_VRAM_WRITE)+((vdp_addr_C604&0x3FFF)<<16)+((vdp_addr_C604>>14)&0x3)
	.long	loc_ff22cb, (VDP_VRAM_WRITE)+((vdp_addr_C684&0x3FFF)<<16)+((vdp_addr_C684>>14)&0x3)
	.long	loc_ff22d3, (VDP_VRAM_WRITE)+((vdp_addr_C704&0x3FFF)<<16)+((vdp_addr_C704>>14)&0x3)
ssel_maps_end:

ssel_maps2:
	.word	0
	.long	loc_ff2296, (VDP_VRAM_WRITE)+((vdp_addr_C204&0x3FFF)<<16)+((vdp_addr_C204>>14)&0x3)
	.word	0
	.long	loc_ff229c, (VDP_VRAM_WRITE)+((vdp_addr_C284&0x3FFF)<<16)+((vdp_addr_C284>>14)&0x3)
	.word	0
	.long	loc_ff22a2, (VDP_VRAM_WRITE)+((vdp_addr_C304&0x3FFF)<<16)+((vdp_addr_C304>>14)&0x3)
	.word	0
	.long	loc_ff22a8, (VDP_VRAM_WRITE)+((vdp_addr_C384&0x3FFF)<<16)+((vdp_addr_C384>>14)&0x3)
	.word	0
	.long	loc_ff22ae, (VDP_VRAM_WRITE)+((vdp_addr_C404&0x3FFF)<<16)+((vdp_addr_C404>>14)&0x3)
	.word	0
	.long	loc_ff22b4, (VDP_VRAM_WRITE)+((vdp_addr_C484&0x3FFF)<<16)+((vdp_addr_C484>>14)&0x3)
	.word	0
	.long	loc_ff22ba, (VDP_VRAM_WRITE)+((vdp_addr_C504&0x3FFF)<<16)+((vdp_addr_C504>>14)&0x3)
	.word	0
	.long	loc_ff22c0, (VDP_VRAM_WRITE)+((vdp_addr_C584&0x3FFF)<<16)+((vdp_addr_C584>>14)&0x3)
	.word	0
	.long	loc_ff22c6, (VDP_VRAM_WRITE)+((vdp_addr_C604&0x3FFF)<<16)+((vdp_addr_C604>>14)&0x3)
	.word	0
	.long	loc_ff22cb, (VDP_VRAM_WRITE)+((vdp_addr_C684&0x3FFF)<<16)+((vdp_addr_C684>>14)&0x3)
	.word	0
	.long	loc_ff22d3, (VDP_VRAM_WRITE)+((vdp_addr_C704&0x3FFF)<<16)+((vdp_addr_C704>>14)&0x3)

loc_ff2296:
	.incbin	"data/ssel/data_ff2296.bin"
loc_ff229c:
	.incbin	"data/ssel/data_ff229c.bin"
loc_ff22a2:
	.incbin	"data/ssel/data_ff22a2.bin"
loc_ff22a8:
	.incbin	"data/ssel/data_ff22a8.bin"
loc_ff22ae:
	.incbin	"data/ssel/data_ff22ae.bin"
loc_ff22b4:
	.incbin	"data/ssel/data_ff22b4.bin"
loc_ff22ba:
	.incbin	"data/ssel/data_ff22ba.bin"
loc_ff22c0:
	.incbin	"data/ssel/data_ff22c0.bin"
loc_ff22c6:
	.incbin	"data/ssel/data_ff22c6.bin"
loc_ff22cb:
	.incbin	"data/ssel/data_ff22cb.bin"
loc_ff22d3:
	.incbin	"data/ssel/data_ff22d3.bin"
/* ------------------------------------------------------- */
	.include "src/common/lib_palfade.s"
/* ------------------------------------------------------- */
	.include "src/common/lib_cursor.s"
/* ------------------------------------------------------- */
	.include "src/common/lib_cursor_move.s"
/* ------------------------------------------------------- */
	.include "src/common/lib_sys_init.s"
/* ------------------------------------------------------- */
ssel_nemtiles:
	.incbin	"data/ssel/ssel_nemtiles.bin"
/* ------------------------------------------------------- */
