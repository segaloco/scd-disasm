/* ------------------------------------------------------- */
/* pcm000 - the PCM driver                                                   */
/*  _zero_index is used to allow %ax@(0) to not be optimized to %ax@         */
/* ------------------------------------------------------- */
	.equ	A0_OFF_0_P,     _zero_index
	.equ	A0_OFF_4_L,     0x04
	.equ	A0_OFF_8_L,     0x08
	.equ	A0_OFF_C_B,     0x0c
	.equ	A0_OFF_D_B,     0x0d
	.equ	A0_OFF_E_W,     0x0e

	.equ	A1_OFF_0_P,     _zero_index
	.equ	A1_OFF_4_L,     0x04
	.equ	A1_OFF_D_B,     0x0d
	.equ	A1_OFF_E_W,     0x0e

	.equ	A2_OFF_1_B,     0x01
	.equ	A2_OFF_2_B,     0x02
	.equ	A2_OFF_3_B,     0x03
	.equ	A2_OFF_4_B,     0x04
	.equ	A2_OFF_5_B,     0x05

	.equ	BLT_OFF_0_B,    _zero_index
	.equ	BLT_OFF_1_B,    0x01
	.equ	BLT_OFF_2_B,    0x02
	.equ	BLT_OFF_3_B,    0x03
	.equ	BLT_OFF_4_P,    0x04
	.equ	BLT_OFF_8_B,    0x08
	.equ	BLT_OFF_9_B,    0x09
	.equ	BLT_OFF_A_B,    0x0a
	.equ	BLT_OFF_B_B,    0x0b
	.equ	BLT_OFF_C_B,    0x0c
	.equ	BLT_OFF_D_B,    0x0d
	.equ	BLT_OFF_E_B,    0x0e
	.equ	BLT_OFF_F_B,    0x0f
	.equ	BLT_OFF_10_W,   0x10
	.equ	BLT_OFF_12_B,   0x12
	.equ	BLT_OFF_13_B,   0x13
	.equ	BLT_OFF_14_W,   0x14
	.equ	BLT_OFF_16_W,   0x16
	.equ	BLT_OFF_18_P,   0x18
	.equ	BLT_OFF_1C_L,   0x1c
	.equ	BLT_OFF_20_L,   0x20
	.equ	BLT_OFF_24_P,   0x24
	.equ	BLT_OFF_28_L,   0x28
	.equ	BLT_OFF_2C_L,   0x2c
	.equ	BLT_OFF_30_B,   0x30
	.equ	BLT_OFF_31_B,   0x31
	.equ	BLT_OFF_32_B,   0x32

	.equ	ALT_OFF_1_B,    0x01
	.equ	ALT_OFF_3_B,    0x03
	.equ	ALT_OFF_5_B,    0x05
	.equ	ALT_OFF_7_B,    0x07
	.equ	ALT_OFF_9_B,    0x09
	.equ	ALT_OFF_B_B,    0x0b
	.equ	ALT_OFF_D_B,    0x0d
	.equ	ALT_OFF_F_B,    0x0f
	.equ	ALT_OFF_11_B,   0x11

	.equ	OBJ_OFF_0_B,    _zero_index
	.equ	OBJ_OFF_1_B,    0x001
	.equ	OBJ_OFF_2_B,    0x002
	.equ	OBJ_OFF_3_B,    0x003
	.equ	OBJ_OFF_4_B,    0x004
	.equ	OBJ_OFF_5_B,    0x005
	.equ	OBJ_OFF_6_B,    0x006
	.equ	OBJ_OFF_9_B,    0x009
	.equ	OBJ_OFF_A_L,    0x00a
	.equ	OBJ_OFF_E_B,    0x00e
	.equ	OBJ_OFF_F_B,    0x00f
	.equ	OBJ_OFF_10_L,   0x010
	.equ	OBJ_OFF_14_B,   0x014
	.equ	OBJ_OFF_15_B,   0x015
	.equ	OBJ_OFF_16_B,   0x016
	.equ	OBJ_OFF_17_B,   0x017
	.equ	OBJ_OFF_18_B,   0x018
	.equ	OBJ_OFF_80_B,   0x080
	.equ	OFF_80_7_BIT,   7
	.equ	OFF_80_4_BIT,   4
	.equ	OFF_80_1_BIT,   1
	.equ	OBJ_OFF_100_B,  0x100
	.equ	OBJ_OFF_500_B,  0x500

	.equ	ADDR_0000, 0xff0000
	.equ	ADDR_0020, 0xff0020
	.equ	ADDR_2001, 0xff2001

	.section .text.s_lib
/* ------------------------------------------------------- */
	.ascii	"SNCBNK04.S28    "
/* ------------------------------------------------------- */
pcm_timer_entry:
	jmp	%pc@(loc_18918)

	jmp	%pc@(loc_18faa)

pcm_post_header:
/* ------------------------------------------------------- */
	.fill	0x900
/* ------------------------------------------------------- */
loc_18918:
	jsr	%pc@(sub_18fc8)
	addqb	#1, %a5@(OBJ_OFF_6_B)
	jsr	%pc@(sub_18c1e)
	jsr	%pc@(sub_18c9c)
	jsr	%pc@(sub_18e86)
	jsr	%pc@(sub_18eac)
	jsr	%pc@(sub_18e28)
	jsr	%pc@(sub_18940)
	moveb	%a5@(OBJ_OFF_2_B), %a4@(ALT_OFF_11_B)
	rts
/* ------------------------------------------------------- */
sub_18940:
	clrb	%a5@(OBJ_OFF_E_B)
	lea	%a5@(OBJ_OFF_80_B), %a3
	moveq	#7, %d7

2:
	addaw	#0x80, %a3
	tstb	%a3@
	bpls	1f

	jsr	%pc@(sub_18978)

1:
	dbf	%d7, 2b

	lea	%a5@(OBJ_OFF_500_B), %a3
	moveb	#0x80, %a5@(OBJ_OFF_E_B)
	moveq	#7, %d7

2:
	tstb	%a3@
	bpls	1f

	jsr	%pc@(sub_18978)

1:
	addaw	#0x80, %a3
	dbf	%d7, 2b

	rts
/* ------------------------ */
sub_18978:
	subqb	#1, %a3@(BLT_OFF_B_B)
	bnes	1f

	bclr	#OFF_80_4_BIT, %a3@
	jsr	%pc@(sub_1899a)
	jsr	%pc@(sub_18a90)
	jsr	%pc@(sub_18a5e)
	braw	sub_18ef4

1:
	jsr	%pc@(sub_18aa4)
	braw	loc_18a4a
/* ------------------------ */
sub_1899a:
	moveal	%a3@(BLT_OFF_4_P), %a2
	bclr	#OFF_80_1_BIT, %a3@

1:
	moveq	#0, %d5
	moveb	%a2@+, %d5
	cmpib	#0xe0, %d5
	bcss	1f

	jsr	%pc@(sub_19098)
	bras	1b

1:
	tstb	%d5
	bpls	1f

	jsr	%pc@(sub_189cc)
	moveb	%a2@+, %d5
	bpls	1f

	subqw	#1, %a2
	braw	loc_189ea

1:
	jsr	%pc@(sub_18a32)
	braw	loc_189ea
/* ------------------------ */
sub_189cc:
	subib	#0x80, %d5
	beqw	sub_18ed4

	lea	%pc@(data_18fd8), %a0
	addb	%a3@(BLT_OFF_8_B), %d5
	andiw	#0x7f, %d5
	lslw	#1, %d5
	movew	%a0@(0, %d5:w), %a3@(BLT_OFF_10_W)
	rts
/* ------------------------ */
loc_189ea:
	movel	%a2, %a3@(BLT_OFF_4_P)
	moveb	%a3@(BLT_OFF_C_B), %a3@(BLT_OFF_B_B)
	btst	#OFF_80_4_BIT, %a3@
	bnes	9f

	jsr	%pc@(sub_18ee4)
	moveb	%a3@(BLT_OFF_E_B), %a3@(BLT_OFF_D_B)
	movel	%a3@(BLT_OFF_28_L), %a3@(BLT_OFF_24_P)
	movel	%a3@(BLT_OFF_1C_L), %a3@(BLT_OFF_20_L)
	moveb	%a3@(BLT_OFF_30_B), %a3@(BLT_OFF_31_B)
	movel	#ADDR_2001, %a3@(BLT_OFF_18_P)
	clrw	%a3@(BLT_OFF_14_W)
	clrw	%a3@(BLT_OFF_16_W)
	clrb	%a3@(BLT_OFF_12_B)
	moveb	#7, %a3@(BLT_OFF_13_B)

9:
	rts
/* ------------------------ */
sub_18a32:
	moveb	%d5, %d0
	moveb	%a3@(BLT_OFF_2_B), %d1

1:
	subqb	#1, %d1
	beqs	1f

	addb	%d5, %d0
	bras	1b

1:
	moveb	%d0, %a3@(BLT_OFF_C_B)
	moveb	%d0, %a3@(BLT_OFF_B_B)
	rts
/* ------------------------ */
loc_18a4a:
	tstb	%a3@(BLT_OFF_D_B)
	beqs	9f

	subqb	#1, %a3@(BLT_OFF_D_B)
	bnes	9f

	jsr	%pc@(sub_18ed4)
	addqw	#4, %sp

9:
	rts
/* ------------------------------------------------------- */
sub_18a5e:
	btst	#OFF_80_1_BIT, %a3@((BLT_OFF_0_B).w)
	bnes	9f

	movew	%a3@(BLT_OFF_10_W), %d5
	moveb	%a3@(BLT_OFF_F_B), %d0
	extw	%d0
	addw	%d0, %d5
	movew	%d5, %d1
	moveb	%a3@(BLT_OFF_1_B), %d0
	orb	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	moveb	%d1, %a4@(ALT_OFF_5_B)
	lsrw	#8, %d1
	moveb	%d1, %a4@(ALT_OFF_7_B)
	rts

9:
	addqw	#4, %sp
	rts
/* ------------------------------------------------------- */
sub_18a90:
	tstb	%a3@(BLT_OFF_32_B)
	bnes	9f

	btst	#OFF_80_1_BIT, %a3@((BLT_OFF_0_B).w)
	bnes	9f

	braw	loc_18b04

9:
	rts
/* ------------------------ */
sub_18aa4:
	tstb	%a3@(BLT_OFF_31_B)
	beqs	1f

	subqb	#1, %a3@(BLT_OFF_31_B)
	beqw	sub_18ed4

1:
	tstb	%a3@(BLT_OFF_32_B)
	bnew	9f

	btst	#OFF_80_1_BIT, %a3@((BLT_OFF_0_B).w)
	bnew	9f

	lea	ADDR_0020, %a0
	moveq	#0, %d0
	moveq	#0, %d1
	moveb	%a3@(BLT_OFF_1_B), %d1
	lslw	#2, %d1
	movel	%a0@(0, %d1:w), %d0
	movel	%d0, %d1
	lslw	#8, %d0
	swap	%d1
	moveb	%d1, %d0
	movew	%a3@(BLT_OFF_14_W), %d1
	movew	%d0, %a3@(BLT_OFF_14_W)
	cmpw	%d1, %d0
	bccs	1f

	subiw	#0x1e00, %a3@(BLT_OFF_16_W)

1:
	andiw	#0x1fff, %d0
	addiw	#0x1000, %d0
	movew	%a3@(BLT_OFF_16_W), %d1
	cmpw	%d1, %d0
	bhis	loc_18b04

9:
	rts
/* ------------------------ */
loc_18b04:
	addiw	#0x200, %a3@(BLT_OFF_16_W)
	movel	%a3@(BLT_OFF_20_L), %d6
	moveal	%a3@(BLT_OFF_24_P), %a2
	moveal	%a3@(BLT_OFF_18_P), %a0
	moveb	%a3@(BLT_OFF_1_B), %d1
	lslb	#1, %d1
	addb	%a3@(BLT_OFF_12_B), %d1
	orib	#0x80, %d1
	moveb	%d1, %a4@(ALT_OFF_F_B)
	movel	#0x200, %d0
	movel	%d0, %d1

loop_18b30:
	cmpl	%d0, %d6
	bccs	1f

	movel	%d6, %d0

1:
	subl	%d0, %d6
	subl	%d0, %d1
	subql	#1, %d0

1:
	moveb	%a2@+, %a0@+
	addqw	#1, %a0
	dbf	%d0, 1b

	tstl	%d1
	beqs	1f

	moveq	#0, %d0
	movel	%a3@(BLT_OFF_1C_L), %d0
	subl	%a3@(BLT_OFF_2C_L), %d0
	subal	%d0, %a2
	addl	%d0, %d6
	movel	%d1, %d0
	bras	loop_18b30

1:
	tstl	%d6
	bnes	1f

	moveq	#0, %d0
	movel	%a3@(BLT_OFF_1C_L), %d0
	subl	%a3@(BLT_OFF_2C_L), %d0
	subal	%d0, %a2
	movel	%d0, %d6

1:
	movel	%d6, %a3@(BLT_OFF_20_L)
	movel	%a2, %a3@(BLT_OFF_24_P)
	subqb	#1, %a3@(BLT_OFF_13_B)
	bmis	1f

	movel	%a0, %a3@(BLT_OFF_18_P)
	rts

1:
	movel	#ADDR_2001, %a3@(BLT_OFF_18_P)
	tstb	%a3@(BLT_OFF_12_B)
	bnes	1f

	moveb	#6, %a3@(BLT_OFF_13_B)
	moveb	#1, %a3@(BLT_OFF_12_B)
	rts

1:
	moveb	#7, %a3@(BLT_OFF_13_B)
	clrb	%a3@(BLT_OFF_12_B)
	rts
/* ------------------------------------------------------- */
sub_18ba8:
	lea	%pc@(data_19300), %a0
	movel	%a0@+, %d0
	beqs	9f
	bmis	9f

	subqw	#1, %d0

3:
	moveal	%a0@+, %a1
	addal	%a5@(OBJ_OFF_10_L), %a1
	tstb	%a1@(A1_OFF_D_B)
	beqs	8f

	moveal	%a1@((A1_OFF_0_P).w), %a2
	addal	%a5@(OBJ_OFF_10_L), %a2
	movew	%a1@(A1_OFF_E_W), %d1
	movew	%d1, %d5
	rolw	#4, %d1
	orib	#0x80, %d1
	andiw	#0xf00, %d5
	movel	%a1@(A1_OFF_4_L), %d2
	movew	%d2, %d3
	rolw	#4, %d3
	andiw	#0xf, %d3

2:
	moveb	%d1, %a4@(ALT_OFF_F_B)
	movew	%d2, %d4
	cmpiw	#0x1000, %d2
	blss	1f

	movew	#0x1000, %d4

1:
	addw	%d5, %d2
	subw	%d5, %d4
	subqw	#1, %d4
	lea	ADDR_2001, %a3
	addaw	%d5, %a3
	addaw	%d5, %a3

1:
	moveb	%a2@+, %a3@+
	addqw	#1, %a3
	dbf	%d4, 1b

	subiw	#0x1000, %d2
	addqb	#1, %d1
	moveq	#0, %d5
	dbf	%d3, 2b

8:
	dbf	%d0, 3b

9:
	rts
/* ------------------------------------------------------- */
sub_18c1e:
	tstl	%a5@(OBJ_OFF_A_L)
	beqs	9f

	lea	%a5@(OBJ_OFF_A_L), %a1
	moveb	%a5@(OBJ_OFF_3_B), %d3
	moveq	#3, %d4

3:
	moveq	#0, %d0
	moveb	%a1@, %d0
	moveb	%d0, %d1
	clrb	%a1@+
	cmpib	#0x81, %d0
	bcss	8f

	cmpib	#0x8f, %d0
	blsw	loc_18c7e

	cmpib	#0xb0, %d0
	bcss	8f

	cmpib	#0xb4, %d0
	blsw	loc_18c88

	cmpib	#0xe0, %d0
	bcss	8f

	cmpib	#0xe4, %d0
	blsw	loc_18c92

	bras	8f

2:
	moveb	%a0@(0, %d0:w), %d2
	cmpb	%d3, %d2
	bcss	8f

	moveb	%d2, %d3
	moveb	%d1, %a5@(OBJ_OFF_9_B)

8:
	dbf	%d4, 3b

	tstb	%d3
	bmis	9f

	moveb	%d3, %a5@(OBJ_OFF_3_B)

9:
	rts

loc_18c7e:
	subib	#0x81, %d0
	lea	%pc@(data_192e6), %a0
	bras	2b

loc_18c88:
	subib	#0xb0, %d0
	lea	%pc@(data_192e6), %a0
	bras	2b

loc_18c92:
	subib	#0xe0, %d0
	lea	%pc@(data_192e8), %a0
	bras	2b
/* ------------------------------------------------------- */
sub_18c9c:
	moveq	#0, %d7
	moveb	%a5@(OBJ_OFF_9_B), %d7
	beqw	loc_18faa

	bplw	loc_18f78

	moveb	#0x80, %a5@(OBJ_OFF_9_B)
	cmpib	#0x81, %d7
	bcss	9f

	cmpib	#0x8f, %d7
	blsw	loc_18cdc

	cmpib	#0xb0, %d7
	bcss	9f

	cmpib	#0xb4, %d7
	blsw	loc_18d88

	cmpib	#0xe0, %d7
	bcss	9f

	cmpib	#0xe4, %d7
	blsw	loc_18f04

9:
	rts

loc_18cdc:
	jsr	sub_18f50
	lea	%pc@(data_192ee), %a2
	subib	#0x81, %d7
	andiw	#0x7f, %d7
	lslw	#2, %d7
	moveal	%a2@(0, %d7:w), %a2
	addal	%a5@(OBJ_OFF_10_L), %a2
	moveal	%a2, %a0
	moveq	#0, %d7
	moveb	%a2@(A2_OFF_2_B), %d7
	moveb	%a2@(A2_OFF_4_B), %d1
	moveb	%a2@(A2_OFF_5_B), %a5@((OBJ_OFF_0_B).w)
	moveb	%a2@(A2_OFF_5_B), %a5@(OBJ_OFF_1_B)
	addqw	#6, %a2
	lea	%a5@(OBJ_OFF_80_B), %a3
	lea	%pc@(data_18d7e), %a1
	moveb	#0x80, %d2
	subqw	#1, %d7

1:
	moveq	#0, %d0
	movew	%a2@+, %d0
	addl	%a0, %d0
	movel	%d0, %a3@(BLT_OFF_4_P)
	movew	%a2@+, %a3@(BLT_OFF_8_B)
	moveb	%a1@+, %d0
	moveb	%d0, %a3@(BLT_OFF_1_B)
	orib	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	lslb	#5, %d0
	moveb	%d0, %a4@(ALT_OFF_D_B)
	moveb	%d0, %a4@(ALT_OFF_B_B)
	moveb	#0, %a4@(ALT_OFF_9_B)
	moveb	#0xff, %a4@(ALT_OFF_3_B)
	moveb	%a3@(BLT_OFF_9_B), %a4@(ALT_OFF_1_B)
	moveb	%d1, %a3@(BLT_OFF_2_B)
	moveb	%d2, %a3@(BLT_OFF_A_B)
	moveb	#0x80, %a3@((BLT_OFF_0_B).w)
	moveb	#1, %a3@(BLT_OFF_B_B)
	addaw	#0x80, %a3
	dbf	%d7, 1b

	clrb	%a5@(OBJ_OFF_80_B)
	moveb	#0xff, %a5@(OBJ_OFF_2_B)
	rts
/* ------------- */
data_18d7e:
	.byte	7, 0, 1, 2
	.byte	3, 4, 5, 7
	.byte	6, 0
/* ------------------------------------------------------- */
loc_18d88:
	lea	%pc@(data_192e2), %a2
	subib	#0xb0, %d7
	andiw	#0x7f, %d7
	lslw	#2, %d7
	moveal	%a2@(0, %d7:w), %a2
	addal	%a5@(OBJ_OFF_10_L), %a2
	moveal	%a2, %a0
	moveq	#0, %d7
	moveb	%a2@(A2_OFF_3_B), %d7
	moveb	%a2@(A2_OFF_2_B), %d1
	addqw	#4, %a2
	lea	%pc@(data_18d7e), %a1
	moveb	#0x80, %d2
	subqw	#1, %d7

2:
	lea	%a5@(OBJ_OFF_500_B), %a3
	moveq	#0, %d0
	moveb	%a2@(A2_OFF_1_B), %d0
	lslw	#7, %d0
	addaw	%d0, %a3
	moveal	%a3, %a1
	movew	#0x1f, %d0

1:
	clrl	%a1@+
	dbf	%d0, 1b

	movew	%a2@+, %a3@
	moveq	#0, %d0
	movew	%a2@+, %d0
	addl	%a0, %d0
	movel	%d0, %a3@(BLT_OFF_4_P)
	movew	%a2@+, %a3@(BLT_OFF_8_B)
	moveb	%a3@(BLT_OFF_1_B), %d0
	orib	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	lslb	#5, %d0
	moveb	%d0, %a4@(ALT_OFF_D_B)
	moveb	%d0, %a4@(ALT_OFF_B_B)
	moveb	#0, %a4@(ALT_OFF_9_B)
	moveb	#0xff, %a4@(ALT_OFF_3_B)
	moveb	%a3@(BLT_OFF_9_B), %a4@(ALT_OFF_1_B)
	moveb	%d1, %a3@(BLT_OFF_2_B)
	moveb	%d2, %a3@(BLT_OFF_A_B)
	moveb	#1, %a3@(BLT_OFF_B_B)
	moveb	#0, %a3@(BLT_OFF_E_B)
	moveb	#0, %a3@(BLT_OFF_F_B)
	dbf	%d7, 2b

	rts
/* ------------------------------------------------------- */
sub_18e28:
	moveq	#0, %d0
	moveb	%a5@(OBJ_OFF_14_B), %d0
	beqs	9f

	moveb	%a5@(OBJ_OFF_17_B), %d0
	beqs	1f

	subqb	#1, %a5@(OBJ_OFF_17_B)

9:
	rts

1:
	subqb	#1, %a5@(OBJ_OFF_14_B)
	beqw	sub_18f50

	moveb	%a5@(OBJ_OFF_16_B), %a5@(OBJ_OFF_17_B)
	lea	%a5@(OBJ_OFF_80_B), %a3
	moveq	#8, %d7
	moveb	%a5@(OBJ_OFF_15_B), %d6
	addb	%d6, %a5@(OBJ_OFF_18_B)

2:
	tstb	%a3@
	bpls	8f

	subb	%d6, %a3@(BLT_OFF_9_B)
	bccs	1f

	clrb	%a3@(BLT_OFF_9_B)
	bclr	#OFF_80_7_BIT, %a3@

1:
	moveb	%a3@(BLT_OFF_1_B), %d0
	orib	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	moveb	%a3@(BLT_OFF_9_B), %a4@(ALT_OFF_1_B)

8:
	addaw	#0x80, %a3
	dbf	%d7, 2b

	rts
/* ------------------------------------------------------- */
sub_18e86:
	tstb	%a5@(OBJ_OFF_F_B)
	beqs	9f

	bmis	2f

	cmpib	#2, %a5@(OBJ_OFF_F_B)
	beqs	1f

	moveb	#0xff, %a4@(ALT_OFF_11_B)
	moveb	#2, %a5@(OBJ_OFF_F_B)

1:
	addqw	#4, %sp

9:
	rts

2:
	clrb	%a5@(OBJ_OFF_F_B)
	rts
/* ------------------------------------------------------- */
sub_18eac:
	tstb	%a5@((OBJ_OFF_0_B).w)
	beqs	9f

	subqb	#1, %a5@(OBJ_OFF_1_B)
	bnes	9f

	moveb	%a5@((OBJ_OFF_0_B).w), %a5@(OBJ_OFF_1_B)
	lea	%a5@(OBJ_OFF_80_B), %a0
	movew	#0x80, %d1
	moveq	#8, %d0

1:
	addqb	#1, %a0@(BLT_OFF_B_B)
	addaw	%d1, %a0
	dbf	%d0, 1b

9:
	rts
/* ------------------------------------------------------- */
sub_18ed4:
	moveb	%a3@(BLT_OFF_1_B), %d0
	bset	%d0, %a5@(OBJ_OFF_2_B)
	bset	#OFF_80_1_BIT, %a3@((BLT_OFF_0_B).w)
	rts
/* ------------------------------------------------------- */
sub_18ee4:
	moveb	%a3@(BLT_OFF_1_B), %d0
	bset	%d0, %a5@(OBJ_OFF_2_B)
	moveb	%a5@(OBJ_OFF_2_B), %a4@(ALT_OFF_11_B)
	rts
/* ------------------------------------------------------- */
sub_18ef4:
	btst	#OFF_80_4_BIT, %a3@
	bnes	9f

	moveb	%a3@(BLT_OFF_1_B), %d0
	bclr	%d0, %a5@(OBJ_OFF_2_B)

9:
	rts
/* ------------------------------------------------------- */
loc_18f04:
	moveb	%d7, %d0
	subib	#0xe0, %d7
	lslw	#2, %d7
	jmp	%pc@(tbl_18f10, %d7:w)
/* ------------- */
tbl_18f10:
	jmp	%pc@(loc_18f24)

	jmp	%pc@(loc_18f78)

	jmp	%pc@(loc_18f38)

	jmp	%pc@(loc_18f40)

	jmp	%pc@(loc_18f48)
/* ------------- */
loc_18f24:
	moveb	#0x60, %a5@(OBJ_OFF_14_B)
	moveb	#1, %a5@(OBJ_OFF_16_B)
	moveb	#2, %a5@(OBJ_OFF_15_B)
	rts
/* ------------- */
loc_18f38:
	moveb	#1, %a5@(OBJ_OFF_F_B)
	rts
/* ------------- */
loc_18f40:
	moveb	#0x80, %a5@(OBJ_OFF_F_B)
	rts
/* ------------- */
loc_18f48:
	moveb	#0xff, %a4@(ALT_OFF_11_B)
	rts
/* ------------------------------------------------------- */
sub_18f50:
	moveb	#0xff, %a4@(ALT_OFF_11_B)
	movel	%a5@(OBJ_OFF_10_L), %d1
	moveal	%a5, %a0
	movew	#0x23f, %d0

1:
	clrl	%a0@+
	dbf	%d0, 1b

	movel	%d1, %a5@(OBJ_OFF_10_L)
	moveb	#0xff, %a5@(OBJ_OFF_2_B)
	moveb	#0x80, %a5@(OBJ_OFF_9_B)
	rts
/* ------------------------------------------------------- */
loc_18f78:
	jsr	%pc@(sub_18f50)
	jsr	%pc@(sub_18f84)
	braw	sub_18ba8
/* ------------------------------------------------------- */
sub_18f84:
	moveb	#0x80, %d3
	moveq	#0xf, %d1

2:
	lea	ADDR_2001, %a0
	moveb	%d3, %a4@(ALT_OFF_F_B)
	moveq	#-1, %d2
	movew	#0xfff, %d0

1:
	moveb	%d2, %a0@+
	addqw	#1, %a0
	dbf	%d0, 1b

	addqw	#1, %d3
	dbf	%d1, 2b

	rts
/* ------------------------------------------------------- */
loc_18faa:
	jsr	%pc@(sub_18fc8)
	moveb	#0xff, %a4@(ALT_OFF_11_B)
	moveb	#0x80, %a4@(ALT_OFF_F_B)
	lea	%pc@(pcm_timer_entry), %a0
	subal	%fp@(0x1c), %a0
	movel	%a0, %a5@(OBJ_OFF_10_L)
	bras	loc_18f78
/* ------------------------------------------------------- */
sub_18fc8:
	lea	%pc@(data_192ba), %fp
	lea	%pc@(pcm_post_header), %a5
	lea	ADDR_0000, %a4
	rts
/* ------------------------------------------------------- */
data_18fd8:
	.incbin	"data/pcm/data_18fd8.bin"
/* ------------------------------------------------------- */
sub_19098:
	subiw	#0xe0, %d5
	lslw	#2, %d5
	jmp	%pc@(tbl_190a2, %d5:w)
/* ------------------------------------------------------- */
tbl_190a2: 
	jmp	%pc@(sub_19120)

	jmp	%pc@(sub_19136)

	jmp	%pc@(sub_1913c)

	jmp	%pc@(sub_19142)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_1914a)

	jmp	%pc@(sub_19184)

	jmp	%pc@(sub_1918a)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_19194)

	jmp	%pc@(sub_1919e)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_191a4)

	jmp	%pc@(sub_1922c)

	jmp	%pc@(sub_1922c)

	jmp	%pc@(sub_1922c)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_19248)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(sub_19248)

	jmp	%pc@(sub_19254)

	jmp	%pc@(sub_1926e)

	jmp	%pc@(sub_19280)

	jmp	%pc@(sub_19294)

	jmp	%pc@(sub_1929a)

	jmp	%pc@(sub_192a2)

	jmp	%pc@(sub_1911e)

	jmp	%pc@(data_192ba)
/* ------------------------------------------------------- */
sub_1911e:
	rts
/* ------------------------------------------------------- */
sub_19120:
	moveb	%a3@(BLT_OFF_1_B), %d0
	orib	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	moveb	%a2@, %a3@(BLT_OFF_3_B)
	moveb	%a2@+, %a4@(ALT_OFF_3_B)
	rts
/* ------------------------------------------------------- */
sub_19136:
	moveb	%a2@+, %a3@(BLT_OFF_F_B)
	rts
/* ------------------------------------------------------- */
sub_1913c:
	moveb	%a2@+, %a5@(OBJ_OFF_4_B)
	rts
/* ------------------------------------------------------- */
sub_19142:
	moveb	#1, %a5@(OBJ_OFF_5_B)
	rts
/* ------------------------------------------------------- */
sub_1914a:
	moveb	%a3@(BLT_OFF_1_B), %d0
	orib	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	moveb	%a2@+, %d0
	bmis	1f

	addb	%d0, %a3@(BLT_OFF_9_B)
	bcss	2f

	braw	8f

1:
	addb	%d0, %a3@(BLT_OFF_9_B)
	bccs	2f

8:
	moveb	%a3@(BLT_OFF_9_B), %a4@(ALT_OFF_1_B)

9:
	rts

2:
	tstb	%a5@(OBJ_OFF_14_B)
	beqs	9b

	bclr	#OFF_80_7_BIT, %a3@
	moveb	#0, %a4@(ALT_OFF_1_B)
	rts
/* ------------------------------------------------------- */
sub_19184:
	bset	#OFF_80_4_BIT, %a3@
	rts
/* ------------------------------------------------------- */
sub_1918a:
	moveb	%a2@, %a3@(BLT_OFF_D_B)
	moveb	%a2@+, %a3@(BLT_OFF_E_B)
	rts
/* ------------------------------------------------------- */
sub_19194:
	moveb	%a2@, %a5@(OBJ_OFF_1_B)
	moveb	%a2@+, %a5@((OBJ_OFF_0_B).w)
	rts
/* ------------------------------------------------------- */
sub_1919e:
	moveb	%a2@+, %a5@(OBJ_OFF_A_L)
	rts
/* ------------------------------------------------------- */
sub_191a4:
	moveq	#0, %d0
	moveb	%a2@+, %d0
	lea	%pc@(data_19300), %a0
	addqw	#4, %a0
	lslw	#2, %d0
	moveal	%a0@(0, %d0:w), %a0
	addal	%a5@(OBJ_OFF_10_L), %a0
	moveb	%a0@(A0_OFF_C_B), %a3@(BLT_OFF_30_B)
	moveb	%a0@(A0_OFF_C_B), %a3@(BLT_OFF_31_B)
	moveb	%a0@(A0_OFF_D_B), %a3@(BLT_OFF_32_B)
	bnes	1f

	moveal	%a0@((A0_OFF_0_P).w), %a1
	addal	%a5@(OBJ_OFF_10_L), %a1
	movel	%a1, %a3@(BLT_OFF_28_L)
	movel	%a1, %a3@(BLT_OFF_24_P)
	movel	%a0@(A0_OFF_4_L), %a3@(BLT_OFF_1C_L)
	movel	%a0@(A0_OFF_4_L), %a3@(BLT_OFF_20_L)
	movel	%a0@(A0_OFF_8_L), %a3@(BLT_OFF_2C_L)
	movel	#ADDR_2001, %a3@(BLT_OFF_18_P)
	clrb	%a3@(BLT_OFF_12_B)
	moveb	#7, %a3@(BLT_OFF_13_B)
	rts

1:
	moveb	%a3@(BLT_OFF_1_B), %d0
	orib	#0xc0, %d0
	moveb	%d0, %a4@(ALT_OFF_F_B)
	movew	%a0@(A0_OFF_E_W), %d0
	movew	%d0, %d1
	lsrw	#8, %d0
	moveb	%d0, %a4@(ALT_OFF_D_B)
	movel	%a0@(A0_OFF_8_L), %d0
	addw	%d1, %d0
	moveb	%d0, %a4@(ALT_OFF_9_B)
	lsrw	#8, %d0
	moveb	%d0, %a4@(ALT_OFF_B_B)
	rts
/* ------------------------------------------------------- */
sub_1922c:
	bclr	#OFF_80_7_BIT, %a3@
	bclr	#OFF_80_4_BIT, %a3@
	jsr	%pc@(sub_18ed4)
	tstb	%a5@(OBJ_OFF_E_B)
	beqw	1f

	clrb	%a5@(OBJ_OFF_3_B)

1:
	addqw	#8, %sp
	rts
/* ------------------------------------------------------- */
sub_19248:
	moveb	%a2@+, %d0
	lslw	#8, %d0
	moveb	%a2@+, %d0
	addaw	%d0, %a2
	subqw	#1, %a2
	rts
/* ------------------------------------------------------- */
sub_19254:
	moveq	#0, %d0
	moveb	%a2@+, %d0
	moveb	%a2@+, %d1
	tstb	%a3@(0x40, %d0:w)
	bnes	1f

	moveb	%d1, %a3@(0x40, %d0:w)

1:
	subqb	#1, %a3@(0x40, %d0:w)
	bnes	sub_19248

	addqw	#2, %a2
	rts
/* ------------------------------------------------------- */
sub_1926e:
	moveq	#0, %d0
	moveb	%a3@(BLT_OFF_A_B), %d0
	subqb	#4, %d0
	movel	%a2, %a3@(0, %d0:w)
	moveb	%d0, %a3@(BLT_OFF_A_B)
	bras	sub_19248
/* ------------------------------------------------------- */
sub_19280:
	moveq	#0, %d0
	moveb	%a3@(BLT_OFF_A_B), %d0
	moveal	%a3@(0, %d0:w), %a2
	addqw	#2, %a2
	addqb	#4, %d0
	moveb	%d0, %a3@(BLT_OFF_A_B)
	rts
/* ------------------------------------------------------- */
sub_19294:
	moveb	%a2@+, %a3@(BLT_OFF_2_B)
	rts
/* ------------------------------------------------------- */
sub_1929a:
	moveb	%a2@+, %d0
	addb	%d0, %a3@(BLT_OFF_8_B)
	rts
/* ------------------------------------------------------- */
sub_192a2:
	lea	%a5@(OBJ_OFF_80_B), %a0
	moveb	%a2@+, %d0
	movew	#0x80, %d1
	moveq	#8, %d2

1:
	moveb	%d0, %a0@(BLT_OFF_2_B)
	addaw	%d1, %a0
	dbf	%d2, 1b

	rts
/* ------------------------------------------------------- */
data_192ba:
	.long	data_192e6

data_192be:
	.long	data_192be
	.long	data_192ee
	.long	data_192e2

data_192ca:
	.long	data_192ca

data_192ce:
	.long	data_192ce
	.long	0xb0
/* ------------------------ */
data_192d6:
	.long	pcm_timer_entry
	.long	data_192e6
	.long	data_192e8

data_192e2:
	.long	data_192ee
/* ------------------------ */
data_192e6:
	.word	0x7000
/* ------------------------ */
data_192e8:
	.long	0x80808080
	.word	0x8000
/* ------------------------ */
data_192ee:
	.word	0x0000
	.word	0x0101
	.word	0x8006
	.word	0x000a
	.word	0x00ff
	.word	0xef00
	.word	0xe11a
	.word	0x9369
	.word	0xf200
/* ------------------------ */
data_19300:
	.word	0, 1
	.long	data_19308

data_19308:
	.long	data_19318
	.word	0
	.ascii	"SR"
	.fill	8

data_19318:
	.incbin	"data/pcm/data_19318.bin"
/* ------------------------------------------------------- */
