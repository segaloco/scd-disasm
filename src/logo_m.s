/* ------------------------------------------------------- */
/* logo_m - the MainCPU portion of the SEGA logo screen    */
/*  this component loads the graphics, triggers the SubCPU */
/*  to play the PCM soundbyte, and awaits completion of    */
/*  the screen timeframe                                   */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"
	.include "src/inc/z80.i"

	.include "src/inc/m_bank.i"
	.include "src/inc/m_subreg.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/m_map.i"
	.include "src/inc/mmd.i"

	.include "src/common/m_vint_bits.s"

vdp_addr_0000	= 0x0000
vdp_addr_C412	= 0xC412
vdp_addr_E400	= 0xE400

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	MMD_DMNA_MASK
	.even
	.long	logo_m
	.word	(mmd_text_size/4)-1
	.long	logo_m
	.long	0
	.long	logo_vint_vector
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib
/* ------------------------------------------------------- */
_text_start:
	.globl	logo_m
logo_m:
	jmp	(logo_main).l
/* ------------------------------------------------------- */
logo_vint_vector:
	jmp	(logo_vint).l
/* ------------------------------------------------------- */
	.equ	TIMER_LENGTH, 0xf0
logo_main:
	movel	#logo_vint_vector, M_VECTOR_VINT
	bsrw	com_wait_subcpu
	bsrw	com_yield_subcpu
	bsrw	com_wait_subcpu_init

	lea	(mmd_ram_arena_l).w, %a0
	movew	#mmd_ram_arena_size_l-1, %d7
	1: /* for (long of mmd_ram_arena) { */
		movel	#0,  %a0@+
		dbf	%d7, 1b
	/* } */

	lea	%pc@(logo_init_vdpreg), %a0
	bsrw	lib_sys_init
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E400&0x3FFF)<<16)+((vdp_addr_E400>>14)&0x3), VDP_ADDR
	movel	#0, %a2@
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_0000&0x3FFF)<<16)+((vdp_addr_0000>>14)&0x3), VDP_ADDR
	lea	%pc@(logo_nemtiles), %a0
	bsrw	lib_nem_vdpdec
	lea	%pc@(logo_maps), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C412&0x3FFF)<<16)+((vdp_addr_C412>>14)&0x3), %d0
	movew	#0x15, %d1
	movew	#7, %d2
	bsrw	lib_map_load0
	movew	#TIMER_LENGTH, (m_timer_l).w

	lea	%pc@(logo_palette), %a0
	lea	m_mmd_palette, %a1
	moveq	#((logo_palette_end-logo_palette)/LONG_SIZE)-1, %d7
	1: /* for (long of logo_palette) { */
		movel	%a0@+, %a1@+

		dbf	%d7, 1b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	bset	#m_vint_z80load_bit, m_vint_call_mask
	bsrw	lib_palfade_init
	bsrw	main_wait_int
	bsrw	lib_palfade_inb

	2: /* while (m_timer) { */
		cmpiw	#0xd2, (m_timer_l).w
		bnes	1f /* if (m_timer == 0xD2) { */
			moveq	#1, %d0
			bsrw	logo_wait
		1: /* } */

		bsrw	main_wait_int

		tstw	(m_timer_l).w
		bnew	2b
	/* } */

	bsrw	lib_palfade_outb

	bset	#MCOM_FLAG_DONE_BIT, SUBCPU_E_FLG
	1: /* while (!scom_done) { */
		btst	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG
		beqs	1b
	/* } */
	bclr	#MCOM_FLAG_DONE_BIT, SUBCPU_E_FLG

	rts
/* ------------------------------------------------------- */
logo_z80_load:
	moveb	%a0@+, %a1@+
	moveb	%a0@+, %a1@+
	moveb	%a0@+, %a1@+
	moveb	%a0@+, %a1@+
	dbf	%d7, logo_z80_load
	rts
/* ------------------------ */
logo_vint:
	m_vint_open
	beqw	9f /* if (m_vint_open()) { */
		m_vint_vdpwait
		m_vint_dispon
		jsr	%pc@((lib_int_disable).w)
		m_vint_palload

		m_vint_finish
		m_vint_tick

		bclr	#m_vint_z80load_bit, m_vint_call_mask
		beqs	9f /* if (vint_z80load) { */
			movew	#0x100, Z80_RESET
			bsrw	lib_int_disable
			lea	%pc@(logo_z80base), %a0
			lea	Z80_RAM, %a1
			movew	#0x2b9, %d7
			bsrw	logo_z80_load
			lea	%pc@(logo_z80dat0), %a0
			lea	Z80_RAM+0xb00, %a1
			movew	#0x1e5, %d7
			bsrw	logo_z80_load
			lea	%pc@(logo_z80dat1), %a0
			lea	Z80_RAM+0x1900, %a1
			movew	#0xd, %d7
			bsrw	logo_z80_load
			movew	#0, Z80_RESET
			rorb	#8, %d0
			movew	#0x100, Z80_RESET
			bsrw	lib_int_enable
		/* } */
	9: /* } */

	m_vint_ret
/* ------------------------------------------------------- */
	.include "src/common/com_yield_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_subflag.s"
/* ------------------------------------------------------- */
logo_palette:
	.incbin	"data/logo/logo_palette.bin"
logo_palette_end:
/* ------------------------------------------------------- */
logo_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x00
	.byte	0x06, 0x70, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x00
	.byte	0x81, 0x39, 0x00, 0x02
	.byte	0x01, 0x00, 0x00
	.even
/* ------------------------------------------------------- */
logo_nemtiles:
	.incbin	"data/logo/logo_nemtiles.bin"
/* ------------------------------------------------------- */
logo_maps:
	.incbin	"data/logo/logo_maps.bin"
/* ------------------------------------------------------- */
logo_z80base:
	.incbin	"data/logo/logo_z80base.bin"
/* ------------------------------------------------------- */
logo_z80dat0:
	.incbin	"data/logo/logo_z80dat0.bin"
/* ------------------------------------------------------- */
logo_z80dat1:
	.incbin	"data/logo/logo_z80dat1.bin"
/* ------------------------------------------------------- */
