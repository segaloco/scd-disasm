	.include "src/inc/m_subreg.i"
	.section .text.m_lib
/* ------------------------------------------------------- */
/* subcpu_int - triggers a SubCPU level 2 interrupt, as    */
/*   an interrupt                                          */
/* subcpu_int_ret - an empty interrupt return, courtesy    */
/*   of subcpu_int                                         */
/* ------------------------------------------------------- */
	.globl	subcpu_int
	.globl	subcpu_int_ret
subcpu_int:
	bset	#SUBCPU_INT_BIT, SUBCPU_INT

subcpu_int_ret:
	rte
