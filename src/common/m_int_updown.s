	.ifndef	M_INT_UPDOWN
	.equ	M_INT_UPDOWN, 1

	.include "src/inc/z80.i"

	.include "src/inc/m_map.i"

	.macro	m_int_disable	sr_bup
		movew	%sr, (\sr_bup).w
		movew	#0x100, Z80_BUSREQ
		1: /* while () { */
			btst	#0, Z80_BUSREQ
			bnes	1b
		/* } */
	.endm

	.macro	m_int_enable	sr_bup
		movew	#0, Z80_BUSREQ
		movew	(\sr_bup).w, %sr
	.endm

	.endif	/* M_INT_UPDOWN */
