	.include "src/inc/vdp.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_cursor_move
lib_cursor_move:
	lea	VDP_DATA, %a2
	movew	%a0@+, %d2
	3: /* for (i = *a0; i > 0; i--) { */
		moveal	%a0@+, %a1
		movel	%a0@+, VDP_ADDR
		2: /* for (a = *a0; *a != 0xFF, a++) { */
			moveq	#0, %d1
			moveb	%a1@+, %d1
			cmpib	#0xff, %d1
			beqs	1f

			addw	%d0, %d1
			movew	%d1, %a2@

			bras	2b
		1: /* } */

		dbf	%d2, 3b
	/* } */

	rts
