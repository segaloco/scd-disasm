/* lib_palfade - handles palette fadeins and outs
    NOTE: _palfade_idx must be defined in the LD script for each
        MMD that utilizes this library
        it is a pointer to the in_progress flag for that MMD
 */
	.include "src/inc/m_map.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_palfade_outb
	.globl	lib_palfade_init
	.globl	lib_palfade_inb
lib_palfade_outb:
	moveb	#1, (_palfade_idx).w

	moveq	#7, %d6
	moveq	#0, %d0
	moveq	#1, %d1
	1: /* for (i = 7, j = 0; i > 0; i--, j += 2) { */
		bsrs	sub_ff3ce6

		addqw	#2, %d0
		dbf	%d6, 1b
	/* } */

	moveq	#7, %d6
	moveq	#0, %d0
	moveq	#5, %d1
	1: /* for (i = 7, j = 0; i > 0; i--, j += 2) { */
		bsrs	sub_ff3ce6

		addqw	#2, %d0
		dbf	%d6, 1b
	/* } */

	moveq	#7, %d6
	moveq	#0, %d0
	moveq	#9, %d1
	1: /* for (i = 7, j = 0; i > 0; i--, j += 2) { */
		bsrs	sub_ff3ce6

		addqw	#2, %d0
		dbf	%d6, 1b
	/* } */

	rts
/* ------------------------ */
sub_ff3ce6:
	lea	m_mmd_palette, %a1
	moveq	#0x40-1, %d7
	2: /* for (i = 0x40-1; i > 0; i--) { */
		movew	%a1@, %d2
		rolw	#1, %d2
		rorw	%d1, %d2
		movew	%d2, %d3
		andiw	#0xe, %d2
		andiw	#0xeee0, %d3
		subw	%d0, %d2
		bccs	1f /* if () { */
			moveq	#0, %d2
		1: /* } */
		
		orw	%d3, %d2
		rorw	#1, %d2
		rolw	%d1, %d2
		movew	%d2, %a1@+

		dbf	%d7, 2b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	braw	main_wait_int
/* ------------------------ */
lib_palfade_init:
	lea	m_mmd_palette, %a1
	lea	m_mmd_palette+0x80, %a2
	moveq	#0, %d1
	moveq	#0x20-1, %d7
	1: /* for (i = 0x20-1; i > 0; i--) { */
		movel	%a1@, %a2@+
		movel	%d1, %a1@+

		dbf	%d7, 1b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	rts
/* ------------------------ */
lib_palfade_inb:
	movew	#7, %d6
	moveq	#0, %d0
	moveq	#9, %d1
	1: /* for (i = 7, j = 0; i > 0; i--, j += 2) { */
		bsrw	sub_ff3d76
		addqw	#2, %d0
		dbf	%d6, 1b
	/* } */

	movew	#7, %d6
	moveq	#0, %d0
	moveq	#5, %d1
	1: /* for (i = 7, j = 0; i > 0; i--, j += 2) { */
		bsrw	sub_ff3d76
		addqw	#2, %d0
		dbf	%d6, 1b
	/* } */

	movew	#7, %d6
	moveq	#0, %d0
	moveq	#1, %d1
	1: /* for (i = 7, j = 0; i > 0; i--, j += 2) { */
		bsrw	sub_ff3d76
		addqw	#2, %d0
		dbf	%d6, 1b
	/* } */

	moveb	#0, (_palfade_idx).w
	rts
/* ------------------------ */
sub_ff3d76:
	lea	m_mmd_palette,      %a1
	lea	m_mmd_palette+0x80, %a2
	moveq	#0x3f, %d7
	2: /* for (i = 0x40-1; i > 0; i--) { */
		movew	%a2@+, %d2
		movew	%a1@, %d3
		rolw	#1, %d2
		rolw	#1, %d3
		rorw	%d1, %d2
		rorw	%d1, %d3
		andiw	#0xe, %d2
		andiw	#0xeee0, %d3
		cmpw	%d0, %d2
		blss	1f /* if () { */
			movew	%d0, %d2
		1: /* } */

		orw	%d3, %d2
		rolw	%d1, %d2
		rorw	#1, %d2
		movew	%d2, %a1@+

		dbf	%d7, 2b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	braw	main_wait_int
/* ------------------------------------------------------- */
