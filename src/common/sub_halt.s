/* ------------------------------------------------------- */
/* sub_halt - a tight loop, sectioned for SubCPU           */
/* ------------------------------------------------------- */
	.section .text.s_lib
/* ------------------------------------------------------- */
	.globl	sub_halt
sub_halt:
	1: /* for (;;) { */
		nop
		nop
		bras	1b
	/* } * /
/* ------------------------------------------------------- */
