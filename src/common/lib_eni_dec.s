	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_eni_dec
lib_eni_dec:
	moveml	%d0-%d7/%a1-%a5, %sp@-

	moveaw	%d0, %a3
	moveb	%a0@+, %d0
	extw	%d0
	moveaw	%d0, %a5
	moveb	%a0@+, %d4
	lslb	#3, %d4
	moveaw	%a0@+, %a2
	addaw	%a3, %a2
	moveaw	%a0@+, %a4
	addaw	%a3, %a4
	moveb	%a0@+, %d5
	aslw	#8, %d5
	moveb	%a0@+, %d5
	moveq	#0x10, %d6

next_loop:
	moveq	#7, %d0
	movew	%d6, %d7
	subw	%d0, %d7
	movew	%d5, %d1
	lsrw	%d7, %d1
	andiw	#0x7f, %d1
	movew	%d1, %d2
	cmpiw	#0x40, %d1
	bccs	1f /* if () { */
		moveq	#6, %d0
		lsrw	#1, %d2
	1: /* } */

	bsrw	sub_ff4060
	andiw	#0x0f, %d2
	lsrw	#4, %d1
	addw	%d1, %d1
	jmp	%pc@(tbl_eni_loops, %d1:w)

loop_1:
	1: /* for (i; i > 0; i--) { */
		movew	%a2, %a1@+
		addqw	#1, %a2
		dbf	%d2, 1b
	/* } */
	bras	next_loop

loop_2:
	1: /* for (i; i > 0; i--) { */
		movew	%a4, %a1@+
		dbf	%d2, 1b
	/* } */
	bras	next_loop

loop_3:
	bsrw	sub_ff3fb0
	1: /* for (i; i > 0; i--) { */
		movew	%d1, %a1@+
		dbf	%d2, 1b
	/* } */
	bras	next_loop

loop_4:
	bsrw	sub_ff3fb0
	1: /* for (i; i > 0; i--) { */
		movew	%d1, %a1@+
		addqw	#1, %d1
		dbf	%d2, 1b
	/* } */
	bras	next_loop

loop_5:
	bsrw	sub_ff3fb0
	1: /* for (i; i > 0; i--) { */
		movew	%d1, %a1@+
		subqw	#1, %d1
		dbf	%d2, 1b
	/* } */
	bras	next_loop

loop_6:
	cmpiw	#0xf, %d2
	beqs	2f

	1: /* for (i; i > 0; i--) { */
		bsrw	sub_ff3fb0
		movew	%d1, %a1@+
		dbf	%d2, 1b
	/* } */
	bras	next_loop
/* ------------- */
tbl_eni_loops:
	bras	loop_1
	bras	loop_1
	bras	loop_2
	bras	loop_2
	bras	loop_3
	bras	loop_4
	bras	loop_5
	bras	loop_6
/* ------------- */
2:
	subqw	#1, %a0
	cmpiw	#0x10, %d6
	bnes	1f /* if (d6 == 0x10) { */
		subqw	#1, %a0
	1: /* } */

	movew	%a0, %d0
	lsrw	#1, %d0
	bccs	1f /* if (a0 % 1) { */
		addqw	#1, %a0
	1: /* } */

	moveml	%sp@+, %d0-%d7/%a1-%a5
	rts
/* ------------------------ */
sub_ff3fb0:
	movew	%a3, %d3
	moveb	%d4, %d1
	addb	%d1, %d1
	bccs	1f
	subqw	#1, %d6
	btst	%d6, %d5
	beqs	1f /* if () { */
		oriw	#0x8000, %d3
	1: /* } */

	addb	%d1, %d1
	bccs	1f
	subqw	#1, %d6
	btst	%d6, %d5
	beqs	1f /* if () { */
		addiw	#0x4000, %d3
	1: /* } */

	addb	%d1, %d1
	bccs	1f
	subqw	#1, %d6
	btst	%d6, %d5
	beqs	1f /* if () { */
		addiw	#0x2000, %d3
	1: /* } */

	addb	%d1, %d1
	bccs	1f
	subqw	#1, %d6
	btst	%d6, %d5
	beqs	1f /* if () */
		oriw	#0x1000, %d3
	1: /* } */

	addb	%d1, %d1
	bccs	1f
	subqw	#1, %d6
	btst	%d6, %d5
	beqs	1f /* if () { */
		oriw	#0x800, %d3
	1: /* } */

	movew	%d5, %d1
	movew	%d6, %d7
	subw	%a5, %d7
	bccs	1f

	movew	%d7, %d6
	addiw	#0x10, %d6
	negw	%d7
	lslw	%d7, %d1
	moveb	%a0@, %d5
	rolb	%d7, %d5
	addw	%d7, %d7
	andw	%pc@(lib_eni_mask-2, %d7:w), %d5
	addw	%d5, %d1

2:
	movew	%a5, %d0
	addw	%d0, %d0
	andw	%pc@(lib_eni_mask-2, %d0:w), %d1
	addw	%d3, %d1
	moveb	%a0@+, %d5
	lslw	#8, %d5
	moveb	%a0@+, %d5
	rts

1:
	beqs	1f

	lsrw	%d7, %d1
	movew	%a5, %d0
	addw	%d0, %d0
	andw	%pc@(lib_eni_mask-2, %d0:w), %d1
	addw	%d3, %d1
	movew	%a5, %d0
	bras	sub_ff4060

1:
	moveq	#0x10, %d6
	bras	2b
/* ------------------------ */
lib_eni_mask:
	.word	0x0001, 0x0003, 0x0007, 0x000f
	.word	0x001f, 0x003f, 0x007f, 0x00ff
	.word	0x01ff, 0x03ff, 0x07ff, 0x0fff
	.word	0x1fff, 0x3fff, 0x7fff, 0xffff
/* ------------------------ */
sub_ff4060:
	subw	%d0, %d6
	cmpiw	#9, %d6
	bccs	9f /* if () { */
		addqw	#8, %d6
		aslw	#8, %d5
		moveb	%a0@+, %d5
	9: /* } */

	rts
/* ------------------------------------------------------- */
