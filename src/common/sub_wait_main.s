/* ------------------------------------------------------- */
/* sub_wait_main - SubCPU wait for command from MainCPU    */
/* ------------------------------------------------------- */
	.include "src/inc/s_subreg.i"
	.section .text.s_lib
/* ------------------------------------------------------- */
	.globl	sub_wait_main
	.globl	_sub_wait_main
sub_wait_main:
	bset	#SUBCPU_RET_BIT, SUBCPU_MEMMODE

_sub_wait_main:
	movew	SUBCPU_CMD_BUFFER, SUBCPU_STAT_BUFFER
	1: /* while (subcpu_cmd) { */
		tstw	SUBCPU_CMD_BUFFER
		bnes	1b
	/* } */

	movew	#0, SUBCPU_STAT_BUFFER
	rts
/* ------------------------------------------------------- */
