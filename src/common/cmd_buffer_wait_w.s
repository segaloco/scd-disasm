	.ifndef	CMD_BUFFER_WAIT_W
	.equ	CMD_BUFFER_WAIT_W, 1

/* ------------------------------------------------------- */
/* cmd_buffer_wait_w - send a command, await all clear     */
/*  from SubCPU                                            */
/* ------------------------------------------------------- */
	.macro	cmd_buffer_wait_w cmd_buffer, stat_buffer

	movew	%d0, \cmd_buffer
	1: /* while (!stat_buffer) { */
		tstw	\stat_buffer
		beqs	1b
	/* } */

	movew	#0, \cmd_buffer
	1: /* while (stat_buffer) { */
		tstw	\stat_buffer
		bnes	1b
	/* } */

	.endm

	.endif /* CMD_BUFFER_WAIT_W */
