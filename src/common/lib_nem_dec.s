	.include "src/inc/vdp.i"

	.include "src/inc/m_map.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_nem_vdpdec
	.globl	lib_nem_memdec
lib_nem_vdpdec:
	moveml	%d0-%a1/%a3-%a5, %sp@-

	lea	(loc_ff3e74).l, %a3
	lea	VDP_DATA,   %a4
	bras	lib_nem_dec
/* ------------------------ */
lib_nem_memdec:
	moveml	%d0-%a1/%a3-%a5, %sp@-

	lea	(loc_ff3e8a).l, %a3
/* ------------------------ */
lib_nem_dec:
	lea	(_nem_dec_loc).w, %a1
	movew	%a0@+, %d2
	lslw	#1, %d2
	bccs	1f /* if (*a0 & 0x8000) { */
		addaw	#0xa, %a3
	1: /* } */

	lslw	#2, %d2
	moveaw	%d2, %a5
	moveq	#8, %d3
	moveq	#0, %d2
	moveq	#0, %d4
	jsr	%pc@(sub_ff3ea0)
	moveb	%a0@+, %d5
	aslw	#8, %d5
	moveb	%a0@+, %d5
	movew	#0x10, %d6
	bsrs	sub_ff3dfc

	moveml	%sp@+, %d0-%a1/%a3-%a5
	rts
/* ------------------------ */
sub_ff3dfc:
	movew	%d6, %d7
	subqw	#8, %d7
	movew	%d5, %d1
	lsrw	%d7, %d1
	cmpib	#0xfc, %d1
	bccs	2f /* if () { */
		andiw	#0xff, %d1
		addw	%d1, %d1
		moveb	%a1@(0, %d1:w), %d0
		extw	%d0
		subw	%d0, %d6
		cmpiw	#9, %d6
		bccs	1f /* if () { */
			addqw	#8, %d6
			aslw	#8, %d5
			moveb	%a0@+, %d5
		1: /* } */

		moveb	%a1@(1, %d1:w), %d1
		movew	%d1, %d0
		andiw	#0xf, %d1
		andiw	#0xf0, %d0

	loc_ff3e32:
		lsrw	#4, %d0
		3: /* for (i /= 16; i > 0; i--) { */
			lsll	#4, %d4
			orb	%d1, %d4
			subqw	#1, %d3
			bnes	4f /* if () { */
				jmp	%a3@

			_next_row:
				moveq	#0, %d4
				moveq	#8, %d3
			4: /* } */
			dbf	%d0, 3b
		/* } */

		bras	sub_ff3dfc
	2: /* } else { */
		subqw	#6, %d6
		cmpiw	#9, %d6
		bccs	1f /* if () { */
			addqw	#8, %d6
			aslw	#8, %d5
			moveb	%a0@+, %d5
		1: /* } */

		subqw	#7, %d6
		movew	%d5, %d1
		lsrw	%d6, %d1
		movew	%d1, %d0
		andiw	#0x0f, %d1
		andiw	#0x70, %d0
		cmpiw	#9, %d6
		bccs	loc_ff3e32

		addqw	#8, %d6
		aslw	#8, %d5
		moveb	%a0@+, %d5
		bras	loc_ff3e32
	/* } */
/* ------------------------ */
loc_ff3e74:
	movel	%d4, %a4@
	subqw	#1, %a5
	movew	%a5, %d4
	bnes	_next_row

	rts

loc_ff3e7e:
	eorl	%d4, %d2
	movel	%d2, %a4@
	subqw	#1, %a5
	movew	%a5, %d4
	bnes	_next_row

	rts
/* ------------------------ */
loc_ff3e8a:
	movel	%d4, %a4@+
	subqw	#1, %a5
	movew	%a5, %d4
	bnes	_next_row

	rts

loc_ff3e94:
	eorl	%d4, %d2
	movel	%d2, %a4@+
	subqw	#1, %a5
	movew	%a5, %d4
	bnes	_next_row

	rts
/* ------------------------ */
sub_ff3ea0:
	moveb	%a0@+, %d0

9:
	cmpib	#0xff, %d0
	bnes	1f /* if () { */
		rts
	1: /* } */

	movew	%d0, %d7
	2: /* while () { */
		moveb	%a0@+, %d0
		cmpib	#0x80, %d0
		bccs	9b

		moveb	%d0, %d1
		andiw	#0xf, %d7
		andiw	#0x70, %d1
		orw	%d1, %d7
		andiw	#0xf, %d0
		moveb	%d0, %d1
		lslw	#8, %d1
		orw	%d1, %d7
		moveq	#8, %d1
		subw	%d0, %d1
		bnes	1f /* if (d0 == 8) { */
			moveb	%a0@+, %d0
			addw	%d0, %d0
			movew	%d7, %a1@(0, %d0:w)
			bras	2b
		1: /* } else { */
			moveb	%a0@+, %d0
			lslw	%d1, %d0
			addw	%d0, %d0
			moveq	#1, %d5
			lslw	%d1, %d5
			subqw	#1, %d5
			1: /* for (--i; i > 0; i--) { */
				movew	%d7, %a1@(0, %d0:w)
				addqw	#2, %d0
				dbf	%d5, 1b
			/* } */

			bras	2b
		/* } */
	/* } */
/* ------------------------------------------------------- */
