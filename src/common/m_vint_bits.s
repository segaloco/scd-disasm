	.ifndef	M_VINT_BITS
	.equ	M_VINT_BITS, 1

	.include "src/inc/vdp.i"

	.include "src/inc/m_subreg.i"

	.include "src/inc/m_map.i"

	.include "src/common/m_vdp_dma.s"
/* ------------------------------------------------------- */
/* m_vint_open - start a vertical interrupt                */
/*  typically a beq to m_vint_ret is performed immediately */
/*  following these instructions to skip the routine       */
/*  entirely when not needed                               */
	.macro	m_vint_open

	moveml	%d0-%fp, %sp@-
	moveb	#SUBCPU_INT_MASK, SUBCPU_INT
	bclr	#m_vint_req_bit, m_vint_call_mask

    .endm
/* ------------------------------------------------------- */
/* m_vint_ret - return from vertical interrupt             */
	.macro	m_vint_ret

	moveml	%sp@+, %d0-%fp
	rte

	.endm
/* ------------------------------------------------------- */
/* m_vint_palload - load an awaiting palette               */
	.macro	m_vint_palload

	bclr	#m_vint_palfade_bit, m_vint_call_mask
	beqs	1f /* if (m_vint_palfade) { */
		m_vdp_dma_c	0x0000, m_mmd_palette, 0x40
	1: /* } */

    .endm
/* ------------------------------------------------------- */
/* m_vint_vdpwait - await VDP                              */
	.macro	m_vint_vdpwait

	movew	VDP_CTRL, %d0

    .endm
/* ------------------------------------------------------- */
/* m_vint_dispon - enable the display w/ mode snapshot     */
	.macro	m_vint_dispon

	bset	#m_vdpmode1_display_bit, m_vdpmode1_cpy+1
	movew	m_vdpmode1_cpy, VDP_CTRL

    .endm
/* ------------------------------------------------------- */
/* m_vint_finish - finishing touches on typical            */
/*  vint execution                                         */
	.macro	m_vint_finish

	jsr	%pc@((lib_joypad_read).w)
	bsrw	lib_int_enable

    .endm
/* ------------------------------------------------------- */
/* m_vint_tick - finishing touches on typical              */
/*  vint execution                                         */
	.macro	m_vint_tick

	tstw	(m_timer_l).w
	beqs	1f /* if (m_timer_l) { */
		subqw	#1, (m_timer_l).w
	1: /* } */

	addqw	#1, (SVAR_W_fffa44_l).w

    .endm
/* ------------------------------------------------------- */
	.endif	/* M_VINT_BITS */
