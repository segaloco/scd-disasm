	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_memset_128
	.globl	lib_memset_64
	.globl	lib_memset_48
lib_memset_128:
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+

lib_memset_64:
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+

lib_memset_48:
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	movel	%d1, %a1@+
	rts
/* ------------------------------------------------------- */
