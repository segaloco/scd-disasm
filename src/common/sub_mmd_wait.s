/* ------------------------------------------------------- */
/* sub_mmd_wait - awaits on the second word in the buffer, */
/* used in the logo and time attack modes                  */
/* for synchronization                                     */
/* ------------------------------------------------------- */
	.include    "src/inc/s_subreg.i"

sub_mmd_wait:
	movew	(SUBCPU_CMD_BUFFER_W+2).w, (SUBCPU_STAT_BUFFER_W+2).w
	1: /* while ((SUBCPU_CMD_BUFFER_W+2).w) { */
		tstw	(SUBCPU_CMD_BUFFER_W+2).w
		bnes	1b
	/* } */

	movew	#0, (SUBCPU_STAT_BUFFER_W+2).w
	rts
