/* ------------------------------------------------------- */
/* sub_wait_dmna - wait for the DMNA bit to be set         */
/* ------------------------------------------------------- */
	.include "src/inc/s_subreg.i"
	.section .text.s_lib
/* ------------------------------------------------------- */
	.globl	sub_wait_dmna
sub_wait_dmna:
	btst	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
	beqs	sub_wait_dmna

	rts
/* ------------------------------------------------------- */
