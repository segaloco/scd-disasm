	.include "src/inc/m_subreg.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
com_wait_subcpu:
	1: /* while (!scom_busy) { */
		btst	#SCOM_FLAG_BUSY_BIT, SUBCPU_SCOM_FLG
		beqs	1b
	/* } */

	rts
/* ------------------------------------------------------- */
com_wait_subcpu_init:
	1: /* while (scom_busy) { */
		btst	#SCOM_FLAG_BUSY_BIT, SUBCPU_SCOM_FLG
		bnes	1b
	/* } */

	rts
