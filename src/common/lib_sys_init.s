	.include "src/inc/vdp.i"

	.include "src/inc/m_map.i"

	.include "src/common/lib_hw_init.s"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_sys_init
lib_sys_init:
	m_lib_hw_init

	movel	#VDP_VRAM_WRITE, VDP_ADDR
	lea	VDP_DATA, %a0
	moveq	#0, %d0
	movew	#(VDP_VRAM_SIZE/0x10)-1, %d7
	1: /* for (quadword of vram) { */
		movel	%d0, %a0@
		movel	%d0, %a0@
		movel	%d0, %a0@
		movel	%d0, %a0@

		dbf	%d7, 1b
	/* } */

	movel	#VDP_VSRAM_WRITE, VDP_ADDR
	movel	#0, VDP_DATA
	bsrw	lib_int_enable
	movew	#VDPREG_NT_MODE_2+0x34, m_vdpmode1_cpy
	rts
/* ------------------------------------------------------- */
