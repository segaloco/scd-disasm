	.ifndef	M_VDP_DMA
	.equ	M_VDP_DMA, 1

	.include "src/inc/vdp.i"
/* ------------------------------------------------------- */
/* m_vdp_dma - setup for a VDP DMA run                     */
	.macro	m_vdp_dma	dest, src, len, addr_bits

vdp_addr_mask	= (((\dest&0x3FFF)<<16)+((\dest>>14)&0x03))
vdp_addr_mask_h	= (vdp_addr_mask>>16)
vdp_addr_mask_l	= (vdp_addr_mask&0xFFFF)

	lea	VDP_CTRL, %fp

	movel	#(((VDPREG_DMALEN_L+(\len&0xFF))<<16)+(VDPREG_DMALEN_H+((\len&0xFF00)>>8))), %fp@

	movel	#(((VDPREG_DMASRC_L+((\src>>1)&0xFF))<<16)+(VDPREG_DMASRC_M+((\src>>9)&0xFF))), %fp@
	movew	#(VDPREG_DMASRC_H+((\src>>17)&0x7F)), %fp@

	movew	#(VDP_ADDR_DMA_MASK_H)+(\addr_bits>>16)+(vdp_addr_mask_h), %fp@
	movew	#(VDP_ADDR_DMA_MASK_L)+(\addr_bits&0xFFFF)+(vdp_addr_mask_l), %sp@-
	movew	%sp@+, %fp@

	movel	#(\addr_bits)+(vdp_addr_mask), %fp@

	movew	\src, VDP_DATA

	.endm
/* ------------------------------------------------------- */
/* m_vdp_dma_v - setup for a VDP VRAM DMA run              */
	.macro	m_vdp_dma_v	dest, src, len
	
	m_vdp_dma	\dest, \src, \len, VDP_VRAM_WRITE

	.endm
/* ------------------------------------------------------- */
/* m_vdp_dma_c - setup for a VDP CRAM DMA run              */
	.macro	m_vdp_dma_c	dest, src, len
	
	m_vdp_dma	\dest, \src, \len, VDP_CRAM_WRITE

	.endm

	.endif	/* M_VDP_DMA */
