	.include "src/inc/m_subreg.i"
	
	.include "src/common/m_joypad_proc.s"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_joypad_read
lib_joypad_read:
	lea	CMD_BUFFER_CTRL-1, %a0
	lea	0xa10003, %a1

	m_joypad_proc

	rts
/* ------------------------------------------------------- */
