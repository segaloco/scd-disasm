	.ifndef	M_OBJ_ENG
	.equ	M_OBJ_ENG, 1

	.include "src/inc/obj_eng.i"
/* ------------------------------------------------------- */
/* m_obj_eng_enter - enter the object engine               */
	.macro	m_obj_eng_enter

	moveb	#0, SVAR_L_fffa00
	movel	#SVAR_L_ffe000_s, SVAR_L_fffa00+0xa0

    .endm
/* ------------------------------------------------------- */
/* m_obj_eng_exit - exit the object engine                 */
	.macro	m_obj_eng_exit

	moveal	SVAR_L_fffa00+0xa0, %a0
	movel	#0, %a0@

    .endm
/* ------------------------------------------------------- */
/* m_obj_stat_start - start an object cycle, returning     */
/*  status, typically pairs with m_obj_stat_ret            */
	.macro	m_obj_stat_start

	moveal	%a0@(OBJ_DATAPTR), %a2
	moveq	#0, %d0
	moveb	%a0@(OBJ_ANI_FRAME), %d0
	addw	%d0, %d0
	addw	%d0, %d0
	moveal	%a2@(0, %d0:w), %a2
	moveb	%a2@+, %d1
	moveb	%a2@+, %d2
	btst	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	bnes	1f

	subqb	#1, %a0@(OBJ_ANI_TIME)
	bpls	1f

	moveb	%d2, %a0@(OBJ_ANI_TIME)
	addqb	#1,  %a0@(OBJ_ANI_CUE)
	cmpb	%a0@(OBJ_ANI_CUE), %d1
	bhis	1f

	moveb	#0, %a0@(OBJ_ANI_CUE)

1:
	btst	#OBJ_STAT_INACTIVE_BIT, %a0@(OBJ_STAT)

    .endm
/* ------------------------------------------------------- */
/* m_obj_stat_ret - finish an object cycle                 */
	.macro	m_obj_stat_ret

	movel	%a4, SVAR_L_fffa00+0xa0
	rts

    .endm
/* ------------------------------------------------------- */
/* m_obj_ani_start - prepare for animation handling        */
/*  high bit in %d7 indicates whether we skip              */
	.macro	m_obj_ani_start

	movew	%a0@(OBJ_HPOS), %d4
	movew	%a0@(OBJ_VPOS), %d3
	moveq	#0, %d0
	moveb	%a0@(OBJ_ANI_CUE), %d0
	addw	%d0, %d0
	addw	%d0, %d0
	moveal	%a2@(0, %d0:w), %a3
	moveq	#0, %d7
	moveb	%a3@+, %d7

    .endm
/* ------------------------------------------------------- */
/* m_obj_ani_loop_start - start the animation loop         */
/*  pairs with m_obj_ani_loop_next, assumes 9f             */
/*  is m_obj_stat_ret                                      */
	.macro	m_obj_ani_loop_start

	moveal	SVAR_L_fffa00+0xa0, %a4
	movew	%a0@(OBJ_W_OFF_4), %d5

2:
	cmpib	#0x50, SVAR_L_fffa00
	bccs	9f

	moveb	%a3@+, %d0
	extw	%d0
	addw	%d3, %d0

    .endm
/* ------------------------------------------------------- */
/* m_obj_ani_loop_main - main loop body                    */
	.macro	m_obj_ani_loop_main

	movew	%d0, %a4@+
	moveb	%a3@+, %a4@+
	addqb	#1, SVAR_L_fffa00
	moveb	SVAR_L_fffa00, %a4@+
	moveb	%a3@+, %d0
	lslw	#8, %d0
	moveb	%a3@+, %d0
	addw	%d5, %d0
	movew	%d0, %a4@+
	moveb	%a3@+, %d0
	extw	%d0
	addw	%d4, %d0

    .endm
/* ------------------------------------------------------- */
/* m_obj_ani_loop_end - end the animation loop             */
	.macro	m_obj_ani_loop_end

	andiw	#0x1ff, %d0
	bnes	1f

	addqw	#1, %d0

1:
	movew	%d0, %a4@+

    .endm
/* ------------------------------------------------------- */
/* m_obj_ani_loop_next - go the next iteration             */
/*  pairs with m_obj_ani_loop_start                        */
	.macro	m_obj_ani_loop_next

	dbf	%d7, 2b

    .endm
/* ------------------------------------------------------- */
/* m_obj_ani_cycle - cycle an object's animation           */
	.macro	m_obj_ani_cycle

	moveb	%d0, %a0@(OBJ_ANI_FRAME)
	moveq	#0, %d0
	moveb	%d0, %a0@(OBJ_ANI_CUE)
	moveal	%a0@(OBJ_DATAPTR), %fp
	moveb	%a0@(OBJ_ANI_FRAME), %d0
	addw	%d0, %d0
	addw	%d0, %d0
	moveal	%fp@(0, %d0:w), %fp
	moveb	%fp@+, %d0
	moveb	%fp@,  %a0@(OBJ_ANI_TIME)
	moveb	%fp@+, %a0@(OBJ_ANI_LEN)

    .endm
/* ------------------------------------------------------- */
	.endif	/* M_OBJ_ENG */
