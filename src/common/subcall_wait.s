	.include "src/inc/m_subreg.i"

	.include "src/common/cmd_buffer_wait_w.s"
	.section .text.m_lib
/* ------------------------------------------------------- */
/* subcall_wait - send a SubCPU command and await its      */
/*   completion                                            */
/* ------------------------------------------------------- */
	.globl	subcall_wait
subcall_wait:
	cmd_buffer_wait_w CMD_BUFFER_SUBCALL, STAT_BUFFER_SUBCALL
	rts
