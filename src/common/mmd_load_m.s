	.ifndef	MMD_LOAD_M
	.equ	MMD_LOAD_M, 1

	.include "src/inc/m_vector.i"

	.include "src/inc/m_mmd.i"
/* ------------------------------------------------------- */
/* mmd_load_m - common portion of MMD handling routine     */
/* ------------------------------------------------------- */
	.macro	mmd_load_m

	movel	MMD_BASEADDR, %d0
	beqs	2f /* if (MMD_BASEADDR) { */
		moveal	%d0, %a2
		lea	MMD_DATA, %a1
		movew	MMD_SIZE, %d7
		1: /* for (long of MMD_DATA) { */
			movel	%a1@+, %a2@+
			dbf	%d7, 1b
		/* } */
	2: /* } */

	movel	MMD_HINT, %d0
	beqs	1f /* if (MMD_HINT) { */
		movel	%d0, M_VECTOR_HINT
	1: /* } */

	movel	MMD_VINT, %d0
	beqs	1f /* if (MMD_VINT) { */
		movel	%d0, M_VECTOR_VINT
	1: /* } */

	.endm

	.endif	/* MMD_LOAD_M */
