	.ifndef	LIB_HW_INIT
	.equ	LIB_HW_INIT, 1

	.include "src/inc/vdp.i"

/* ------------------------------------------------------- */
/* m_lib_hw_init - VDP register and control init bits      */

	.macro	m_lib_hw_init
		movew	#VDPREG_MASK, %d0
		moveq	#18, %d7
		1: /* for (i = VDPREG_MASK, j = 18; j > 0; i += 0x100, j--) { */
			moveb	%a0@+,  %d0
			movew	%d0,    VDP_CTRL

			addiw	#0x100, %d0
			dbf	%d7, 1b
		/* } */

		moveq	#0x40, %d0
		moveb	%d0, 0xa10009
		moveb	%d0, 0xa1000b
		moveb	%d0, 0xa1000d
		moveb	#0xc0, 0xa10003
		bsrw	lib_int_disable
	.endm

	.endif	/* LIB_HW_INIT */
