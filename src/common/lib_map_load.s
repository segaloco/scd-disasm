	.include "src/inc/vdp.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_map_load0
	.globl	lib_map_load1
lib_map_load0:
	lea	VDP_ADDR, %a2
	lea	VDP_DATA, %a3
	movel	#0x800000, %d4
	1: /* for (i; i > 0; i--) { */
		movel	%d0, %a2@

		movew	%d1, %d3
		2: /* for (i = d1; i > 0; i--) { */
			movew	%a1@+, %a3@
			dbf	%d3, 2b
		/* } */

		addl	%d4, %d0
		dbf	%d2, 1b
	/* } */

	rts
/* ------------------------ */
lib_map_load1:
	lea	VDP_ADDR, %a2
	lea	VDP_DATA, %a3
	movel	#0x1000000, %d4
	1: /* for (i; i > 0; i--) { */
		movel	%d0, %a2@

		movew	%d1, %d3
		2: /* for (i = d1; i > 0; i--) { */
			movew	%a1@+, %a3@
			dbf	%d3, 2b
		/* } */

		addl	%d4, %d0
		dbf	%d2, 1b
	/* } */

	rts
/* ------------------------------------------------------- */
