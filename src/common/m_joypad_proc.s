	.ifndef	M_JOYPAD_PROC
	.equ	M_JOYPAD_PROC, 1

/* ------------------------------------------------------- */
/* m_joypad_proc - process to reduce joypad data into      */
/*   the provided word pointed to by %a0 from the          */
/*   control port pointed to by %a1                        */
	.macro	m_joypad_proc

		moveb	#0, %a1@
		tstw	%a0@
		moveb	%a1@, %d0
		lslb	#2, %d0
		andib	#0xc0, %d0
		moveb	#0x40, %a1@
		tstw	%a0@
		moveb	%a1@, %d1
		andib	#0x3f, %d1
		orb	%d1, %d0
		notb	%d0
		moveb	%d0, %d1
		moveb	%a0@, %d2
		eorb	%d2, %d0
		moveb	%d1, %a0@+
		andb	%d1, %d0
		moveb	%d0, %a0@+

	.endm

	.endif	/* M_JOYPAD_PROC */
