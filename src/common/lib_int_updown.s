	.include "src/inc/z80.i"

	.include "src/inc/m_map.i"

	.include "src/common/m_int_updown.s"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_int_disable
	.globl	lib_int_enable
lib_int_disable:
	m_int_disable	m_sr_backup_l
	rts
/* ------------------------ */
lib_int_enable:
	m_int_enable	m_sr_backup_l
	rts
/* ------------------------------------------------------- */
