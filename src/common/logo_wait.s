	.include "src/inc/m_subreg.i"

	.include "src/common/cmd_buffer_wait_w.s"
	.section .text.m_lib
/* ------------------------------------------------------- */
/* logo_wait - wait for a flag cycle from the logo_s       */
/*   application                                           */
/* ------------------------------------------------------- */
	.globl	logo_wait
logo_wait:
	cmd_buffer_wait_w CMD_BUFFER_LOGO_FLAG, STAT_BUFFER_LOGO_FLAG

	rts
