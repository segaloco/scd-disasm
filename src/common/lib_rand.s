	.include "src/inc/m_map.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
	.equ	RAND_SEED,  0x2a6d365a
	.globl	lib_rand
lib_rand:
	movel	m_lib_rand_val, %d1
	bnes	1f /* if (!m_lib_rand_val) { */
		movel	#RAND_SEED, %d1
	1: /* } */
	movel	%d1, %d0

	asll	#2, %d1
	addl	%d0, %d1
	asll	#3, %d1
	addl	%d0, %d1
	movew	%d1, %d0
	swap	%d1
	addw	%d1, %d0
	movew	%d0, %d1
	swap	%d1
	movel	%d1, m_lib_rand_val
	rts
