	.include "src/inc/m_map.i"
    
	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	main_wait_int
main_wait_int:
	bset	#m_vint_req_bit, m_vint_call_mask
	movew	#0x2500, %sr

1:
	btst	#m_vint_req_bit, m_vint_call_mask
	bnes	1b

	rts
/* ------------------------------------------------------- */
