	.include "src/inc/m_subreg.i"

	.section .text.m_lib
/* ------------------------------------------------------- */
com_yield_subcpu:
	btst	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
	bnes	9f /* if (!subcpu_dmna) { */
		bset	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
		1: /* while (!subcpu_dmna) { */
			btst	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
			beqs	1b
		/* } */
	9: /* } */

	rts
