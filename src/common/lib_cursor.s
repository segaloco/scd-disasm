	.section .text.m_lib
/* ------------------------------------------------------- */
	.globl	lib_cursor_up_w
	.globl	lib_cursor_down_w
	.globl	lib_cursor_up_b
	.globl	lib_cursor_down_b
lib_cursor_up_w:
	movew	%a1@, %d0
	subqw	#1, %d0
	cmpw	%d6, %d0
	bges	1f /* if (--*a1 < d6) { */
		movew	%d7, %d0
	1: /* } */

	movew	%d0, %a1@
	rts
/* ------------------------ */
lib_cursor_down_w:
	movew	%a1@, %d0
	addqw	#1, %d0
	cmpw	%d7, %d0
	blss	1f /* if (++*a1 >= d7) { */
		movew	%d6, %d0
	1: /* } */

	movew	%d0, %a1@
	rts
/* ------------------------ */
lib_cursor_up_b:
	moveb	%a1@, %d0
	subqb	#1, %d0
	cmpb	%d6, %d0
	bges	1f /* if (--*a1 < d6) { */
		moveb	%d7, %d0
	1: /* } */

	moveb	%d0, %a1@
	rts
/* ------------------------ */
lib_cursor_down_b:
	moveb	%a1@, %d0
	addqb	#1, %d0
	cmpb	%d7, %d0
	blss	1f /* if (++*a1 >= d7) { */
		moveb	%d6, %d0
	1: /* } */

	moveb	%d0, %a1@
	rts
/* ------------------------------------------------------- */
