/* ------------------------------------------------------- */
/* ip_main - initial program                               */
/*  only job is to load IPX___.MMD and enter it            */
/* ------------------------------------------------------- */
	.include "src/inc/m_bank.i"
	.include "src/inc/m_subreg.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/subcall.i"
	.include "src/inc/m_mmd.i"
	.include "src/inc/m_map.i"

	.include "src/common/mmd_load_m.s"
/* ------------------------------------------------------- */
	.section .text.cd_head_ip
/* ------------------------------------------------------- */
	.equ	BOOTSTRAP_SIZE, ip_bootstrap_end-ip_bootstrap
/* ------------------------ */
ip_main:
	movel	#subcpu_int,      M_VECTOR_VINT
	bset	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE

	lea	SUBCPU_CMD_BUFFER, %a0
	moveq	#0,	%d0
	moveb	%d0,	%a0@(-2)
	movel	%d0,	%a0@+
	movel	%d0,	%a0@+
	movel	%d0,	%a0@+
	movel	%d0,	%a0@+

	lea	%pc@(ip_bootstrap), %a0
	lea	ip_bootstrap_buffer,  %a1
	movew	#BOOTSTRAP_SIZE-1,    %d7
	1: /* for (byte of bootstrap) { */
		moveb	%a0@+, %a1@+
		dbf	%d7,   1b
	/* } */

	jmp	ip_bootstrap_buffer
/* ------------------------ */
ip_bootstrap:
	moveq	#SUBCALL_LOAD_IPX, %d0
	jsr	subcall_wait
	moveal	MMD_ENTRY,     %a0
	mmd_load_m
	bset	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
	jmp	%a0@
ip_bootstrap_end:
/* ------------------------------------------------------- */
