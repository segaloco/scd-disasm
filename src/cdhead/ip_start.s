/* ------------------------------------------------------- */
/* ip_start - standard MainCPU startup                     */
/*  this section of code provides a proprietary security   */
/*  binary as well as utilizes internal BIOS entrypoints,  */
/*  therefore it is likely provided separately as          */
/*  object code                                            */
/*  this also writes a branch opcode to the SubCPU @0x528, */
/*  presumably to liberate some loop in the                */
/*  SubCPU BIOS code                                       */
/* ------------------------------------------------------- */
	.include "src/inc/m_bank.i"
	.include "src/inc/m_subreg.i"

	.include "src/inc/m_vector.i"
	.include "src/inc/m_bios.i"
/* ------------------------------------------------------- */
	.section .text.cd_head_ip
/* ------------------------------------------------------- */
	.equ	SUBCPU_HALT_OFFSET, 0x528
	.equ	SUBCPU_HALTADDR,    SUBCPU_1M_MEMBASE+SUBCPU_HALT_OFFSET
/* ------------------------ */
	.globl	ip_start    
ip_start:
	movel	#BIOS_ENTRY_RESET, M_VECTOR_RESET

	lea	SUBCPU_BUS, %a5
	1: /* while (!SUBCPU_BUSREQ) { */
		bset	#SUBCPU_BUSREQ_BIT, %a5@
		beqs	1b
	/* } */

	movew	%pc@(ip_halt_release), SUBCPU_HALTADDR

	1: /* while (SUBCPU_BUSREQ) { */
		bclr	#SUBCPU_BUSREQ_BIT, %a5@
		bnes	1b
	/* } */

	lea	%pc@(ip_security_code), %a1
	jsr	BIOS_ENTRY_SECURITY
	braw	1f
/* ------------------------ */
ip_security_code:
	.incbin "data/cdhead/sec.bin"
ip_halt_release:
	bras	.+8
/* ------------------------------------------------------- */
1:
