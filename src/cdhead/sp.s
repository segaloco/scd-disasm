/* ------------------------------------------------------- */
/* sp - system (SubCPU) program                            */
/*  the SP monitors the communication buffer for a word    */
/*  from the MainCPU which is an index into a table of     */
/*  actions to take, many resulting in the loading of a    */
/*  binary file from disc                                  */
/*  the below syscall positions must be in sync with the   */
/*  indices in subcall.i for the time being                */
/*                                                         */
/* Citation: Mega-CD BIOS Manual                           */
/*  4-3 Booting Application Programs (pp. 31, 32)          */
/* ------------------------------------------------------- */
	.include "src/inc/s_bank.i"
	.include "src/inc/s_subreg.i"
	.include "src/inc/fader.i"

	.include "src/inc/s_bios.i"

	.include "src/inc/os.i"
	.include "src/inc/pcm.i"

	.section .text.cd_head_sp
/* ------------------------------------------------------- */
sp:
	.ascii	"MAIN       "
    .even
	.word	0, 0
	.long	0
	.long	sp_end-sp+1
	.long	tbl_call-sp
	.long	0
/* ------------------------ */
tbl_call:
	.word	sp_boot-tbl_call
	.word	sp_main-tbl_call
	.word	sp_level2-tbl_call
	.word	sys_nullret-tbl_call
	.word	0
/* ------------------------------------------------------- */
sp_boot:
	lea	%pc@(sp_toc),    %a0
	movew	#BIOS_DRVINIT,   %d0
	jsr	BIOS_ENTRY

	1: /* while((BIOS_R_CDSTAT & 0xF0)) { */
		movew	#BIOS_CDBSTAT,   %d0
		jsr	BIOS_ENTRY
		andib	#0b11110000,  BIOS_R_CDSTAT
		bnes	1b
	/* } */

	andib	#(~(SUBCPU_MODE_1M+SUBCPU_RET)&0xff), (SUBCPU_MEMMODE_W).w
	moveb	#0, SUBCPU_SCOM_FLG
	movew	#OS_ENTRY_INIT, %d0
	jsr	os_entry

sys_nullret:
	rts
/* ------------------------ */
sp_toc:
	.byte	0x01, 0xff
/* ------------------------ */
	.equ	CDDA_TRACK_TRACK2,  2
	.equ	CDDA_TRACK_ATTACK,  3
	.equ	CDDA_TRACK_TITLE,   4
	.equ	CDDA_TRACK_COME,    5
	.equ	CDDA_TRACK_TRACK6,  6
	.equ	CDDA_TRACK_TRACK7,  7
	.equ	CDDA_TRACK_NULL,    CDDA_TRACK_TRACK2
sp_cdda_table:
sp_cdda_track2:
	.word	CDDA_TRACK_TRACK2
sp_cdda_attack:
	.word	CDDA_TRACK_ATTACK
sp_cdda_title:
	.word	CDDA_TRACK_TITLE
sp_cdda_come:
	.word	CDDA_TRACK_COME
sp_cdda_track6:
	.word	CDDA_TRACK_TRACK6
sp_cdda_track7:
	.word	CDDA_TRACK_TRACK7
sp_cdda_null0:
	.word	CDDA_TRACK_NULL
sp_cdda_null1:
	.word	CDDA_TRACK_NULL
sp_cdda_null2:
	.word	CDDA_TRACK_NULL
sp_cdda_null3:
	.word	CDDA_TRACK_NULL
sp_cdda_null4:
	.word	CDDA_TRACK_NULL
sp_cdda_null5:
	.word	CDDA_TRACK_NULL
sp_cdda_null6:
	.word	CDDA_TRACK_NULL
sp_cdda_null7:
	.word	CDDA_TRACK_NULL
sp_cdda_null8:
	.word	CDDA_TRACK_NULL
sp_cdda_null9:
	.word	CDDA_TRACK_NULL
sp_cdda_null10:
	.word	CDDA_TRACK_NULL
sp_cdda_null11:
	.word	CDDA_TRACK_NULL
sp_cdda_null12:
	.word	CDDA_TRACK_NULL
sp_cdda_null13:
	.word	CDDA_TRACK_NULL
/* ------------------------------------------------------- */
	.equ	SP_MAIN_TXT_LOW,  0x10000
	.equ	SP_MAIN_TXT_HIGH, 0x30000
/* ------------------------ */
	.globl	sp_main
sp_main:
	movew	#OS_ENTRY_CDC_INIT, %d0
	jsr	os_entry

	1: /* while (os_entry_cdc_wait()) { */
		jsr	BIOS_WAITVSYNC
		movew	#OS_ENTRY_CDC_WAIT, %d0
		jsr	os_entry
		bcss	1b
	/* } */
	cmpiw	#OS_SUCCESS, %d0
	bnew	sub_halt /* if (os_entry_cdc_wait() == OS_SUCCESS) { */
		1: /* for (;;) { */
			movew	SUBCPU_CMD_BUFFER, %d0
			beqs	1b /* if (SUBCPU_CMD_BUFFER) { */
				addw	%d0, %d0
				movew	%pc@(sp_syscalls, %d0:w), %d0
				jsr	%pc@(sp_syscalls, %d0:w)
				movel	#sp_null_int, BIOS_TIMERINT
				bras	1b
			/* } */
		/* } */
	/* } */
/* ------------------------ */
/* subcall.i indices must match      */
/* those present here                */
sp_syscalls:
	.word	sys_nullret-sp_syscalls     /* 0 */
	.word	sys_load_r11a-sp_syscalls   /* 1 */
	.word	sys_load_r11b-sp_syscalls   /* 2 */
	.word	sys_load_r11c-sp_syscalls   /* 3 */
	.word	sys_load_r11d-sp_syscalls   /* 4 */
	.word	sys_load_logo-sp_syscalls   /* 5 */
	.word	sys_load_ssel-sp_syscalls   /* 6 */
	.word	sys_load_r12a-sp_syscalls   /* 7 */
	.word	sys_load_r12b-sp_syscalls   /* 8 */
	.word	sys_load_r12c-sp_syscalls   /* 9 */
	.word	sys_load_r12d-sp_syscalls   /* 10 */
	.word	sys_load_title-sp_syscalls  /* 11 */
	.word	sys_load_warp-sp_syscalls   /* 12 */
	.word	sys_load_attack-sp_syscalls /* 13 */

	.word	sys_faders_fade-sp_syscalls /* 14 */
	.word	sys_cdda_track2-sp_syscalls /* 15 */
	.word	sys_cdda_attack-sp_syscalls /* 16 */
	.word	sys_cdda_title-sp_syscalls  /* 17 */
	.word	sys_cdda_come-sp_syscalls   /* 18 */
	.word	sys_cdda_track6-sp_syscalls /* 19 */
	.word	sys_cdda_track7-sp_syscalls /* 20 */
	.word	sys_cdda_null0-sp_syscalls  /* 21 */
	.word	sys_cdda_null1-sp_syscalls  /* 22 */
	.word	sys_cdda_null2-sp_syscalls  /* 23 */
	.word	sys_cdda_null3-sp_syscalls  /* 24 */
	.word	sys_cdda_null4-sp_syscalls  /* 25 */
	.word	sys_cdda_null5-sp_syscalls  /* 26 */
	.word	sys_cdda_null6-sp_syscalls  /* 27 */
	.word	sys_cdda_null7-sp_syscalls  /* 28 */
	.word	sys_cdda_null8-sp_syscalls  /* 29 */
	.word	sys_cdda_null9-sp_syscalls  /* 30 */
	.word	sys_cdda_null10-sp_syscalls /* 31 */
	.word	sys_cdda_null11-sp_syscalls /* 32 */
	.word	sys_cdda_null12-sp_syscalls /* 33 */
	.word	sys_cdda_null13-sp_syscalls /* 34 */

	.word	sys_load_ipx-sp_syscalls    /* 35 */
	.word	sys_load_open-sp_syscalls   /* 36 */
	.word	sys_load_come-sp_syscalls   /* 37 */
/* ------------------------ */
sys_load_r12a:
	lea	%pc@((fname_r12a).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r12b:
	lea	%pc@((fname_r12b).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r12c:
	lea	%pc@((fname_r12c).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r12d:
	lea	%pc@((fname_r12d).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r11a:
	lea	%pc@((fname_r11a).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r11b:
	lea	%pc@((fname_r11b).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r11c:
	lea	%pc@((fname_r11c).w), %a0
	bras	sys_load_level
/* ------------------------ */
sys_load_r11d:
	lea	%pc@((fname_r11d).w), %a0
/* ------------------------ */
sys_load_level:
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	bsrw	sys_faders_full
	braw	sub_wait_main
/* ------------------------ */
sys_load_logo:
	lea	%pc@((fname_logo_m).w), %a0
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	bsrw	sub_wait_main
	lea	%pc@((fname_logo_s).w), %a0
	lea	SP_MAIN_TXT_LOW, %a1
	jsr	(os_loadfile).w
	lea	%pc@((fname_pcm).w), %a0
	lea	PCM_BASE, %a1
	jsr	(os_loadfile).w
	jmp	SP_MAIN_TXT_LOW
/* ------------------------ */
sys_load_title:
	lea	%pc@((fname_title).w), %a0
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	bsrw	sub_wait_main
	bsrw	sys_faders_full
	lea	%pc@((sp_cdda_title).w), %a0
	movew	#BIOS_MSCPLAY1, %d0
	jmp	BIOS_ENTRY
/* ------------------------ */
sys_load_come:
	lea	%pc@((fname_come).w), %a0
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	bsrw	sub_wait_main
	bsrw	sys_faders_full
	lea	%pc@((sp_cdda_come).w), %a0
	movew	#BIOS_MSCPLAYR, %d0
	jmp	BIOS_ENTRY
/* ------------------------ */
sys_load_ipx:
	lea	%pc@((fname_ipx).w), %a0
	bras	sys_load_misc
/* ------------------------ */
sys_load_ssel:
	lea	%pc@((fname_ssel).w), %a0
	bras	sys_load_misc
/* ------------------------ */
sys_load_warp:
	lea	%pc@((fname_warp).w), %a0
/* ------------------------ */
sys_load_misc:
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	braw	sub_wait_main
/* ------------------------ */
sys_load_attack:
	lea	%pc@((fname_attackm).w), %a0
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	bsrw	sub_wait_main
	lea	%pc@((fname_attackb).w), %a0
	lea	SP_MAIN_TXT_LOW, %a1
	jsr	(os_loadfile).w
	bsrw	sys_faders_full
	lea	%pc@((sp_cdda_attack).w), %a0
	movew	#BIOS_MSCPLAYR, %d0
	jsr	BIOS_ENTRY
	jmp	SP_MAIN_TXT_LOW
/* ------------------------ */
sys_load_open:
	lea	%pc@((fname_openm).w), %a0
	bsrw	sub_wait_dmna
	lea	SUBCPU_M2_BANK2, %a1
	jsr	(os_loadfile).w
	bsrw	sub_wait_main
	lea	%pc@((fname_opens).w), %a0
	lea	SP_MAIN_TXT_HIGH, %a1
	jsr	(os_loadfile).w
	jsr	SP_MAIN_TXT_HIGH
	andib	#0xfa, SUBCPU_MEMMODE
	rts
/* ------------------------------------------------------- */
sys_faders_fade:
	movew	#BIOS_FDRCHG, %d0
	moveq	#(0<<16)+0x20, %d1
	jsr	BIOS_ENTRY
	braw	_sub_wait_main
/* ------------------------ */
sys_faders_full:
	movel	%a0,          %sp@-
	movew	#BIOS_FDRSET, %d0
	movew	#FADER0_FULL, %d1
	jsr	BIOS_ENTRY
	movew	#BIOS_FDRSET, %d0
	movew	#FADER1_FULL, %d1
	jsr	BIOS_ENTRY
	moveal	%sp@+,        %a0
	rts
/* ------------------------ */
sys_cdda_track2:
	lea	%pc@(sp_cdda_track2), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_attack:
	lea	%pc@(sp_cdda_attack), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_title:
	lea	%pc@(sp_cdda_title), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_come:
	lea	%pc@(sp_cdda_come), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_track6:
	lea	%pc@(sp_cdda_track6), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_track7:
	lea	%pc@(sp_cdda_track7), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null0:
	lea	%pc@(sp_cdda_null0), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null1:
	lea	%pc@(sp_cdda_null1), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null2:
	lea	%pc@(sp_cdda_null2), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null3:
	lea	%pc@(sp_cdda_null3), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null4:
	lea	%pc@(sp_cdda_null4), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null5:
	lea	%pc@(sp_cdda_null5), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null6:
	lea	%pc@(sp_cdda_null6), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null7:
	lea	%pc@(sp_cdda_null7), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null8:
	lea	%pc@(sp_cdda_null8), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null9:
	lea	%pc@(sp_cdda_null9), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null10:
	lea	%pc@(sp_cdda_null10), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null11:
	lea	%pc@(sp_cdda_null11), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null12:
	lea	%pc@(sp_cdda_null12), %a0
	bras	sys_load_cdda
/* ------------------------ */
sys_cdda_null13:
	lea	%pc@(sp_cdda_null13), %a0
/* ------------------------ */
sys_load_cdda:
	bsrw	sys_faders_full
	movew	#BIOS_MSCPLAYR, %d0
	jsr	BIOS_ENTRY
	braw	_sub_wait_main
/* ------------------------------------------------------- */
	.globl	sp_level2
sp_level2:
	moveml	%d0-%fp,        %sp@-
	movew	#OS_ENTRY_VINT, %d0
	jsr	os_entry
	moveml	%sp@+,          %d0-%fp
	rts
/* ------------------------------------------------------- */
	.globl	sp_null_int
sp_null_int:
    rte
/* ------------------------------------------------------- */
	.section .text.sp_os
	.globl	os_ftable
os_ftable:
fname_r11a:
	.asciz	"R11A__.MMD;1"
fname_r11b:
	.asciz	"R11B__.MMD;1"
fname_r11c:
	.asciz	"R11C__.MMD;1"
fname_r11d:
	.asciz	"R11D__.MMD;1"
fname_logo_m:
	.asciz	"LOGO_M.MMD;1"
fname_logo_s:
	.asciz	"LOGO_S.BIN;1"
fname_ssel:
	.asciz	"SSEL__.MMD;1"
fname_r12a:
	.asciz	"R12A__.MMD;1"
fname_r12b:
	.asciz	"R12B__.MMD;1"
fname_r12c:
	.asciz	"R12C__.MMD;1"
fname_r12d:
	.asciz	"R12D__.MMD;1"
fname_title:
	.asciz	"TITLE_.MMD;1"
fname_pcm:
	.asciz	"PCM000.BIN;1"
fname_warp:
	.asciz	"WARP__.MMD;1"
fname_attackm:
	.asciz	"ATTACK.MMD;1"
fname_attackb:
	.asciz	"ATTACK.BIN;1"
fname_ipx:
	.asciz	"IPX___.MMD;1"
fname_openstm:
	.asciz	"OPN.STM;1   "
fname_openm:
	.asciz	"OPEN_M.MMD;1"
fname_opens:
	.asciz	"OPEN_S.BIN;1"
fname_come:
	.asciz	"COME__.MMD;1"
/* ------------------------------------------------------- */
sp_end:
