/* ------------------------------------------------------- */
/* disc_head - disc identifier                             */
/*  most fields equivalent to Mega Drive header            */
/*                                                         */
/* Citation: Mega-CD Disk Format Specifications            */
/*  Appendix C (pp. 20, 21)                                */
/* ------------------------------------------------------- */
	.section .text.cd_disc_head
/* ------------------------------------------------------- */
	.ascii	"SEGA MEGA DRIVE "
	.ascii	"(C)SEGA 1993.APR"
	.ascii	"CD-SONIC THE HEDGEHOG                           "
	.ascii	"CD-SONIC THE HEDGEHOG                           "
	.ascii	"XX 00000000-00"
	.align	4, 0x20
	.ascii	"J               "
	.ascii	"                            "
	.ascii	"  ", "    ", "    "
	.align	4, 0x20
	.ascii	"                                        "
	.ascii	"J               "
/* ------------------------------------------------------- */
