/* ------------------------------------------------------- */
/* sys_head - system identifier                            */
/*  identifies the disc itself and what title it belongs   */
/*  to, as well as properties of any executable payloads   */
/*  in the boot sector                                     */
/*                                                         */
/* Citation: Mega-CD Disk Format Specifications            */
/*  Appendix B (pp. 18, 19)                                */
/* ------------------------------------------------------- */
	.section .text.cd_sys_head
/* ------------------------------------------------------- */
	.ascii	"SEGADISCSYSTEM  "  /* disc identifier */
	.ascii	"SEGAIPSAMP "       /* volume name */
    .even
	.word	0, 1                /* version 00, CD-ROM */
	.ascii	"SYSTEM NAME"       /* system name */
    .even
	.word	0                   /* version 00 */
	.align	4
	.long	_head_end           /* IP payload address */
	.long	_ip_size            /* IP payload size */
	.long	0
	.long	0
	.long	_ip_end             /* SP payload address */
	.long	_sp_size            /* SP payload size */
	.long	0
	.long	0
	.align	0x100, 0x20
/* ------------------------------------------------------- */
