/* ------------------------------------------------------- */
/* sp_os - the main file handling library                  */
/*  this library is padded far from the rest of the SP     */
/*  code and is presumed to be a filesystem library        */
/*  distributed as object code with predictable            */
/*  entrypoints                                            */
/* ------------------------------------------------------- */
	.include "src/inc/s_bank.i"
	.include "src/inc/s_subreg.i"

	.include "src/inc/s_bios.i"

	.include "src/inc/os.i"
	.section .text.sp_os
/* ------------------------------------------------------- */
	.globl	os_loadfile
os_loadfile:
	movew	#OS_ENTRY_CDC_LOAD, %d0
	jsr	(os_entry).l

	1: /* while (os_cdc_wait()) { */
		movew	#OS_ENTRY_CDC_WAIT, %d0
		jsr	(os_entry).l
		bcss	1b
	/* } */
	cmpiw	#OS_SUCCESS, %d0
	bnew	sub_halt

	rts
/* ------------------------------------------------------- */
	.align	0x40
	.globl	os_loadfile_idx
os_loadfile_idx:
	muluw	#(OS_FSNAME_LEN+1), %d0
	lea	os_ftable, %a0
	addaw	%d0,       %a0
	rts
/* ------------------------------------------------------- */
/* NOTE: below, OS_VINT is defined as a macro expanding to */
/*  _os_vint, a linker, rather than assembler              */
/*  representation of 0                                    */
/*  the reason for this is the opcode in the original      */
/*  binary was specifically an address register indirect   */
/*  with offset of zero                                    */
/*  GNU as, the chosen assembler for this project,         */
/*  silently optimizes this to a direct reference, a       */
/*  smart, but inaccurate assumption                       */
/*  this linker quirk retains the original binary output   */
	.equ	OS_DEFFRAME,      0x8c00
	.equ	OS_CDCFRAMESIZE,  0x800

	.equ	OS_VINT,	  _os_vint
	.equ	OS_VINT_NULL,     0
	.equ	OS_CDCSIZE,       0x0004
	.equ	OS_CDCCOUNT,      0x0008
	.equ	OS_RETLOC,        0x000c
	.equ	OS_CDCBUFP,       0x0010
	.equ	OS_CDCHDRP,       0x0014
	.equ	SUB7880_TBL_0016, 0x0016
	.equ	OS_CDCBLKSIZE,    0x0018
	.equ	OS_VINTCALL,      0x001c
	.equ	OS_CDCSTAT,       0x001e
	.equ	SUB7880_TBL_0020, 0x0020
	.equ	OS_RESTAT_COUNT,  0x0022
	.equ	OS_REREAD_COUNT,  0x0024
	.equ	OS_READ_COUNT,    0x0026
	.equ	OS_CDCDEST,       0x0028
	.equ	SUB7880_TBL_0029, 0x0029
	.equ	OS_FILNAMP,       0x002a
	.equ	SUB7880_TBL_0042, 0x0042
	.equ	SUB7880_TBL_0046, 0x0046
	.equ	SUB7880_TBL_0100, 0x0100
	.equ	SUB7880_TBL10_00,	0x00
	.equ	SUB7880_TBL10_17,	0x17
	.equ	SUB7880_TBL10_18,   	  0x18
	.equ	SUB7880_TBL10_1c,	0x1c
	.equ	SUB7880_TBL_1100, 0x1100
	.equ	SUB7880_TBL11_06,	0x06
	.equ	SUB7880_TBL11_0e,	0x0e
	.equ	SUB7880_TBL11_19,	0x19
	.equ	SUB7880_TBL11_20,	0x20
	.equ	SUB7880_TBL11_21,	0x21
	.equ	SUB7880_TBL11_A2,	0xa2
	.equ	SUB7880_TBL11_AA,	0xaa
	.equ	SUB7880_TBL_1a00, 0x1a00
	.equ	SUB7880_TBL_1a03, 0x1a03
	.equ	SUB7880_TBL_1a04, 0x1a04
/* ------------------------ */
	.align	0x40
	.globl	os_entry
os_entry:
	lea	OS_DEFFRAME, %a5
	addw	%d0, %d0
	movew	%pc@(tbl_oscall, %d0:w), %d0
	jsr	%pc@(tbl_oscall, %d0:w)
	rts
/* ------------------------ */
tbl_oscall:
	.word	sub_os_init-tbl_oscall
	.word	sub_os_vint-tbl_oscall
	.word	sub_os_cdc_wait-tbl_oscall
	.word	sub_os_cdc_init-tbl_oscall
	.word	sub_os_cdc_load-tbl_oscall
	.word	sub_os_block_prep-tbl_oscall
	.word	sub_os_block_load-tbl_oscall
	.word	sub_os_unwind-tbl_oscall
/* ------------------------------------------------------- */
sub_os_cdc_init:
	movew	#1,	%a5@(OS_VINTCALL)
	moveb	#0x80,	%a5@(SUB7880_TBL_1a03)
	movel	#0,	%a5@(SUB7880_TBL_1a04)
	rts
/* ------------------------------------------------------- */
sub_os_init:
	movel	#sub_os_defvint,    %a5@((OS_VINT).b)
	movew	#OS_VINT_NULL,      %a5@(OS_VINTCALL)
	rts
/* ------------------------------------------------------- */
sub_os_vint:
	moveal	%a5@((OS_VINT).b), %a0
	jmp	%a0@
/* ------------------------------------------------------- */
sub_os_defvint:
	bsrw	os_vint_unwind
	movew	%a5@(OS_VINTCALL), %d0
	addw	%d0, %d0
	movew	%pc@(tbl_vint, %d0:w), %d0
	jmp	%pc@(tbl_vint, %d0:w)
/* ------------------------ */
tbl_vint:
	.word	sub_os_defvint-tbl_vint
	.word	sub_78ee-tbl_vint
	.word	sub_79e4-tbl_vint
	.word	sub_7c88-tbl_vint
/* ------------------------------------------------------- */
os_vint_unwind:
	movel	%sp@+, %a5@((OS_VINT).b)
	rts
/* ------------------------------------------------------- */
tbl_100_entry_size	= 0x20

sub_78ee:
	moveb	#CDC_SUBCPU_READ, %a5@(OS_CDCDEST)
	movel	#0x10,            %a5@(OS_CDCSIZE)
	movel	#1,               %a5@(OS_CDCCOUNT)
	lea	%a5@(SUB7880_TBL_1100),        %a0
	movel	%a0,              %a5@(OS_CDCBUFP)
	bsrw	sub_cdcfread
	cmpiw	#OS_ERR_ff9c,     %a5@(OS_CDCSTAT)
	beqw	sub_78ee_err

	lea	%a5@(SUB7880_TBL_1100), %a1
	movel	%a1@(SUB7880_TBL11_A2), %a5@(OS_CDCSIZE)
	movel	%a1@(SUB7880_TBL11_AA), %d0
	divuw	#OS_CDCFRAMESIZE, %d0
	swap	%d0
	tstw	%d0
	beqs	1f /* if (cdc_framesize_hi) { */
		addil	#0x10000, %d0
	1: /* } */

	swap	%d0
	movew	%d0, %a5@(OS_CDCBLKSIZE)
	clrw	%a5@(SUB7880_TBL_0020)
	2: /* for (cdc_blksize; cdc_blksize; cdc_blksize--) { */
		movel	#1, %a5@(OS_CDCCOUNT)
		lea	%a5@(SUB7880_TBL_1100), %a1
		movel	%a1, %a5@(OS_CDCBUFP)
		bsrw	sub_cdcfread
		cmpiw	#OS_ERR_ff9c, %a5@(OS_CDCSTAT)
		beqw	sub_78ee_err

		lea	%a5@(SUB7880_TBL_0100), %a0
		movew	%a5@(SUB7880_TBL_0020), %d0
		muluw	#tbl_100_entry_size,    %d0
		addal	%d0,                    %a0
		lea	%a5@(SUB7880_TBL_1100), %a1
		moveq	#0,                     %d0
		3: /* for (a0 => tbl_100[tbl_100_entry_size*tbl_20], a1 => tbl_1100; !tbl_1100[OS_VINT]; a0 += tbl_100_entry_size, a1 += tbl_1100[OS_VINT]) { */
			moveb	%a1@((OS_VINT).b), %d0
			beqs	4f

			moveb	%a1@(SUB7880_TBL11_19), %a0@(SUB7880_TBL10_17)

			moveq	#0, %d1
			1: /* for (i = 0; i < 4; i++) { */
				moveb	%a1@(SUB7880_TBL11_06, %d1:w), %a0@(SUB7880_TBL10_18, %d1:w)
				moveb	%a1@(SUB7880_TBL11_0e, %d1:w), %a0@(SUB7880_TBL10_1c, %d1:w)

				addqw	#1, %d1
				cmpiw	#4, %d1
				blts	1b
			/* } */

			moveq	#0, %d1
			1: /* for (i = 0; i < tbl_1100[0x20]; i++) { */
				moveb	%a1@(SUB7880_TBL11_21, %d1:w), %a0@(SUB7880_TBL10_00, %d1:w)

				addqw	#1, %d1
				cmpb	%a1@(SUB7880_TBL11_20), %d1
				blts	1b
			/* } */

			1:  /* for (i; i < 0xC; i++) { */
				cmpib	#0xc, %d1
				bges	1f

				moveb	#0x20, %a0@(SUB7880_TBL10_00, %d1:w)

				addqw	#1, %d1
				bras	1b
			1: /* } */

			addqw	#1,  %a5@(SUB7880_TBL_0020)
			addal	%d0, %a1
			addal	#tbl_100_entry_size, %a0
			bras	3b
		4: /* } */

		subqw	#1, %a5@(OS_CDCBLKSIZE)
		bnew	2b
	/* } */

	movew	#OS_SUCCESS, %a5@(OS_CDCSTAT)
	1:

	movew	#OS_VINT_NULL, %a5@(OS_VINTCALL)
	braw	sub_os_defvint

sub_78ee_err:
	movew	#OS_ERR_ffff, %a5@(OS_CDCSTAT)
	bras	1b
/* ------------------------------------------------------- */
sub_79e4:
	moveb	#CDC_SUBCPU_READ, %a5@(OS_CDCDEST)
	lea	%a5@(OS_FILNAMP), %a0
	bsrw	sub_os_block_prep
	bcsw	sub_79e4_err_fffe

	movel	%a0@(SUB7880_TBL_0042-OS_FILNAMP), %a5@(OS_CDCSIZE)
	movel	%a0@(SUB7880_TBL_0046-OS_FILNAMP), %d1
	movel	%d1,        %a5@(OS_CDCBLKSIZE)
	movel	#1,         %a5@(OS_CDCCOUNT)
	1: /* for (tet; cdc_blksize; cdc_blksize -= cdc_framesize) { */
		subil	#OS_CDCFRAMESIZE, %d1
		bles	1f

		addql	#1, %a5@(OS_CDCCOUNT)
		bras	1b
	1: /* } */

	bsrw	sub_cdcfread
	cmpiw	#OS_SUCCESS, %a5@(OS_CDCSTAT)
	beqs	1f /* if (sub_cdcfread() != OS_SUCCESS) { */
		movew	#OS_ERR_fffd, %a5@(OS_CDCSTAT)
	1: /* } */

	movew	#OS_VINT_NULL,   %a5@(OS_VINTCALL)
	braw	sub_os_defvint

sub_79e4_err_fffe:
	movew	#OS_ERR_fffe,    %a5@(OS_CDCSTAT)
	bras	1b
/* ------------------------------------------------------- */
sub_os_cdc_wait:
	cmpiw	#OS_VINT_NULL, %a5@(OS_VINTCALL)
	bnes	loc_7a66

	movew	%a5@(OS_CDCSTAT), %d0
	cmpiw	#OS_SUCCESS,      %d0
	bnes	loc_7a56

	movel	%a5@(OS_CDCBLKSIZE), %d1
	bras	loc_7a60

loc_7a56:
	cmpiw	#OS_ERR_fffd,  %d0
	bnes	loc_7a60

	movew	%a5@(OS_READ_COUNT), %d1

loc_7a60:
	movew	#0, %ccr
	rts

loc_7a66:
	movew	#1, %ccr
	rts
/* ------------------------------------------------------- */
sub_os_cdc_load:
	movew	#2,  %a5@(OS_VINTCALL)
	movel	%a1, %a5@(OS_CDCBUFP)
	moveal	%a0, %a1
	lea	%a5@(OS_FILNAMP), %a2
	movew	#11, %d1

1:
	moveb	%a1@+, %a2@+
	dbf	%d1, 1b

	rts
/* ------------------------------------------------------- */
sub_os_block_prep:
	movel	%a2,    %sp@-

	moveq	#0,     %d1
	moveal	%a0,    %a1
	movew	#11,    %d0
	1: /* for (d1 = 0, a1 = a0, i = 11; *a1.not_in(0, 0x20, 0x3B) && i > 0; d1++, a1++, i--) */
		tstb	%a1@
		beqs	1f

		cmpib	#0x3b, %a1@
		beqs	1f

		cmpib	#0x20, %a1@
		beqs	1f

		addqw	#1,    %d1
		addqw	#1,    %a1
		dbf	%d0,   1b
	1: /* } */

	movew	%a5@(SUB7880_TBL_0020), %d0
	moveal	%a0,                    %a1
	lea	%a5@(SUB7880_TBL_0100), %a0
	lea	%pc@(data_7ae2),        %a2
	bsrw	os_memcmp
	beqw	sub_os_block_prep_end

	moveal	%a0,   %a2
	subqw	#1,    %d0
	1: /* for (a2 = a0, i = *tbl_20-1; i > 0; a2 += 32, i--) { */
		bsrw	os_memcmp
		beqs	1f

		addaw	#32,   %a2
		dbf	%d0,   1b
	/* } */

	bras	2f
	1: /* if (!os_memcmp()) { */
		moveq	#1,    %d0
		moveal	%a2,   %a0

	sub_os_block_prep_end:
		moveal	%sp@+, %a2
		rts

	2: /* } else { */
		movew	#1, %ccr
		bras	sub_os_block_prep_end
	/* } */
/* ------------------------ */
data_7ae2:
	.long	0x5c202020
	.long	0x20202020
	.long	0x20202000
/* ------------------------------------------------------- */
sub_cdcfread:
	movel	%sp@+,  %a5@(OS_RETLOC)
	movew	#0,     %a5@(OS_READ_COUNT)
	movew	#0xa,   %a5@(OS_REREAD_COUNT)

sub_cdcfread_reread:
	moveb	%a5@(OS_CDCDEST), SUBCPU_CDCMODE_S
	lea	%a5@(OS_CDCSIZE), %a0
	movel	%a0@, %d0
	divuw	#0x4b, %d0
	swap	%d0
	extl	%d0
	divuw	#0xa, %d0
	moveb	%d0, %d1
	lslb	#4, %d1
	swap	%d0
	movew	#0, %ccr
	abcd	%d1, %d0
	moveb	%d0, %a5@(SUB7880_TBL_0029)
	movew	#BIOS_ROMREADN, %d0
	jsr	BIOS_ENTRY
	movew	#0x258, %a5@(OS_RESTAT_COUNT)

sub_cdcfread_unwind:
	bsrw	os_vint_unwind

sub_cdcfread_has_frames:
	movew	#BIOS_CDCSTAT,  %d0
	jsr	BIOS_ENTRY
	bccs	sub_cdcfread_data_found

	subqw	#1, %a5@(OS_RESTAT_COUNT)
	bges	sub_cdcfread_unwind

	subqw	#1, %a5@(OS_REREAD_COUNT)
	bges	sub_cdcfread_reread

	braw	sub_cdcfread_fail

sub_cdcfread_data_found:
	movew	#BIOS_CDCREAD, %d0
	jsr	BIOS_ENTRY
	bcsw	sub_cdcfread_data_err

	movel	%d0, %a5@(OS_CDCHDRP)
	moveb	%a5@(SUB7880_TBL_0029), %d0
	cmpb	%a5@(SUB7880_TBL_0016), %d0
	beqs	sub_cdcfread_data_success

sub_cdcfread_data_err:
	subqw	#1, %a5@(OS_REREAD_COUNT)
	bgew	sub_cdcfread_reread

	braw	sub_cdcfread_fail

sub_cdcfread_data_success:
	movew	#0x7ff, %d0

sub_cdcfread_data_wait:
	btst	#CDC_DATA_READY_BIT, SUBCPU_CDCMODE_S
	dbne	%d0, sub_cdcfread_data_wait

	bnes	sub_cdcfread_data_ready

	subqw	#1, %a5@(OS_REREAD_COUNT)
	bgew	sub_cdcfread_reread

	braw	sub_cdcfread_fail

sub_cdcfread_data_ready:
	cmpib	#CDC_MAINCPU_READ, %a5@(OS_CDCDEST)
	beqw	sub_cdcfread_maincpu_wait

	movew	#BIOS_CDCTRN, %d0
	moveal	%a5@(OS_CDCBUFP), %a0
	lea	%a5@(OS_CDCHDRP), %a1
	jsr	BIOS_ENTRY
	bcss	sub_cdcfread_trn_fail

	moveb	%a5@(SUB7880_TBL_0029), %d0
	cmpb	%a5@(SUB7880_TBL_0016), %d0
	beqs	sub_cdcfread_trn_success

sub_cdcfread_trn_fail:
	subqw	#1, %a5@(OS_REREAD_COUNT)
	bgew	sub_cdcfread_reread

	braw	sub_cdcfread_fail

sub_cdcfread_trn_success:
	movew	#0,  %ccr
	moveq	#1,  %d1
	abcd	%d1, %d0
	moveb	%d0,   %a5@(SUB7880_TBL_0029)
	cmpib	#0x75, %a5@(SUB7880_TBL_0029)
	bcss	sub_cdcfread_eot_success

	moveb	#0,    %a5@(SUB7880_TBL_0029)

sub_cdcfread_eot_success:
	movew	#BIOS_CDCACK, %d0
	jsr	BIOS_ENTRY
	movew	#6,     %a5@(OS_RESTAT_COUNT)
	movew	#0xa,   %a5@(OS_REREAD_COUNT)
	addil	#OS_CDCFRAMESIZE, %a5@(OS_CDCBUFP)
	addqw	#1,     %a5@(OS_READ_COUNT)
	addql	#1,     %a5@(OS_CDCSIZE)
	subql	#1,     %a5@(OS_CDCCOUNT)
	bgtw	sub_cdcfread_has_frames

	movew	#OS_SUCCESS, %a5@(OS_CDCSTAT)

sub_cdcfread_return:
	moveb	%a5@(OS_CDCDEST), SUBCPU_CDCMODE_S
	moveal	%a5@(OS_RETLOC),  %a0
	jmp	%a0@

sub_cdcfread_fail:
	movew	#OS_ERR_ff9c, %a5@(OS_CDCSTAT)
	bras	sub_cdcfread_return

sub_cdcfread_maincpu_wait:
	movew	#6, %a5@(OS_RESTAT_COUNT)

sub_cdcfread_wait_complete:
	bsrw	os_vint_unwind
	btst	#CDC_DATA_EOT_BIT, SUBCPU_CDCMODE_S
	bnes	sub_cdcfread_eot_success

	subqw	#1, %a5@(OS_RESTAT_COUNT)
	bges	sub_cdcfread_wait_complete

	bras	sub_cdcfread_fail
/* ------------------------------------------------------- */
os_memcmp:
	moveml	%d1/%a1-%a2, %sp@-

	1: /* for (i; i > 0; i--) { */
		cmpmb	%a1@+, %a2@+
		bnes	1f

		dbf	%d1, 1b
	/* } */

	/* if (*a1 == *a2) { */
		moveq	#0, %d1
	1: /* } */

	moveml	%sp@+, %d1/%a1-%a2
	rts
/* ------------------------------------------------------- */
sub_os_block_load:
	movew	#3,               %a5@(OS_VINTCALL)
	movel	#SUBCPU_M2_BANK1, %a5@(OS_CDCBUFP)
	movew	#0,               %a5@(SUB7880_TBL_1a00)
	bset	#7,               %a5@(SUB7880_TBL_1a03)

	moveal	%a0, %a1
	lea	%a5@(OS_FILNAMP), %a2
	movew	#11, %d1
	1: /* for (i = 11; i > 0; i--) { */
		moveb	%a1@+, %a2@+
		dbf	%d1, 1b
	/* } */

	rts
/* ------------------------------------------------------- */
sub_7c88:
	moveb	#CDC_SUBCPU_READ, %a5@(OS_CDCDEST)
	lea	%a5@(OS_FILNAMP), %a0
	bsrw	sub_os_block_prep
	bcsw	sub_7c88_err_fffe

	movel	%a0@(SUB7880_TBL_0042-OS_FILNAMP), %a5@(OS_CDCSIZE)
	movel	%a0@(SUB7880_TBL_0046-OS_FILNAMP), %d1
	movel	%d1, %a5@(OS_CDCBLKSIZE)
	movel	#1, %a5@(OS_CDCCOUNT)
	1: /* for () { */
		subil	#OS_CDCFRAMESIZE, %d1
		bles	1f

		addql	#1, %a5@(OS_CDCCOUNT)
		bras	1b
	1: /* } */

	bsrw	sub_cdcfreadp
	cmpiw	#OS_SUCCESS, %a5@(OS_CDCSTAT)
	beqs	1f /* if (sub_cdcfreadp() == OS_SUCCESS) { */
		movew	#OS_ERR_fffd, %a5@(OS_CDCSTAT)
	1: /* } */

	movew	#OS_VINT_NULL, %a5@(OS_VINTCALL)
	braw	sub_os_defvint

sub_7c88_err_fffe:
	movew	#OS_ERR_fffe, %a5@(OS_CDCSTAT)
	bras	1b
/* ------------------------------------------------------- */
sub_cdcfreadp:
	movel	%sp@+,  %a5@(OS_RETLOC)
	movew	#0,     %a5@(OS_READ_COUNT)
	movew	#0xa,   %a5@(OS_REREAD_COUNT)

sub_cdcfreadp_reread:
	moveb	%a5@(OS_CDCDEST), SUBCPU_CDCMODE_S
	lea	%a5@(OS_CDCSIZE), %a0
	movel	%a0@, %d0
	divuw	#0x4b, %d0
	swap	%d0
	extl	%d0
	divuw	#0xa, %d0
	moveb	%d0, %d1
	lslb	#4, %d1
	swap	%d0
	movew	#0, %ccr
	abcd	%d1, %d0
	moveb	%d0, %a5@(SUB7880_TBL_0029)
	movew	#BIOS_ROMREADN, %d0
	jsr	BIOS_ENTRY
	movew	#0x258, %a5@(OS_RESTAT_COUNT)

sub_cdcfreadp_restat_deep:
	bsrw	os_vint_unwind

sub_cdcfreadp_restat:
	movew	#BIOS_CDCSTAT,  %d0
	jsr	BIOS_ENTRY
	bccs	sub_cdcfreadp_data_found

	subqw	#1, %a5@(OS_RESTAT_COUNT)
	bges	sub_cdcfreadp_restat_deep

	subqw	#1, %a5@(OS_REREAD_COUNT)
	bges	sub_cdcfreadp_reread

	braw	sub_cdcfreadp_fail

sub_cdcfreadp_data_found:
	movew	#BIOS_CDCREAD, %d0
	jsr	BIOS_ENTRY
	bcsw	sub_cdcfreadp_data_err

	movel	%d0, %a5@(OS_CDCHDRP)
	moveb	%a5@(SUB7880_TBL_0029), %d0
	cmpb	%a5@(SUB7880_TBL_0016), %d0
	beqs	loc_7d72

sub_cdcfreadp_data_err:
	addql	#1, %a5@(SUB7880_TBL_1a04)
	subqw	#1, %a5@(OS_REREAD_COUNT)
	bgew	sub_cdcfreadp_reread

	braw	sub_cdcfreadp_fail

loc_7d72:
	movew	#0x7ff, %d0

sub_cdcfreadp_data_wait:
	btst	#CDC_DATA_READY_BIT, SUBCPU_CDCMODE_S
	dbne	%d0, sub_cdcfreadp_data_wait

	bnes	sub_cdcfreadp_data_ready

	subqw	#1, %a5@(OS_REREAD_COUNT)
	bgew	sub_cdcfreadp_reread

	braw	sub_cdcfreadp_fail

sub_cdcfreadp_data_ready:
	cmpib	#CDC_MAINCPU_READ, %a5@(OS_CDCDEST)
	beqw	sub_cdcfreadp_maincpu_wait

	movew	#BIOS_CDCTRN, %d0
	moveal	%a5@(OS_CDCBUFP), %a0
	lea	%a5@(OS_CDCHDRP), %a1
	jsr	BIOS_ENTRY
	bcss	sub_cdcfreadp_trn_fail

	moveb	%a5@(SUB7880_TBL_0029), %d0
	cmpb	%a5@(SUB7880_TBL_0016), %d0
	beqs	sub_cdcfreadp_trn_success

sub_cdcfreadp_trn_fail:
	addql	#1, %a5@(SUB7880_TBL_1a04)
	subqw	#1, %a5@(OS_REREAD_COUNT)
	bgew	sub_cdcfreadp_reread

	braw	sub_cdcfreadp_fail

sub_cdcfreadp_trn_success:
	movew	#0, %ccr
	moveq	#1, %d1
	abcd	%d1, %d0
	moveb	%d0, %a5@(SUB7880_TBL_0029)
	cmpib	#0x75, %a5@(SUB7880_TBL_0029)
	bcss	sub_cdcfreadp_eot_success

	moveb	#0, %a5@(SUB7880_TBL_0029)

sub_cdcfreadp_eot_success:
	movew	#BIOS_CDCACK, %d0
	jsr	BIOS_ENTRY
	movew	#6, %a5@(OS_RESTAT_COUNT)
	movew	#0xa, %a5@(OS_REREAD_COUNT)
	movew	%a5@(SUB7880_TBL_1a00), %d0
	cmpiw	#0xf, %d0
	beqs	case_7e0e

	cmpiw	#0x4a, %d0
	beqs	case_7e24

	addil	#OS_CDCFRAMESIZE, %a5@(OS_CDCBUFP)
	bras	endsw_7e60

case_7e0e:
	moveb	#1,     BIOS_1a02
	bclr	#7, %a5@(SUB7880_TBL_1a03)
	movel	#SUBCPU_M2_BANK3, %a5@(OS_CDCBUFP)
	bras	endsw_7e60

case_7e24:
	bset	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG

	1: /* while () { */
		btst	#MCOM_FLAG_DONE_BIT, SUBCPU_E_FLG
		beqs	1b
	/* } */

	bclr	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG
	eorib	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE

	1: /* while () { */
		btst	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
		bnes	1b
	/* } */

	moveb	#0, BIOS_1a02
	movel	#SUBCPU_M2_BANK1, %a5@(OS_CDCBUFP)
	bset	#7, %a5@(SUB7880_TBL_1a03)
	bclr	#5, %a5@(SUB7880_TBL_1a03)

endsw_7e60:
	addqw	#1, %a5@(OS_READ_COUNT)
	addql	#1, %a5@(OS_CDCSIZE)
	addqw	#1, %a5@(SUB7880_TBL_1a00)
	cmpiw	#0x4b, %a5@(SUB7880_TBL_1a00)
	bcss	1f

	movew	#0, %a5@(SUB7880_TBL_1a00)

1:
	subql	#1, %a5@(OS_CDCCOUNT)
	bgtw	sub_cdcfreadp_restat

	movew	#OS_SUCCESS, %a5@(OS_CDCSTAT)

sub_cdcfreadp_return:
	moveb	%a5@(OS_CDCDEST), SUBCPU_CDCMODE_S
	moveal	%a5@(OS_RETLOC), %a0
	jmp	%a0@

sub_cdcfreadp_fail:
	movew	#OS_ERR_ff9c, %a5@(OS_CDCSTAT)
	bras	sub_cdcfreadp_return

sub_cdcfreadp_maincpu_wait:
	movew	#6, %a5@(OS_RESTAT_COUNT)
	1: /* for (os_restat_count = 6; os_restat_count >= 0; os_restat_count--) { */
		bsrw	os_vint_unwind
		btst	#CDC_DATA_EOT_BIT, SUBCPU_CDCMODE_S
		bnew	sub_cdcfreadp_eot_success

		subqw	#1, %a5@(OS_RESTAT_COUNT)
		bges	1b
	/* } */

	bras	sub_cdcfreadp_fail
/* ------------------------------------------------------- */
sub_os_unwind:
	movew	#OS_VINT_NULL, %a5@(OS_VINTCALL)
	braw	sub_os_defvint
/* ------------------------------------------------------- */
