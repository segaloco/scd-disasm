/* ------------------------------------------------------- */
/* attack_s - Time attack sub CPU routines                 */
/* ------------------------------------------------------- */
	.include "src/inc/s_subreg.i"

	.include "src/inc/s_bios.i"

	.text

sub_5F16	= 0x5F16
long_5F78	= 0x5F78

	reg_FF8003_bit_0	= 0
	reg_FF8003_bit_1	= 1
	reg_FF8003_bit_2	= 2

	reg_FF800E_bit_1	= 1
	reg_FF800E_bit_2	= 2
	reg_FF800E_bit_4	= 4

	reg_FF800F_bit_1	= 1
/* ------------------------ */
tbl_80000	= 0x80000

byte_800001	= 0x800001
/* ------------------------------------------------------- */
	.equ	BUF_F800,	0xF800
	.equ	BUF_F800_END,	0x10000
	.globl	attack_s
attack_s:
	moveb	#0,                     (SUBCPU_MEMMODE_W).w
	bset	#SCOM_FLAG_BUSY_BIT,    (SUBCPU_SCOM_FLG_W).w
	bclr	#INT_MASK_GFX_DONE_BIT, (SUBCPU_INT_MASK_W).w
	bclr	#INT_MASK_TIMER_BIT,    (SUBCPU_INT_MASK_W).w
	moveb	#CDC_SUBCPU_READ,       (SUBCPU_CDCMODE_W).w

	lea	BUF_F800, %a0
	movew	#((BUF_F800_END-BUF_F800)/4)-1, %d7
	1: /* for (long of BUF_F800) { */
		movel	#0, %a0@+

		dbf	%d7, 1b
	/* } */

	moveb	#3, byte_800001

	lea	%pc@(tbl_101A2), %a0
	lea	%pc@(tbl_107E2), %a1
	movew	#0, %d0
	jsr	sub_5F16

	lea	%pc@(tbl_10188), %a0
	lea	tbl_80000, %a1
	movew	#3, %d0
	jsr	sub_5F16
	bccs	1f /* if (sub_5F16(tbl_10188, tbl_80000, 3)) { */
   		bsrw	sub_10114
	1: /* } */

	bset	#reg_FF8003_bit_0,   (SUBCPU_MEMMODE_W).w

	bclr	#SCOM_FLAG_BUSY_BIT, (SUBCPU_SCOM_FLG_W).w
	9: /* while (!((SUBCPU_E_FLG_W).w & bit_0)) { */
		btst	#MCOM_FLAG_DONE_BIT, (SUBCPU_E_FLG_W).w
		beqs	2f /* if ((SUBCPU_E_FLG_W).w & bit_0) { */
			bset	#SCOM_FLAG_DONE_BIT, (SUBCPU_SCOM_FLG_W).w
			1: /* while () { */
				btst	#MCOM_FLAG_DONE_BIT, (SUBCPU_E_FLG_W).w
				bnes	1b
			/* } */
			bclr	#SCOM_FLAG_DONE_BIT, (SUBCPU_SCOM_FLG_W).w
			rts
		2: /* } */

		movew	(SUBCPU_CMD_BUFFER_W+2).w, %d0
		beqs	9b /* if ((SUBCPU_CMD_BUFFER_W+2).w) { */
			bsrw	sub_mmd_wait
			addw	%d0, %d0
			movew	%pc@(attack_sys_svc, %d0:w), %d0
			jsr	%pc@(attack_sys_svc, %d0:w)
			bras	9b
		/* } */
	/* } */
/* ------------------------ */
attack_sys_svc:
	.word	attack_sys_svc_0-attack_sys_svc
	.word	attack_sys_svc_1-attack_sys_svc
	.word	attack_sys_svc_2-attack_sys_svc
	.word	attack_sys_svc_3-attack_sys_svc
/* ------------------------ */
attack_sys_svc_1:
	1: /* while (sub_5F16(tbl_10188, tbl_80000, 3)) { */
		bsrw	sub_1010A

		lea	%pc@(tbl_10188), %a0
		lea	tbl_80000, %a1
		movew	#3, %d0
		jsr	sub_5F16
		bcss	1b
	/* } */

	bset	#reg_FF8003_bit_0, (SUBCPU_MEMMODE_W).w

attack_sys_svc_0:
	rts
/* ------------------------ */
attack_sys_svc_2:
	1: /* while (sub_5F16(tbl_10194, tbl_80000, 4)) { */
		bsrw	sub_1010A

		lea	%pc@(tbl_10194), %a0
		lea	tbl_80000, %a1
		movew	#4, %d0
		jsr	sub_5F16
		bcss	1b
	/* } */

	bset	#reg_FF8003_bit_0, (SUBCPU_MEMMODE_W).w
	rts
/* ------------------------ */
attack_sys_svc_3:
	movew	#BIOS_FDRCHG, %d0
	moveq	#32, %d1
	jsr	BIOS_ENTRY

	rts
/* ------------------------------------------------------- */
	.include "src/common/sub_mmd_wait.s"
/* ------------------------------------------------------- */
sub_1010A:
	1: /* while (!((SUBCPU_MEMMODE_W).w & bit_1)) { */
		btst	#reg_FF8003_bit_1, (SUBCPU_MEMMODE_W).w
		beqs	1b
	/* } */

	rts
/* ------------------------------------------------------- */
sub_10114:
	1: /* while (sub_5F16(tbl_10194, tbl_10128, 4)) { */
		lea	%pc@(tbl_10194), %a0
		lea	%pc@(tbl_10128), %a1
		movew	#4, %d0
		jsr	sub_5F16
		bcss	1b
	/* } */

	rts
/* ------------------------------------------------------- */
tbl_10128:
	.long	0x50000, 0x50000, 0x50000, 0x50000
	.long	0x50000, 0x50000, 0x50000, 0x50000
	.long	0x50000, 0x50000, 0x50000, 0x50000
	.long	0x50000, 0x50000, 0x50000, 0x50000
	.long	0x50000, 0x50000, 0x50000, 0x50000
	.long	0x50000, 0x50000, 0x50000, 0x50000

tbl_10188:
	.asciz	"CDSONIC0000"

tbl_10194:
	.asciz	"CDSONIC0000"
	.word	2

tbl_101A2:
	.space	0x640

tbl_107E2:
	.space	0x0C
