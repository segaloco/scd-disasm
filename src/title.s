/* ------------------------------------------------------- */
/* title - the title screen                                */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"

	.include "src/inc/m_subreg.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/m_map.i"
	.include "src/inc/subcall.i"
	.include "src/inc/obj_eng.i"

	.include "src/common/m_vint_bits.s"
	.include "src/common/m_obj_eng.s"

vdp_addr_0000	= 0x0000
vdp_addr_8000	= 0x8000
vdp_addr_9000	= 0x9000
vdp_addr_C000	= 0xC000
vdp_addr_E000	= 0xE000
vdp_addr_E050	= 0xE050
vdp_addr_EA50	= 0xEA50
vdp_addr_F000	= 0xF000
vdp_addr_F402	= 0xF402

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	title
	.word	(mmd_title_size/4)-1
	.long	title
	.long	0
	.long	0
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib
/* ------------------------------------------------------- */
	.equ	TIMER_LENGTH,   0x1554
	.equ	OBJ_IDX_2,      0x0c0
	.equ	OBJ_IDX_3,      0x100
	.equ	OBJ_IDX_4,      0x140
	.equ	OBJ_IDX_5,      0x180
	.equ	OBJ_IDX_6,      0x040
	.equ	OBJ_IDX_7,      0x080
	.equ	OBJ_IDX_8,      0x000
	.globl	title
title:
	movel	#title_vint, M_VECTOR_VINT
	bsrw	com_yield_subcpu

	lea	(mmd_ram_arena_l).w, %a0
	movew	#mmd_ram_arena_size_l-1, %d7
	1: /* for (long of mmd_ram_arena) { */
		movel	#0, %a0@+

		dbf	%d7, 1b
	/* } */

	lea	%pc@(title_init_vdpreg), %a0
	bsrw	lib_sys_init

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_0000&0x3FFF)<<16)+((vdp_addr_0000>>14)&0x3), VDP_ADDR
	lea	%pc@(title_nem_blk0), %a0
	bsrw	lib_nem_vdpdec

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_8000&0x3FFF)<<16)+((vdp_addr_8000>>14)&0x3), VDP_ADDR
	lea	%pc@(title_nem_blk1), %a0
	bsrw	lib_nem_vdpdec

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_9000&0x3FFF)<<16)+((vdp_addr_9000>>14)&0x3), VDP_ADDR
	lea	%pc@(title_nem_blk2), %a0
	bsrw	lib_nem_vdpdec

	lea	%pc@(title_map_blk0), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E000&0x3FFF)<<16)+((vdp_addr_E000>>14)&0x3), %d0
	movew	#0x27, %d1
	movew	#0x1b, %d2
	bsrw	lib_map_load0

	lea	%pc@(title_map_blk1), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E050&0x3FFF)<<16)+((vdp_addr_E050>>14)&0x3), %d0
	movew	#0x17, %d1
	movew	#7, %d2
	bsrw	lib_map_load0

	lea	%pc@(title_map_blk2), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_EA50&0x3FFF)<<16)+((vdp_addr_EA50>>14)&0x3), %d0
	movew	#0x17, %d1
	movew	#7, %d2
	bsrw	lib_map_load0

	lea	%pc@(title_map_blk3), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C000&0x3FFF)<<16)+((vdp_addr_C000>>14)&0x3), %d0
	movew	#0x27, %d1
	movew	#0x1b, %d2
	bsrw	lib_map_load0

	movew	#8, m_obj_mem+OBJ_IDX_8
	movew	#6, m_obj_mem+OBJ_IDX_6
	movew	#7, m_obj_mem+OBJ_IDX_7
	movew	#2, m_obj_mem+OBJ_IDX_2
	movew	#3, m_obj_mem+OBJ_IDX_3
	movew	#4, m_obj_mem+OBJ_IDX_4
	movew	#5, m_obj_mem+OBJ_IDX_5

	movew	#TIMER_LENGTH, (m_timer_l).w

	lea	%pc@(title_palette), %a0
	lea	m_mmd_palette, %a1
	moveq	#((title_palette_end-title_palette)/LONG_SIZE)-1, %d7
	1: /* for (long of title_palette) { */
		movel	%a0@+, %a1@+

		dbf	%d7, 1b
	/* } */

	bsrw	title_obj_cycle
	bsrw	lib_palfade_init
	bsrw	main_wait_int
	bsrw	lib_palfade_inb

	1: /* while (m_timer && !ctrl_start) { */
		lea	%pc@(title_is_time_attack), %a1
		moveq	#0, %d6
		moveq	#1, %d7

		btst	#CMD_BUFFER_CTRL_UP_BIT, CMD_BUFFER_CTRL
		beqs	2f /* if (ctrl_up) { */
			bsrw	lib_cursor_up_w
		2: /* } */
		btst	#CMD_BUFFER_CTRL_DOWN_BIT, CMD_BUFFER_CTRL
		beqs	2f /* if (ctrl_down) { */
			bsrw	lib_cursor_down_w
		2: /* } */

		bsrw	title_obj_cycle
		bsrw	title_calc_parallax
		bsrw	main_wait_int

		moveb	#0, title_is_game_start
		tstw	(m_timer_l).w
		beqs	1f
		moveb	#1, title_is_game_start
		btst	#CMD_BUFFER_CTRL_START_BIT, CMD_BUFFER_CTRL
		beqs	1b
	1: /* } */

	moveq	#SUBCALL_FADERS_FADE, %d0
	bsrw	subcall_wait
	bsrw	lib_palfade_outb
	movew	(title_is_time_attack).l, %d0
	moveb	(title_is_game_start).l, %d1
	rts
/* ------------------------ */
title_is_time_attack:
	.word	0
title_is_game_start:
	.word	0
/* ------------------------------------------------------- */
title_calc_parallax:
	lea	m_mmd_parallax_buf, %a0

	movel	#0x10000, %d0
	movew	#(0x60/LONG_SIZE)-1, %d7
	1: /* for () { */
		addl	%d0, %a0@+

		dbf	%d7, 1b
	/* } */

	movel	#0xc000, %d0
	movew	#(0x20/LONG_SIZE)-1, %d7
	1: /* for () { */
		addl	%d0, %a0@+

		dbf	%d7, 1b
	/* } */

	movel	#0x8000, %d0
	movew	#(0x60/LONG_SIZE)-1, %d7
	1: /* for () { */
		addl	%d0, %a0@+

		dbf	%d7, 1b
	/* } */

	movel	#0x4000, %d0
	movew	#(0x20/LONG_SIZE)-1, %d7
	1: /* for () { */
		addl	%d0, %a0@+

		dbf	%d7, 1b
	/* } */

	moveq	#0, %d0
	movew	#(0x180/LONG_SIZE)-1, %d7
	1: /* for () { */
		addl	%d0, %a0@+

		dbf	%d7, 1b
	/* } */

	moveq	#0, %d0
	movel	#0x800, %d1
	movew	#(0x100/LONG_SIZE)-1, %d7
	1: /* for () { */
		addl	%d0, %a0@+
		addl	%d1, %d0

		dbf	%d7, 1b
	/* } */

	rts
/* ------------------------ */
title_apply_parallax:
	movew	#0x8f04, VDP_CTRL
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_F402&0x3FFF)<<16)+((vdp_addr_F402>>14)&0x3), VDP_ADDR
	lea	m_mmd_parallax_buf, %a0
	lea	VDP_DATA, %a1
	movew	#((m_mmd_parallax_buf_end-m_mmd_parallax_buf)/WORD_SIZE)-1, %d7
	1: /* for (word of m_mmd_parallax_buf) { */
		movew	%a0@+, %a1@
		tstw	%a0@+

		dbf	%d7, 1b
	/* } */

	movew	#0x8f02, VDP_CTRL
	rts
/* ------------------------------------------------------- */
title_obj_cycle:
	lea	m_obj_mem, %a0
	moveq	#8-1, %d7
	1: /* for (i = 8-1; i > 0; i--) { */
		bsrs	title_obj_enter
		addaw	#OBJ_SIZE, %a0

		dbf	%d7, 1b
	/* } */

	m_obj_eng_enter

	lea	%pc@(title_obj_mem_list), %a1
	moveq	#8-1, %d7
	1: /* for () { */
		moveaw	%a1@+, %a0
		movew	%a0@, %d0
		beqs	2f /* if (obj_index != 0) { */
			movel	%d7, %sp@-
			bsrs	title_obj_refresh
			movel	%sp@+, %d7
		2: /* } */

		dbf	%d7, 1b
	/* } */

	m_obj_eng_exit

	rts
/* ------------- */
title_obj_enter:
	movew	%a0@, %d0
	beqs	9f /* if (obj_index != 0) { */
		lea	%pc@(title_obj_list-4), %a1
		addw	%d0, %d0
		addw	%d0, %d0
		moveal	%a1@(0, %d0:w), %a1
		movew	%d7, %sp@-
		jsr	%a1@
		movew	%sp@+, %d7

		btst	#0, %a0@(OBJ_STAT)
		beqs	9f /* if () { */
			moveal	%a0, %a1
			moveq	#0, %d1
			braw	lib_memset_64
		/* } */
	9: /* } */

	rts
/* ------------------------ */
title_obj_refresh:
	m_obj_stat_start
	bnes	9b

	m_obj_ani_start
	bmis	9b

	m_obj_ani_loop_start

	cmpiw	#0x60, %d0
	bles	4f

	cmpiw	#0x180, %d0
	bges	4f

	m_obj_ani_loop_main

	cmpiw	#0x60, %d0
	bles	5f

	cmpiw	#0x1c0, %d0
	bges	5f

	m_obj_ani_loop_end

3:
	m_obj_ani_loop_next

9:
	m_obj_stat_ret

4:
	addqw	#4, %a3
	bras	3b

5:
	subqw	#6, %a4
	subqb	#1, SVAR_L_fffa00
	bras	3b
/* ------------- */
title_obj_ani_cycle:
	m_obj_ani_cycle
	rts
/* ------------------------ */
title_obj_mem_list:
	.word	m_obj_mem+OBJ_IDX_8-ipx_addr_base_l
	.word	m_obj_mem+OBJ_IDX_6-ipx_addr_base_l
	.word	m_obj_mem+OBJ_IDX_7-ipx_addr_base_l
	.word	m_obj_mem+OBJ_IDX_2-ipx_addr_base_l
	.word	m_obj_mem+OBJ_IDX_3-ipx_addr_base_l
	.word	m_obj_mem+OBJ_IDX_4-ipx_addr_base_l
	.word	m_obj_mem+OBJ_IDX_5-ipx_addr_base_l
	.word	m_obj_mem+0x1c0-ipx_addr_base_l

title_obj_list:
	.long	obj_idx_8
	.long	obj_idx_6
	.long	obj_idx_7
	.long	obj_idx_2
	.long	obj_idx_3
	.long	obj_idx_4
	.long	obj_idx_5
	.long	obj_idx_1c0
/* ------------------------------------------------------- */
obj_idx_6:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_6, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_6, %d0:w)
/* ------------- */
obj_tbl_idx_6:
	.word	obj_idx_6_init-obj_tbl_idx_6
	.word	obj_idx_6_routine-obj_tbl_idx_6
/* ------------- */
obj_idx_6_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0xe000, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_6_data, %a0@(OBJ_DATAPTR)
	movew	#0x120, %a0@(OBJ_HPOS)
	movew	#0x150, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_6_routine:
	rts
/* ------------- */
obj_idx_6_data:
	.long	obj_idx_6_data_tbl
obj_idx_6_data_tbl:
	.byte	(obj_idx_6_data_tbl_end-obj_idx_6_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_6_data_tbl_start:
	.long	data_ff23d6
obj_idx_6_data_tbl_end:

data_ff23d6:
	.incbin	"data/title/data_obj_idx_6.bin"
/* ------------------------ */
obj_idx_2:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_2, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_2, %d0:w)
/* ------------- */
obj_tbl_idx_2:
	.word	obj_idx_2_init-obj_tbl_idx_2
	.word	obj_idx_2_rout1-obj_tbl_idx_2
	.word	obj_idx_2_rout2-obj_tbl_idx_2
/* ------------- */
obj_idx_2_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_2_data, %a0@(OBJ_DATAPTR)
	movew	#0x180, %a0@(OBJ_HPOS)
	movew	#0xc0, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	movew	#0x40, %a0@(OBJ_COUNTER)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_2_rout1:
	addil	#0x2000, %a0@(OBJ_VPOS)

	subqw	#1, %a0@(OBJ_COUNTER)
	bnes	1f /* if (--obj_counter == 0) { */
		addqb	#1, %a0@(OBJ_ROUTINE)
		movew	#0x40, %a0@(OBJ_COUNTER)
	1: /* } */

	rts

obj_idx_2_rout2:
	subil	#0x2000, %a0@(OBJ_VPOS)

	subqw	#1, %a0@(OBJ_COUNTER)
	bnes	1f /* if (--obj_counter == 0) { */
		subqb	#1, %a0@(OBJ_ROUTINE)
		movew	#0x40, %a0@(OBJ_COUNTER)
	1: /* } */
	
	rts
/* ------------- */
obj_idx_2_data:
	.long	obj_idx_2_data_tbl
obj_idx_2_data_tbl:
	.byte	(obj_idx_2_data_tbl_end-obj_idx_2_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_2_data_tbl_start:
	.long	data_ff24c0
obj_idx_2_data_tbl_end:

data_ff24c0:
	.incbin	"data/title/data_obj_idx_2.bin"
/* ------------------------ */
obj_idx_7:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_7, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_7, %d0:w)
/* ------------- */
obj_tbl_idx_7:
	.word	obj_idx_7_init-obj_tbl_idx_7
	.word	obj_idx_7_routine-obj_tbl_idx_7
/* ------------- */
obj_idx_7_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0xa000, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_7_data, %a0@(OBJ_DATAPTR)
	movew	#0x124, %a0@(OBJ_HPOS)
	movew	#0x108, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_7_routine:
	rts
/* ------------- */
obj_idx_7_data:
	.long	obj_idx_7_data_tbl
obj_idx_7_data_tbl:
	.byte	(obj_idx_7_data_tbl_end-obj_idx_7_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_7_data_tbl_start:
	.long	data_ff253e
obj_idx_7_data_tbl_end:

data_ff253e:
	.incbin	"data/title/data_obj_idx_7.bin"
/* ------------------------ */
obj_idx_8:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_8, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_8, %d0:w)
/* ------------- */
obj_tbl_idx_8:
	.word	obj_idx_8_init-obj_tbl_idx_8
	.word	obj_idx_8_routine-obj_tbl_idx_8
/* ------------- */
obj_idx_8_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0xc000, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_8_data, %a0@(OBJ_DATAPTR)
	movew	#0x120, %a0@(OBJ_HPOS)
	movew	#0x104, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_8_routine:
	andiw	#0x1f, (SVAR_W_fffa44_l).w
	bnes	1f /* if (!(SVAR_W_fffa44 & 0x1F)) { */
		bchg	#OBJ_STAT_INACTIVE_BIT, %a0@(OBJ_STAT)
	1: /* } */

	rts
/* ------------- */
obj_idx_8_data:
	.long	obj_idx_8_data_tbl
obj_idx_8_data_tbl:
	.byte	(obj_idx_8_data_tbl_end-obj_idx_8_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_8_data_tbl_start:
	.long	data_ff25e8
obj_idx_8_data_tbl_end:

data_ff25e8:
	.incbin	"data/title/data_obj_idx_8.bin"
/* ------------------------ */
obj_idx_4:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_4, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_4, %d0:w)
/* ------------- */
obj_tbl_idx_4:
	.word	obj_idx_4_init-obj_tbl_idx_4
	.word	obj_idx_4_routine-obj_tbl_idx_4
/* ------------- */
obj_idx_4_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0xc400, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_4_data, %a0@(OBJ_DATAPTR)
	movew	#0x120, %a0@(OBJ_HPOS)
	movew	#0xf0, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_4_routine:
	tstw	(title_is_time_attack).l
	bnes	loc_ff2654

loc_ff2644:
	andiw	#0xf, (SVAR_W_fffa44_l).w
	bnes	8f /* if (!(SVAR_W_fffa44 & 0x0F)) { */
		bchg	#OBJ_STAT_INACTIVE_BIT, %a0@(OBJ_STAT)
	8: /* } */

	rts

loc_ff2654:
	bclr	#OBJ_STAT_INACTIVE_BIT, %a0@(OBJ_STAT)
	rts
/* ------------- */
obj_idx_4_data:
	.long	obj_idx_4_data_tbl
obj_idx_4_data_tbl:
	.byte	(obj_idx_4_data_tbl_end-obj_idx_4_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_4_data_tbl_start:
	.long	data_ff2666
obj_idx_4_data_tbl_end:

data_ff2666:
	.incbin	"data/title/data_obj_idx_4.bin"
/* ------------------------ */
obj_idx_5:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_5, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_5, %d0:w)
/* ------------- */
obj_tbl_idx_5:
	.word	obj_idx_5_init-obj_tbl_idx_5
	.word	obj_idx_5_routine-obj_tbl_idx_5
/* ------------- */
obj_idx_5_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0xc400, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_5_data, %a0@(OBJ_DATAPTR)
	movew	#0x120, %a0@(OBJ_HPOS)
	movew	#0xfc, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_5_routine:
	cmpiw	#1, (title_is_time_attack).l
	beqw	loc_ff2644

	braw	loc_ff2654
/* ------------- */
obj_idx_5_data:
	.long	obj_idx_5_data_tbl
obj_idx_5_data_tbl:
	.byte	(obj_idx_5_data_tbl_end-obj_idx_5_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_5_data_tbl_start:
	.long	data_ff26ce
obj_idx_5_data_tbl_end:

data_ff26ce:
	.incbin	"data/title/data_obj_idx_5.bin"
/* ------------------------ */
obj_idx_3:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_3, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_3, %d0:w)
/* ------------- */
obj_tbl_idx_3:
	.word	obj_idx_3_init-obj_tbl_idx_3
	.word	obj_idx_3_routine-obj_tbl_idx_3
/* ------------- */
obj_idx_3_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0x4000, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_3_data, %a0@(OBJ_DATAPTR)
	movew	#0x120, %a0@(OBJ_HPOS)
	movew	#0xd8, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_3_routine:
	rts
/* ------------- */
obj_idx_3_data:
	.long	obj_idx_3_data_tbl
obj_idx_3_data_tbl:
	.byte	(obj_idx_3_data_tbl_end-obj_idx_3_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_3_data_tbl_start:
	.long	data_ff2728
obj_idx_3_data_tbl_end:

data_ff2728:
	.incbin	"data/title/data_obj_idx_3.bin"
/* ------------------------------------------------------- */
obj_idx_1c0:
	moveq	#0, %d0
	moveb	%a0@(OBJ_ROUTINE), %d0
	addw	%d0, %d0
	movew	%pc@(obj_tbl_idx_1c0, %d0:w), %d0
	jmp	%pc@(obj_tbl_idx_1c0, %d0:w)
/* ------------- */
obj_tbl_idx_1c0:
	.word	obj_idx_1c0_init-obj_tbl_idx_1c0
	.word	obj_idx_1c0_routine-obj_tbl_idx_1c0
/* ------------- */
obj_idx_1c0_init:
	bset	#OBJ_STAT_INIT_BIT, %a0@(OBJ_STAT)
	movew	#0xe480, %a0@(OBJ_W_OFF_4)
	movel	#obj_idx_1c0_data, %a0@(OBJ_DATAPTR)
	movew	#0x168, %a0@(OBJ_HPOS)
	movew	#0x158, %a0@(OBJ_VPOS)
	moveq	#0, %d0
	jsr	%pc@(title_obj_ani_cycle)
	addqb	#1, %a0@(OBJ_ROUTINE)

obj_idx_1c0_routine:
	rts
/* ------------- */
obj_idx_1c0_data:
	.long	obj_idx_1c0_data_tbl
obj_idx_1c0_data_tbl:
	.byte	(obj_idx_1c0_data_tbl_end-obj_idx_1c0_data_tbl_start)/LONG_SIZE
	.byte	0
obj_idx_1c0_data_tbl_start:
	.long	data_ff27a0
obj_idx_1c0_data_tbl_end:

data_ff27a0:
	.incbin	"data/title/data_obj_idx_1c0.bin"
/* ------------------------------------------------------- */
title_vint:
	m_vint_open
	beqw	9f /* if (m_vint_open()) { */
		bsrw	lib_int_disable
		m_vint_vdpwait
		m_vint_dispon
		m_vint_palload

		m_vdp_dma_v	vdp_addr_F000, SVAR_L_ffe000, 0x140
		bsrw	title_apply_parallax

		m_vint_finish
		m_vint_tick
	9: /* } */

    m_vint_ret
/* ------------------------------------------------------- */
	.include "src/common/com_yield_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_req_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_subflag.s"
/* ------------------------------------------------------- */
title_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x00
	.byte	0x07, 0x78, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x03
	.byte	0x81, 0x3d, 0x00, 0x02
	.byte	0x01, 0x00, 0x00
    .even
/* ------------------------------------------------------- */
title_palette:
	.incbin	"data/title/title_palette.bin"
title_palette_end:
/* ------------------------------------------------------- */
title_map_blk0:
	.incbin	"data/title/title_map_blk0.bin"
/* ------------------------------------------------------- */
title_map_blk3:
	.incbin	"data/title/title_map_blk3.bin"
/* ------------------------------------------------------- */
title_map_blk1:
	.incbin	"data/title/title_map_blk1.bin"
/* ------------------------------------------------------- */
title_map_blk2:
	.incbin	"data/title/title_map_blk2.bin"
/* ------------------------------------------------------- */
title_nem_blk1:
	.incbin	"data/title/title_nem_blk1.bin"
/* ------------------------------------------------------- */
title_nem_blk0:
	.incbin	"data/title/title_nem_blk0.bin"
/* ------------------------------------------------------- */
title_nem_blk2:
	.incbin	"data/title/title_nem_blk2.bin"
/* ------------------------------------------------------- */
