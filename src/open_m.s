/* ------------------------------------------------------- */
/* open_m - main CPU code for the opening FMV              */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"
	.include "src/inc/z80.i"

	.include "src/inc/m_vector.i"
	.include "src/inc/m_bank.i"

	.include "src/inc/m_map.i"
	.include "src/inc/obj_eng.i"

	.include "src/inc/misc.i"

	.include "src/common/lib_hw_init.s"
	.include "src/common/m_vint_bits.s"
	.include "src/common/m_obj_eng.s"
	.include "src/common/m_int_updown.s"
	.include "src/common/m_joypad_proc.s"

vdp_addr_0000	= 0x0000
vdp_addr_0E00	= 0x0E00
vdp_addr_1C00	= 0x1C00
vdp_addr_2A00	= 0x2A00
vdp_addr_3800	= 0x3800
vdp_addr_4600	= 0x4600
vdp_addr_5400	= 0x5400
vdp_addr_6200	= 0x6200
vdp_addr_7D00	= 0x7D00
vdp_addr_A000	= 0xA000
vdp_addr_C000	= 0xC000
vdp_addr_E400	= 0xE400

vdp_cram_addr_0000	= 0x0000
vdp_cram_addr_0020	= 0x0020

vdp_vsaddr_0000		= 0x0000

OPEN_VINT_NULL	 = 0
OPEN_VINT_PAGE_0 = 1
OPEN_VINT_PAGE_1 = 2
OPEN_VINT_PAGE_2 = 3
OPEN_VINT_PAGE_3 = 4
OPEN_VINT_PAGE_4 = 5
OPEN_VINT_PAGE_5 = 6
OPEN_VINT_PAGE_6 = 7
OPEN_VINT_PAGE_7 = 8

open_vint_do	= SVAR_L_fffa00
open_vdp_ctrl	= m_sr_backup_l
open_ctrl0_w	= SVAR_B_fffa4a

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	open_m_vec
	.word	(mmd_warp_size/4)-1
	.long	open_m_vec
	.long	0
	.long	open_vint_vec
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib
/* ------------------------------------------------------- */
open_m_vec:
	jmp	((ADDR_SIGN_MASK+open_m)).l
/* ------------------------------------------------------- */
open_vint_vec:
	jmp	((ADDR_SIGN_MASK+open_vint)).l

	.long	0xc040
/* ------------------------------------------------------- */

open_m:
	movew	#OPEN_VINT_NULL, m_vint_num
	movel	#(ADDR_SIGN_MASK+open_vint), (M_VECTOR_VINT-ADDR_SIGN_MASK).l
	moveb	#0, SUBCPU_E_FLG
	bsrw	open_wait_memmode

	bsrw	open_init

	lea	VDP_CTRL, %a1
	lea	VDP_DATA, %a2
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E400&0x3FFF)<<16)+((vdp_addr_E400>>14)&0x3), VDP_ADDR
	movel	#0x02000200, %a2@

	lea	mmd_ram_arena_l, %a0
	bset	#m_vdpmode1_display_bit, open_vdp_ctrl+1
	movew	open_vdp_ctrl, VDP_CTRL
	movew	#mmd_ram_arena_size_l-1, %d7
	1: /* for (long in mmd_ram_arena) { */
		movel	#0, %a0@+
		dbf	%d7, 1b
	/* } */

	bsrw	open_clearram_2
	bsrw	open_clearram_1
	bsrw	open_draw_surface
	bsrw	open_draw_border

	movew	#0, CMD_BUFFER_LOGO_FLAG

	bset	#E_FLG_STD, SUBCPU_E_FLG
	1: /* while (SCOM_BUSY) { */
		btst	#SCOM_FLAG_BUSY_BIT, SUBCPU_SCOM_FLG
		bnes	1b
	/* } */
	bclr	#E_FLG_STD, SUBCPU_E_FLG

	bsrw	open_subcom
	movew	#OPEN_VINT_NULL, m_vint_num

	6: /* while (!scom_err && !(SUBCPU_E_FLG & bit2)) { */
		movel	#(VDP_VRAM_WRITE)+((vdp_addr_E400&0x3FFF)<<16)+((vdp_addr_E400>>14)&0x3), VDP_ADDR
		movel	#0, %a2@

		lea	SUBCPU_M2_MEMBASE, %a0
		movel	%a0, SVAR_L_ffc082
		movew	#0, open_inner_loop_count
		5: /* for (open_inner_loop_count = 0; open_inner_loop_count < 8; open_inner_loop_count++) { */
			btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
			bnew	open_finally

			movel	%a0, SVAR_L_ffc082
			moveal	SVAR_L_ffc082, %a0
			lea	(mmd_ram_arena2).w, %a1
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0@+, %a1@+
			movel	%a0, SVAR_L_ffc082
			moveal	SVAR_L_ffc082, %a0
			lea	mmd_ram_arena_l, %a1
			moveq	#0, %d7
			movew	#0x37f, %d7
			bsrw	lib_copy_lines

			movel	%a0, SVAR_L_ffc082
			btst	#0, open_inner_loop_count+1
			beqs	1f /* if (odd_iter) { */
				movew	#OPEN_VINT_PAGE_0, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_PAGE_1, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_PAGE_2, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_PAGE_3, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_NULL, m_vint_num
				bsrw	open_wait_vint
				bras	2f
			1: /* } else { */
				movew	#OPEN_VINT_PAGE_4, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_PAGE_5, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_PAGE_6, m_vint_num
				bsrw	open_wait_vint
				movew	#OPEN_VINT_PAGE_7, m_vint_num
				bsrw	open_wait_vint
			2: /* } */

			bsrw	open_checkstart

			movew	#OPEN_VINT_NULL, m_vint_num
			addqw	#1, open_inner_loop_count
			cmpiw	#8, open_inner_loop_count
			bmiw	1f
			bras	2f
			1: /* if (++open_inner_loop_count < 8) { */
				bsrw	open_wait_vint
				bsrw	open_wait_vint
				bsrw	open_wait_vint

				btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
				bnes	open_finally
				btst	#E_FLG_DMNA_ERR, SUBCPU_E_FLG
				bnes	open_finally

				braw	5b
			2: /* } else { */
				btst	#E_FLG_DMNA_ERR, SUBCPU_E_FLG
				bnes	open_finally
				btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
				bnes	open_finally

				bsrw	open_subcom

				braw	6b
			/* } */
		/* } */
	/* } */

open_done:
	clrb	SUBCPU_E_FLG
	rts

open_finally:
	3: /* while (!scom_err) { */
		btst	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG
		beqs	2f /* if (SCOM_DONE) { */
			bset	#E_FLG_DMNA, SUBCPU_E_FLG
			1: /* while (SCOM_DONE) { */
				btst	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG
				bnes	1b
			/* } */

			1: /* while (SUBCPU_DMNA) { */
				btst	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
				bnes	1b
			/* } */

			bclr	#E_FLG_DMNA, SUBCPU_E_FLG
		2: /* } */

		btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
		beqs	3b
	/* } */

	movew	#OPEN_VINT_NULL, m_vint_num
	movew	#0x3c, %d1
	1: /* for (i = 0x3C; i > 0; i--) { */
		bsrw	open_wait_vint
		dbf	%d1, 1b
	/* } */

	bset	#E_FLG_M_DONE, SUBCPU_E_FLG
	1: /* while (scom_err) { */
		btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
		bnes	1b
	/* } */

	bras	open_done
/* ------------------------------------------------------- */
open_draw_surface:
	moveml	%d0-%fp, %sp@-

	lea	VDP_ADDR, %a0
	lea	VDP_DATA, %a1

	moveq	#0, %d0
	moveq	#0, %d2
	moveq	#0, %d3
	movew	#0x1c0, %d3
	moveq	#0, %d4
	moveq	#0, %d7
	movew	#((VDP_VRAM_WRITE_H)+(vdp_addr_C000&0x3FFF)), %a0@
	movew	#((VDP_VRAM_WRITE_L)+((vdp_addr_C000>>14)&0x3)), %a0@
	5: /* for (j = 0; j < 32; j++) { */
		moveq	#0, %d1
		4: /* for (i = 0; i < 128; i++) { */
			cmpiw	#7, %d0
			bmis	2f
			cmpiw	#21, %d0
			bpls	2f
			cmpiw	#4, %d1
			bmis	1f
			cmpiw	#36, %d1
			bpls	1f /* if (j >= 7 && j <= 21 && i >= 4 && i <= 36) { */
				movew	%d2, %d4
				addqw	#1, %d2
				bras	3f
			1:
			cmpiw	#68, %d1
			bmis	2f
			cmpiw	#100, %d1
			bpls	2f /*  } else if (j >= 7 && j <= 21 && i >= 68 && i <= 100) { */
				movew	%d3, %d4
				addqw	#1, %d3
				bras	3f
			2: /* } else { */
				movew	#0x3e8, %d4
			3: /* } */

			movew	%d4, %a1@
			addqw	#1, %d1
			cmpiw	#128, %d1
			bmis	4b
		/* } */

		addqw	#1, %d0
		cmpiw	#32, %d0
		bmis	5b
	/* } */

	moveml	%sp@+, %d0-%fp
	rts
/* ------------------------------------------------------- */
open_vint:
	moveml	%d0-%fp, %sp@-
	moveb	#1, SUBCPU_INT
	tstb	open_vint_do
	beqw	open_vint_end_exit

	moveb	#0, open_vint_do
	bclr	#6, open_vdp_ctrl+1
	movew	open_vdp_ctrl, VDP_CTRL
	lea	VDP_CTRL, %a1
	lea	VDP_DATA, %a2
	movew	%a1@, %d0
	jsr	%pc@(lib_int_disable)
	movew	m_vint_num, %d0
	addw	%d0, %d0
	movew	%pc@(open_vint_tbl, %d0:w), %d0
	jmp	%pc@(open_vint_tbl, %d0:w)
/* ------------- */
open_vint_tbl:
	.word	open_vint_null-open_vint_tbl
	.word	open_vint_page_0-open_vint_tbl
	.word	open_vint_page_1-open_vint_tbl
	.word	open_vint_page_2-open_vint_tbl
	.word	open_vint_page_3-open_vint_tbl
	.word	open_vint_page_4-open_vint_tbl
	.word	open_vint_page_5-open_vint_tbl
	.word	open_vint_page_6-open_vint_tbl
	.word	open_vint_page_7-open_vint_tbl
/* ------------- */
open_vint_null:
	braw	open_vint_end
/* ------------- */
open_vint_page_0:
	m_vdp_dma_c	vdp_cram_addr_0000, (mmd_ram_arena2_l+0x20), 0x10

	lea	mmd_ram_arena2_l, %a0
	lea	mmd_ram_arena2_l+0x20, %a1
	moveq	#3, %d7
	bsrw	lib_copy_lines

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E400&0x3FFF)<<16)+((vdp_addr_E400>>14)&0x3), VDP_ADDR
	movel	#0x2000000, %a2@

	m_vdp_dma_v	vdp_addr_0000, (mmd_ram_arena_l+0x0000), 0x700

	braw	open_vint_end
/* ------------- */
open_vint_page_1:
	m_vdp_dma_v	vdp_addr_0E00, (mmd_ram_arena_l+0x0E00), 0x700
	
	braw	open_vint_end
/* ------------- */
open_vint_page_2:
	m_vdp_dma_v	vdp_addr_1C00, (mmd_ram_arena_l+0x1C00), 0x700

	braw	open_vint_end
/* ------------- */
open_vint_page_3:
	m_vdp_dma_v	vdp_addr_2A00, (mmd_ram_arena_l+0x2A00), 0x700

	braw	open_vint_end
/* ------------- */
open_vint_page_4:
	m_vdp_dma_c	vdp_cram_addr_0000, (mmd_ram_arena2_l+0x20), 0x10

	lea	mmd_ram_arena2_l, %a0
	lea	mmd_ram_arena2_l+0x20, %a1
	moveq	#3, %d7
	bsrw	lib_copy_lines

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E400&0x3FFF)<<16)+((vdp_addr_E400>>14)&0x3), VDP_ADDR
	movel	#0, %a2@

	m_vdp_dma_v	vdp_addr_3800, (mmd_ram_arena_l+0x0000), 0x700

	braw	open_vint_end
/* ------------- */
open_vint_page_5:
	m_vdp_dma_v	vdp_addr_4600, (mmd_ram_arena_l+0x0E00), 0x700

	braw	open_vint_end
/* ------------- */
open_vint_page_6:
	m_vdp_dma_v	vdp_addr_5400, (mmd_ram_arena_l+0x1C00), 0x700

	braw	open_vint_end
/* ------------- */
open_vint_page_7:
	m_vdp_dma_v	vdp_addr_6200, (mmd_ram_arena_l+0x2A00), 0x700
/* ------------- */
open_vint_end:
	bset	#6, open_vdp_ctrl+1
	movew	open_vdp_ctrl, VDP_CTRL

	jsr	%pc@(lib_joypad_read)
	bsrw	lib_int_enable

	m_vint_tick

open_vint_end_exit:
	m_vint_ret
/* ------------------------------------------------------- */
open_clearram_2:
	moveq	#0, %d1
	lea	mmd_ram_arena2_l, %a1
	bsrw	lib_memset_48

	rts
/* ------------------------------------------------------- */
open_clearram_1:
	moveq	#0, %d1
	movew	#129, %d0
	lea	mmd_ram_arena_l, %a1
	1: /* for (i = 129, i > 0; i--) { */
		bsrw	lib_memset_128
		
		dbf	%d0, 1b
	/* } */

	rts
/* ------------------------------------------------------- */
open_wait_memmode:
	1: /* while (SUBCPU_MODE_2M) { */
		btst	#SUBCPU_MODE_1M_BIT, SUBCPU_MEMMODE
		beqs	1b
	/* } */

	rts
/* ------------------------------------------------------- */
open_wait_vint:
	moveb	#1, open_vint_do
	1: /* while (open_vint_do) { */
		tstb	open_vint_do
		bnes	1b
	/* } */

	rts
/* ------------------------------------------------------- */
open_checkstart:
	btst	#CMD_BUFFER_CTRL_START_BIT, open_ctrl0_w
	beqs	1f /* if (open_ctrl0_w & bit7) { */
		bset	#E_FLG_DMNA_ERR, SUBCPU_E_FLG
	1: /* } */

	rts
/* ------------------------------------------------------- */
	.include	"src/common/lib_memset.s"
/* ------------------------------------------------------- */
open_clear_size	= 0xFFFF

open_init:
	lea	%pc@(open_init_vdpreg), %a0
	m_lib_hw_init

	lea	VDP_CTRL, %fp
	movew	#0x8F01, %fp@
	movel	#((VDPREG_DMALEN_L+(open_clear_size>>8))<<16)+(VDPREG_DMALEN_H+(open_clear_size&0xFF)), %fp@
	movew	#0x9780, %fp@
	movel	#(VDP_VRAM_WRITE)+(VDP_ADDR_DMA_MASK), %fp@
	movew	#0, VDP_DATA
	1: /* while () { */
		btst	#1, %fp@(1)
		bnes	1b
	/* } */

	movel	#VDP_VRAM_WRITE, %fp@
	movew	#0, VDP_DATA
	movew	#0x8F02, %fp@

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C000&0x3FFF)<<16)+((vdp_addr_C000>>14)&0x3), VDP_ADDR
	movew	#0xFFF,%d7
	1: /* for (i = 0xFFF; i > 0; i--) { */
		movew	#0xE7E1, VDP_DATA
		dbf	%d7, 1b
	/* } */

	movel	#VDP_CRAM_WRITE, VDP_ADDR
	lea	%pc@(open_palette), %a0
	moveq	#((open_palette_end-open_palette)/LONG_SIZE)-1, %d7
	1: /* for (long of palette_data) { */
		movel	%a0@+, VDP_DATA

		dbf	%d7, 1b
	/* } */

	movel	#(VDP_VSRAM_WRITE)+((vdp_vsaddr_0000&0x3FFF)<<16)+((vdp_vsaddr_0000>>14)&0x3), VDP_ADDR
	movel	#0, VDP_DATA
	bsrw	lib_int_enable

	movew	#0x8134, open_vdp_ctrl
	rts
/* ------------------------ */
open_palette:
	.incbin	"data/open/open_palette_0.bin"
	.incbin	"data/open/open_palette_1.bin"
open_palette_end:

open_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x00
	.byte	0x05, 0x70, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x00
	.byte	0x81, 0x39, 0x00, 0x02
	.byte	0x03, 0x00, 0x00, 0x00
/* ------------------------------------------------------- */
lib_int_disable:
	m_int_disable	m_sr_backup_2_l
	rts

lib_int_enable:
	m_int_enable	m_sr_backup_2_l
	rts
/* ------------------------------------------------------- */
lib_joypad_read:
	lea	open_ctrl0_w, %a0
	lea	0xa10003, %a1
	bsrs	lib_joypad_read_com

lib_joypad_read_b:
	lea	SVAR_B_fffa4c, %a0
	lea	0xa10005, %a1

lib_joypad_read_com:
	m_joypad_proc

	rts
/* ------------------------------------------------------- */
lib_copy_lines:
	1: /* for (i; i > 0; i--) { */
		movel	%a0@+, %a1@+
		movel	%a0@+, %a1@+
		movel	%a0@+, %a1@+
		movel	%a0@+, %a1@+

		dbf	%d7, 1b
	/* } */

	rts
/* ------------------------------------------------------- */
open_subcom:
	1: /* while (!scom_done) { */
		btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
		bnew	open_finally
		btst	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG
		beqs	1b
	/* } */
	btst	#SCOM_FLAG_ERR_BIT, SUBCPU_SCOM_FLG
	bnew	open_finally /* if (!scom_err) { */
		bset	#E_FLG_DMNA, SUBCPU_E_FLG
		1: /* while (dmna) { */
			btst	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
			bnes	1b
		/* } */
		bclr	#E_FLG_DMNA, SUBCPU_E_FLG

		rts
	/* } */
/* ------------------------------------------------------- */
open_draw_border:
	lea	mmd_ram_arena_l, %a1
	lea	(ADDR_SIGN_MASK+open_border_tiles).l, %a0
	movel	#0x1C0, %d7
	bsrw	lib_copy_lines

	m_vdp_dma_v	vdp_addr_7D00, (mmd_ram_arena_l+0x0000), 0xE00

	lea	mmd_ram_arena_l, %a1
	lea	(ADDR_SIGN_MASK+open_border_palette).l, %a0
	moveq	#2, %d7
	bsrw	lib_copy_lines

	m_vdp_dma_c	vdp_cram_addr_0020, (mmd_ram_arena_l+0x0000), 0x10

	lea	VDP_ADDR, %a0
	lea	VDP_DATA, %a1
	lea	(ADDR_SIGN_MASK+open_border_mapping).l, %a2
	movew	#(VDP_VRAM_WRITE_H)+(vdp_addr_A000&0x3FFF), %a0@
	movew	#(VDP_VRAM_WRITE_L)+((vdp_addr_A000>>14)&0x3), %a0@
	moveq	#0, %d1
	moveq	#0, %d0
	1: /* for (i = 0; i < 0x1D; i++) { */
		moveq	#0, %d1
		2: /* for (j = 0; j < 0x80; j++) { */
			cmpiw	#0x27, %d1
			bgtw	3f /* if (j <= 0x27) { */
				movew	%a2@+, %a1@

				bras	4f
			3: /* } else { */
				movew	#0x3e8, %a1@
			4: /* } */

			addqw	#1, %d1
			cmpiw	#0x80, %d1
			bmis	2b
		/* } */

		addqw	#1, %d0
		cmpiw	#0x1d, %d0
		bmis	1b
	/* } */

	rts
/* ------------------------ */
open_border_tiles:
	.incbin	"data/open/border_tiles.bin"

open_border_mapping:
	.incbin	"data/open/border_mapping.bin"

open_border_palette:
	.incbin	"data/open/border_palette.bin"
/* ------------------------------------------------------- */
