    .ifndef OS_I
	.equ	OS_I, 1
/* ------------------------------------------------------- */
	.equ	OS_ENTRY_INIT,      0
	.equ	OS_ENTRY_VINT,      1
	.equ	OS_ENTRY_CDC_WAIT,  2
	.equ	OS_ENTRY_CDC_INIT,  3
	.equ	OS_ENTRY_CDC_LOAD,  4
	.equ	OS_ENTRY_BLOCK_PREP,    5
	.equ	OS_ENTRY_BLOCK_LOAD,    6
	.equ	OS_ENTRY_UNWIND,    7

	.equ	OS_SUCCESS,	0x64
	.equ	OS_ERR_ff9c,	0xff9c
	.equ	OS_ERR_fffd,	0xfffd
	.equ	OS_ERR_fffe,	0xfffe
	.equ	OS_ERR_ffff,	0xffff
	.equ	OS_FSNAME_LEN,	0xc

	/* Note: for now these *must* be synchronized with
	 * the addresses generated for symbols
	 * "os_loadfile_idx" and "os_entry" when building
	 * cdhead.bin
	 */
	.equ	OS_LOADFILE_IDX_ADDR,	0x7840
	.equ	OS_ENTRY_ADDR,		0x7880
/* ------------------------------------------------------- */
    .endif
