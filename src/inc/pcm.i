    .ifndef PCM_I
	.equ	PCM_I, 1
/* ------------------------------------------------------- */
	.equ	PCM_BASE,               0x18000
	.equ	PCM_TIMER_ENTRY_OFF,    0x10
	.equ	PCM_TIMER_ENTRY,        PCM_BASE+PCM_TIMER_ENTRY_OFF
	.equ	PCM_VAR_22_OFF,         0x22
	.equ	PCM_VAR_22,             PCM_BASE+PCM_VAR_22_OFF
/* ------------------------------------------------------- */
    .endif
