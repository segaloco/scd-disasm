    .ifndef S_BIOS_I
	.equ	S_BIOS_I, 1
/* ------------------------------------------------------- */
	.equ	BIOS_1a02,      0x1a02
    
	.equ	BIOS_R_CDSTAT,  0x5e80
	.equ	BIOS_WAITVSYNC, 0x5f10
	.equ	BIOS_ENTRY,     0x5f22
	.equ	BIOS_DRVINIT,   0x10
	.equ	BIOS_MSCPLAY1,  0x12
	.equ	BIOS_MSCPLAYR,  0x13
	.equ	BIOS_ROMREADN,  0x20
	.equ	BIOS_CDBSTAT,   0x81
	.equ	BIOS_FDRSET,    0x85
	.equ	BIOS_FDRCHG,    0x86
	.equ	BIOS_CDCSTAT,   0x8a
	.equ	BIOS_CDCREAD,   0x8b
	.equ	BIOS_CDCTRN,    0x8c
	.equ	BIOS_CDCACK,    0x8d
	.equ	BIOS_TIMERINT,  0x5f84
/* ------------------------------------------------------- */
    .endif
