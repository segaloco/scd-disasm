    .ifndef Z80_I
	.equ	Z80_I, 1
/* ------------------------------------------------------- */
	.equ	Z80_RAM,    0xa00000
	.equ	z80addr_1c0a, Z80_RAM+0x1c0a
	.equ	z80addr_1c3c, Z80_RAM+0x1c3c
	.equ	Z80_BUSREQ, 0xa11100
	.equ	Z80_RESET,  0xa11200
/* ------------------------------------------------------- */
    .endif
