	.ifndef MISC_I
	.equ	MISC_I, 1
/* ------------------------------------------------------- */
	.equ	ADDR_SIGN_MASK, 0xFF000000
	.equ	WORD_SIZE,	2
	.equ	LONG_SIZE,	4
	.equ	INIT_STRING,	0x696E6974
/* ------------------------------------------------------- */
	.endif
