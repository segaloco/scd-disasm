    .ifndef M_SUBREG_I
	.equ	M_SUBREG_I, 1
/* ------------------------------------------------------- */
	.equ	SUBCPU_INT,         0xa12000
	.equ	SUBCPU_BUS,         0xa12001
	.equ	SUBCPU_MEMMODE,     0xa12003
	.equ	SUBCPU_E_FLG,       0xa1200e
	.equ	SUBCPU_SCOM_FLG,    0xa1200f
	.equ	SUBCPU_CMD_BUFFER,  0xa12010
	.equ	SUBCPU_STAT_BUFFER, 0xa12020

	.include "src/inc/subreg.i"

	.equ	CMD_BUFFER_SUBCALL,         SUBCPU_CMD_BUFFER+CMD_BUFFER_SUBCALL_OFF
	.equ	CMD_BUFFER_LOGO_FLAG,       SUBCPU_CMD_BUFFER+CMD_BUFFER_LOGO_FLAG_OFF
	.equ	CMD_BUFFER_CTRL,            SUBCPU_CMD_BUFFER+CMD_BUFFER_CTRL_OFF
	.equ	STAT_BUFFER_SUBCALL,        SUBCPU_STAT_BUFFER+CMD_BUFFER_SUBCALL_OFF
	.equ	STAT_BUFFER_LOGO_FLAG,      SUBCPU_STAT_BUFFER+CMD_BUFFER_LOGO_FLAG_OFF
/* ------------------------------------------------------- */
    .endif
