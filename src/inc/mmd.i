    .ifndef MMD_I
	.equ	MMD_I, 1
/* ------------------------------------------------------- */
	.equ	MMD_DMNA_OFF,       0x0
	.equ	MMD_DMNA_BIT,   6
	.equ	MMD_DMNA_MASK,  0x40
	.equ	MMD_BASEADDR_OFF,   0x2
	.equ	MMD_SIZE_OFF,       0x6
	.equ	MMD_ENTRY_OFF,      0x8
	.equ	MMD_HINT_OFF,       0xe
	.equ	MMD_VINT_OFF,       0x10
	.equ	MMD_DATA_OFF,       0x100
/* ------------------------------------------------------- */
    .endif
