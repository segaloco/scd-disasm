    .ifndef OBJ_ENG_I
	.equ	OBJ_ENG_I, 1
/* ------------------------------------------------------- */
	.equ	OBJ_INDEX,     0x00
	.equ	OBJ_STAT,      0x02
	.equ	OBJ_STAT_CLEAR_BIT,    0
	.equ	OBJ_STAT_INIT_BIT,     1
	.equ	OBJ_STAT_INACTIVE_BIT, 2
	.equ	OBJ_ROUTINE,   0x03
	.equ	OBJ_W_OFF_4,   0x04
	.equ	OBJ_DATAPTR,   0x06
	.equ	OBJ_ANI_FRAME, 0x0a
	.equ	OBJ_ANI_CUE,   0x0b
	.equ	OBJ_ANI_TIME,  0x0c
	.equ	OBJ_ANI_LEN,   0x0d
	.equ	OBJ_COUNTER,   0x0e
	.equ	OBJ_HPOS,      0x10
	.equ	OBJ_VPOS,      0x14
	.equ	OBJ_W_OFF_18,  0x18
	.equ	OBJ_W_OFF_1C,  0x1C

	.equ	OBJ_SIZE,      0x40
/* ------------------------------------------------------- */
    .endif
