    .ifndef M_MMD_I
	.equ	M_MMD_I, 1
/* ------------------------------------------------------- */
	.include "src/inc/mmd.i"
	.include "src/inc/m_bank.i"

	.equ	MMD_DMNA,           SUBCPU_M2_MEMBASE+MMD_DMNA_OFF
	.equ	MMD_BASEADDR,       SUBCPU_M2_MEMBASE+MMD_BASEADDR_OFF
	.equ	MMD_SIZE,           SUBCPU_M2_MEMBASE+MMD_SIZE_OFF
	.equ	MMD_ENTRY,          SUBCPU_M2_MEMBASE+MMD_ENTRY_OFF
	.equ	MMD_HINT,           SUBCPU_M2_MEMBASE+MMD_HINT_OFF
	.equ	MMD_VINT,           SUBCPU_M2_MEMBASE+MMD_VINT_OFF
	.equ	MMD_DATA,           SUBCPU_M2_MEMBASE+MMD_DATA_OFF
/* ------------------------------------------------------- */
    .endif
