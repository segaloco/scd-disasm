    .ifndef SUBREG_I
	.equ	SUBREG_I, 1
/* ------------------------------------------------------- */
	.equ	SUBCPU_INT_BIT,     0
	.equ	SUBCPU_INT_MASK,    1

	.equ	SUBCPU_BUSREQ_BIT,  1

	.equ	SUBCPU_MODE_1M_BIT, 2
	.equ	SUBCPU_MODE_2M,     0b00000000
	.equ	SUBCPU_MODE_1M,     0b00000100
	.equ	SUBCPU_DMNA_BIT,    1
	.equ	SUBCPU_DMNA,        0b00000010
	.equ	SUBCPU_RET_BIT,     0
	.equ	SUBCPU_RET,         0b00000001

	.equ	CDC_DATA_READY_BIT, 6
	.equ	CDC_DATA_EOT_BIT,   7
	.equ	CDC_SUBCPU_READ,    0b011
	.equ	CDC_MAINCPU_READ,   0b010

	.equ	E_FLG_DMNA,	    0
	.equ	E_FLG_STD,	    1
	.equ	E_FLG_DMNA_ERR,	    2
	.equ	E_FLG_M_DONE,	    4

	.equ	MCOM_FLAG_DONE_BIT, 0

	.equ	SCOM_FLAG_BUSY_BIT, 7
	.equ	SCOM_FLAG_ERR_BIT,  1
	.equ	SCOM_FLAG_DONE_BIT, 0

	.equ	CMD_BUFFER_SUBCALL_OFF,     0
	.equ	CMD_BUFFER_LOGO_FLAG_OFF,   2
	.equ	CMD_BUFFER_CTRL_OFF,      0xF
	.equ	CMD_BUFFER_CTRL_UP_BIT,        0
	.equ	CMD_BUFFER_CTRL_DOWN_BIT,      1
	.equ	CMD_BUFFER_CTRL_START_BIT,     7

	.equ	INT_MASK_GFX_DONE_BIT,  1
	.equ	INT_MASK_TIMER_BIT,     3
/* ------------------------------------------------------- */
    .endif
