/* ------------------------------------------------------- */
/* ipx - (init) executive                                  */
/*  the IPX represents the core process of the title,      */
/*  managing state and cycling through the various modes   */
/*  of execution                                           */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"

	.include "src/inc/m_bank.i"
	.include "src/inc/m_subreg.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/subcall.i"
	.include "src/inc/m_mmd.i"

	.include "src/inc/m_map.i"

	.include "src/common/mmd_load_m.s"

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	ipx
	.word	(ipx_buffer_size/4)-1
	.long	ipx
	.long	0
	.long	0
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib     /* prevents unnecessary padding */
/* ------------------------------------------------------- */
	.equ	IPX_LEVEL_ENTRY_SIZE, level_000_end-level_000
	.equ	IPX_LEVEL_SUBCALL_OFF, 0
	.equ	IPX_LEVEL_ACT_OFF,     2
	.equ	IPX_ACT_NULL,      0
	.equ	IPX_ACT_1,         0
	.equ	IPX_ACT_2,         1
	.equ	IPX_LEVEL_TIME_OFF,    4
	.equ	IPX_LEVEL_TIME_NULL,    0
	.equ	IPX_LEVEL_TIME_PAST,    0
	.equ	IPX_LEVEL_TIME_PRESENT, 1
	.equ	IPX_LEVEL_TIME_FUTURE,  2
	.equ	IPX_LEVEL_FUTURE_OFF,  5
	.equ	IPX_LEVEL_FUTURE_NULL,  0
	.equ	IPX_LEVEL_FUTURE_BAD,   0
	.equ	IPX_LEVEL_FUTURE_GOOD,  1
ipx:
	movel	#subcpu_int, M_VECTOR_VINT
	bset	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE

	moveq	#SUBCALL_LOAD_LOGO, %d0
	bsrw	ipx_file_load

ipx_main_loop:
	moveq	#SUBCALL_LOAD_TITLE, %d0
	bsrw	ipx_file_load
	tstb	%d1
	beqw	ipx_title_timeout

	tstw	%d0
	beqw	ipx_load_level

	braw	ipx_timeattack
/* ------------- */
	moveq	#SUBCALL_LOAD_SSEL, %d0
	bsrw	ipx_file_load
	muluw	#IPX_LEVEL_ENTRY_SIZE, %d0
	movew	%pc@(level_tbl+IPX_LEVEL_ACT_OFF,     %d0:w), ipx_level_act
	moveb	%pc@(level_tbl+IPX_LEVEL_TIME_OFF,    %d0:w), ipx_level_time
	moveb	%pc@(level_tbl+IPX_LEVEL_FUTURE_OFF,  %d0:w), ipx_level_future
	movew	%pc@(level_tbl+IPX_LEVEL_SUBCALL_OFF, %d0:w), %d0
	moveb	#0, ipx_is_attack
	bsrw	ipx_file_load
	braw	ipx_main_loop
/* ------------------------ */
level_tbl:
level_000:
	.word	SUBCALL_LOAD_R11A
	.word	IPX_ACT_1
	.byte	IPX_LEVEL_TIME_PRESENT
	.byte	IPX_LEVEL_FUTURE_NULL
level_000_end:

	.word	SUBCALL_LOAD_R11B
	.word	IPX_ACT_1
	.byte	IPX_LEVEL_TIME_PAST
	.byte	IPX_LEVEL_FUTURE_NULL

	.word	SUBCALL_LOAD_R11C
	.word	IPX_ACT_1
	.byte	IPX_LEVEL_TIME_FUTURE
	.byte	IPX_LEVEL_FUTURE_GOOD

	.word	SUBCALL_LOAD_R11D
	.word	IPX_ACT_1
	.byte	IPX_LEVEL_TIME_FUTURE
	.byte	IPX_LEVEL_FUTURE_BAD

	.word	SUBCALL_LOAD_R12A
	.word	IPX_ACT_2
	.byte	IPX_LEVEL_TIME_PRESENT
	.byte	IPX_LEVEL_FUTURE_NULL

	.word	SUBCALL_LOAD_R12B
	.word	IPX_ACT_2
	.byte	IPX_LEVEL_TIME_PAST
	.byte	IPX_LEVEL_FUTURE_NULL

	.word	SUBCALL_LOAD_R12C
	.word	IPX_ACT_2
	.byte	IPX_LEVEL_TIME_FUTURE
	.byte	IPX_LEVEL_FUTURE_GOOD

	.word	SUBCALL_LOAD_R12D
	.word	IPX_ACT_2
	.byte	IPX_LEVEL_TIME_FUTURE
	.byte	IPX_LEVEL_FUTURE_BAD

	.word	SUBCALL_LOAD_WARP
	.word	IPX_ACT_NULL
	.byte	IPX_LEVEL_TIME_NULL
	.byte	IPX_LEVEL_FUTURE_NULL

	.word	SUBCALL_LOAD_OPEN
	.word	IPX_ACT_NULL
	.byte	IPX_LEVEL_TIME_NULL
	.byte	IPX_LEVEL_FUTURE_NULL

	.word	SUBCALL_LOAD_COME
	.word	IPX_ACT_NULL
	.byte	IPX_LEVEL_TIME_NULL
	.byte	IPX_LEVEL_FUTURE_NULL
/* ------------------------ */
ipx_title_timeout:
	moveq	#SUBCALL_LOAD_OPEN, %d0
	bsrw	ipx_file_load
	braw	ipx_main_loop
/* ------------- */
ipx_load_level:
	moveb	#IPX_LEVEL_TIME_PRESENT, ipx_level_time
	moveb	#0, ipx_is_attack
	bsrw	ipx_level_r11
	tstb	SVAR_B_ff1212
	beqs	1f /* if (SVAR_B_ff1212) { */
		bsrw	ipx_level_r12
	1: /* } */

	moveq	#SUBCALL_LOAD_COME, %d0
	bsrw	ipx_file_load
	braw	ipx_main_loop
/* ------------------------------------------------------- */
	.equ	IPX_LEVEL_PTR_SIZE,    2
	.equ	IPX_LEVEL_PTR_PRESENT, _ipx_level_ptr_present /* see sp_os */
	.equ	IPX_LEVEL_PTR_PAST,    1*IPX_LEVEL_PTR_SIZE
	.equ	IPX_LEVEL_PTR_GOODF,   2*IPX_LEVEL_PTR_SIZE
	.equ	IPX_LEVEL_PTR_BADF,    3*IPX_LEVEL_PTR_SIZE
ipx_level_r11:
	lea	%pc@(ipx_r11_ptrs), %a0
	movew	#IPX_ACT_1, ipx_level_act
	braw	ipx_level_doload
/* ------------------------ */
ipx_level_r12:
	lea	%pc@(ipx_r12_ptrs), %a0
	movew	#IPX_ACT_2, ipx_level_act
	braw	ipx_level_doload
/* ------------------------ */
ipx_level_doload:
	movew	%a0@((IPX_LEVEL_PTR_PRESENT).b), %d0

ipx_level_dowarp:
	bsrw	ipx_file_load
	tstb	SVAR_B_ff1212
	beqs	ipx_level_nowarp

	btst	#7, ipx_level_time
	beqs	ipx_level_nowarp

	moveq	#SUBCALL_LOAD_WARP, %d0
	bsrw	ipx_file_load
	moveb	ipx_level_time, %d1
	movew	%a0@(IPX_LEVEL_PTR_PAST), %d0
	andib	#0x7f, %d1
	beqs	ipx_level_dowarp

	movew	%a0@((IPX_LEVEL_PTR_PRESENT).b), %d0
	subqb	#IPX_LEVEL_TIME_PRESENT, %d1
	beqs	ipx_level_dowarp

	movew	%a0@(IPX_LEVEL_PTR_BADF), %d0
	tstb	ipx_level_future
	beqs	ipx_level_dowarp

	movew	%a0@(IPX_LEVEL_PTR_GOODF), %d0
	bras	ipx_level_dowarp

ipx_level_nowarp:
	rts
/* ------------------------ */
ipx_r11_ptrs:
	.word	SUBCALL_LOAD_R11A
	.word	SUBCALL_LOAD_R11B
	.word	SUBCALL_LOAD_R11C
	.word	SUBCALL_LOAD_R11D
ipx_r12_ptrs:
	.word	SUBCALL_LOAD_R12A
	.word	SUBCALL_LOAD_R12B
	.word	SUBCALL_LOAD_R12C
	.word	SUBCALL_LOAD_R12D
/* ------------------------------------------------------- */
ipx_timeattack:
	moveq	#SUBCALL_LOAD_ATTACK, %d0
	bsrw	ipx_file_load
	movew	%d0, SVAR_W_ff0594
	beqw	ipx_main_loop

	addw	%d0, %d0
	movew	%pc@(ipx_timeattack_tbl-2, %d0:w), %d0
	moveb	#1, ipx_is_attack
	bsrw	ipx_file_load
	movel	SVAR_L_ff1212, SVAR_L_ff0590
	bras	ipx_timeattack
/* ------------------------ */
ipx_timeattack_tbl:
	.word	SUBCALL_LOAD_R11A
	.word	SUBCALL_LOAD_R12A
/* ------------------------------------------------------- */
	.globl	ipx_file_load
ipx_file_load:
	movel	%a0, %sp@-

	bsrw	subcall_wait
	movel	MMD_ENTRY, %d0
	beqs	3f /* if (mmd_entry) { */
		moveal	%d0, %a0
		mmd_load_m
		btst	#MMD_DMNA_BIT, MMD_DMNA
		beqs	1f /* if (mmd_dmna & dmna_bit) { */
			bset	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
		1: /* } */

		jsr	%a0@
		moveb	#0, m_vint_call_mask
		movel	#subcpu_int_ret, M_VECTOR_HINT
		movel	#subcpu_int, M_VECTOR_VINT
		movew	#0x8134, m_vdpmode1_cpy
		movew	#0x8134, VDP_CTRL
		bset	#SUBCPU_DMNA_BIT, SUBCPU_MEMMODE
	3: /* } */

	moveal	%sp@+, %a0
	rts
