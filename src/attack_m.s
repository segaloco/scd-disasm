/* ------------------------------------------------------- */
/* attack_m - Time attack main CPU routines                */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"

	.include "src/inc/m_subreg.i"

	.include "src/inc/m_vector.i"
	.include "src/inc/m_bank.i"
	.include "src/inc/m_map.i"
	.include "src/common/m_vdp_dma.s"
	.include "src/common/m_vint_bits.s"

vdp_incr_0080	= 0x0080

vdp_addr_0000	= 0x0000
vdp_addr_2000	= 0x2000
vdp_addr_C12A	= 0xC12A
vdp_addr_C1AA	= 0xC1AA
vdp_addr_C1BC	= 0xC1BC
vdp_addr_C22A	= 0xC22A
vdp_addr_C2AA	= 0xC2AA
vdp_addr_C32A	= 0xC32A
vdp_addr_C3AA	= 0xC3AA
vdp_addr_C42A	= 0xC42A
vdp_addr_C4AA	= 0xC4AA
vdp_addr_C52A	= 0xC52A
vdp_addr_C5AA	= 0xC5AA
vdp_addr_C62A	= 0xC62A
vdp_addr_C6AA	= 0xC6AA
vdp_addr_C72A	= 0xC72A
vdp_addr_C7AA	= 0xC7AA
vdp_addr_C82A	= 0xC82A
vdp_addr_C8AA	= 0xC8AA
vdp_addr_C92A	= 0xC92A
vdp_addr_C9AA	= 0xC9AA
vdp_addr_CA2A	= 0xCA2A
vdp_addr_CAAA	= 0xCAAA
vdp_addr_CB2A	= 0xCB2A
vdp_addr_CBAA	= 0xCBAA
vdp_addr_CC2A	= 0xCC2A
vdp_addr_CCAA	= 0xCCAA
vdp_addr_CD2A	= 0xCD2A
vdp_addr_D106	= 0xD106
vdp_addr_D386	= 0xD386
vdp_addr_E386	= 0xE386
vdp_addr_F400	= 0xF400

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	attack_m
	.word	(mmd_warp_size/4)-1
	.long	attack_m
	.long	0
	.long	0
	.align	256, 0
/* ------------------------------------------------------- */
	.section .text.m_lib
attack_m:
	movel	#attack_vint, M_VECTOR_VINT
	bsrw	com_yield_subcpu
	bsrw	com_wait_subcpu
	bsrw	com_wait_subcpu_init

	lea	(mmd_ram_arena).w, %a0
	movew	#(mmd_ram_arena_size_l)-1, %d7
	1: /* for (long in mmd_ram_arena) { */
		movel	#0, %a0@+
		dbf	%d7, 1b
	/* } */

	lea	%pc@(lib_sys_init_vdpreg), %a0
	bsrw	lib_sys_init
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_F400&0x3FFF)<<16)+((vdp_addr_F400>>14)&0x3), VDP_ADDR
	movel	#0, VDP_DATA

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_0000&0x3FFF)<<16)+((vdp_addr_0000>>14)&0x3), VDP_ADDR
	lea	%pc@(nem_FF278A), %a0
	bsrw	lib_nem_vdpdec

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_2000&0x3FFF)<<16)+((vdp_addr_2000>>14)&0x3), VDP_ADDR
	lea	%pc@(nem_FF2CD4), %a0
	bsrw	lib_nem_vdpdec

	lea	%pc@(map_FF3C1E), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_D106&0x3FFF)<<16)+((vdp_addr_D106>>14)&0x3), %d0
	movew	#0x10, %d1
	movew	#3, %d2
	bsrw	lib_map_load0

	lea	%pc@(map_FF3F26), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_D386&0x3FFF)<<16)+((vdp_addr_D386>>14)&0x3), %d0
	movew	#0x0F, %d1
	movew	#0x13, %d2
	bsrw	lib_map_load0

	lea	%pc@(map_FF3CA6), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_E386&0x3FFF)<<16)+((vdp_addr_E386>>14)&0x3), %d0
	movew	#0x0F, %d1
	movew	#0x13, %d2
	bsrw	lib_map_load0

	lea	%pc@(attack_maps), %a0
	movew	#0x8000, %d0
	bsrw	lib_cursor_move
	bsrw	com_yield_subcpu

	moveq	#1, %d0
	bsrw	logo_wait
	bsrw	com_req_subcpu

	movew	SVAR_W_ff0594, %d0
	beqs	1f
	tstb	SVAR_B_ff1212
	beqs	1f 
	lea	SUBCPU_M2_MEMBASE, %a0
	subqw	#1, %d0
	lslw	#2, %d0
	movel	%a0@(0, %d0:w), %d1
	cmpl	SVAR_L_ff0590, %d1
	blss	1f /* if (SVAR_W_ff0594 && SVAR_B_ff1212 && (SUBCPU_M2_MEMBASE[(4*(data_FF0594-1))] < SVAR_L_ff0590)) { */
		movel	SVAR_L_ff0590, %a0@(0, %d0:w)
		bsrw	com_yield_subcpu
		moveq	#2, %d0
		bsrw	logo_wait
		bsrw	com_req_subcpu
	1: /* } */

	bsrw	sub_FF2224

	lea	%pc@(attack_palette), %a0
	lea	(m_mmd_palette).w, %a1
	moveq	#((attack_palette_end-attack_palette)/LONG_SIZE)-1, %d7
	1: /* for (long of palette) { */
		movel	%a0@+, %a1@+

		dbf	%d7, 1b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	bsrw	lib_palfade_init
	bsrw	main_wait_int
	bsrw	lib_palfade_inb

	2: /* while (!(subcpu_cmd & 0xF0)) { */
		lea	%pc@(attack_maps), %a0
		movew	#0x8000, %d0
		bsrw	lib_cursor_move

		lea	%pc@(data_FF2222), %a1
		moveq	#0, %d6
		moveq	#2, %d7
		btst	#0, SUBCPU_CMD_BUFFER+0xF
		beqs	1f /* if (subcpu_cmd & 0x1) { */
			bsrw	lib_cursor_up_w
			bsrw	lib_int_disable
			moveb	#0x90, 0xA01C0A
			bsrw	lib_int_enable
		1: /* } */
		btst	#1, SUBCPU_CMD_BUFFER+0xF
		beqs	1f /* if (subcpu_cmd & 0x2) { */
			bsrw	lib_cursor_down_w
			bsrw	lib_int_disable
			moveb	#0x90, 0xA01C0A
			bsrw	lib_int_enable
		1: /* } */

		movew	%a1@, %d0
		muluw	#10, %d0
		lea	%pc@(attack_maps2), %a0
		lea	%a0@(0, %d0:w), %a0
		movew	#0x8040, %d0
		bsrw	lib_cursor_move
		bsrw	sub_FF2224
		bsrw	sub_FF2340
		bsrw	main_wait_int

		moveb	SUBCPU_CMD_BUFFER+0xF, %d0
		andiw	#0xF0, %d0
		beqs	2b
	/* } */

	moveq	#3, %d0
	bsrw	logo_wait
	bsrw	lib_palfade_outb
	movew	(data_FF2222).l, %d0
	beqs	1f /* if (data_FF2222) { */
		addw	%d0, %d0
		movew	%pc@(data_FF21F2-2, %d0:w), ipx_level_act
		moveb	#1, ipx_level_time
		moveb	#0, ipx_level_future
	1: /* } */

	bset	#E_FLG_DMNA, SUBCPU_E_FLG
	1: /* while (!scom_done) { */
		btst	#SCOM_FLAG_DONE_BIT, SUBCPU_SCOM_FLG
		beqs	1b
	/* } */
	bclr	#E_FLG_DMNA, SUBCPU_E_FLG

	movew	(data_FF2222).l, %d0
	rts
/* ------------------------- */
data_FF21F2:
	.word	0x0000, 0x0001, 0x0002
	.word	0x0100, 0x0101, 0x0102
	.word	0x0200, 0x0201, 0x0202
	.word	0x0300, 0x0301, 0x0302
	.word	0x0400, 0x0401, 0x0402
	.word	0x0500, 0x0501, 0x0502
	.word	0x0600, 0x0601, 0x0602
	.word	0x0700, 0x0701, 0x0702

data_FF2222:
	.word	0
/* ------------------------------------------------------- */
sub_FF2224:
	lea	SUBCPU_M2_MEMBASE, %a0
	lea	VDP_ADDR, %a1
	lea	VDP_DATA, %a2
	movel	#((vdp_incr_0080&0x3FFF)<<16)+((vdp_incr_0080>>14)&0x3), %d5
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C1BC&0x3FFF)<<16)+((vdp_addr_C1BC>>14)&0x3), %d6
	moveq	#24-1, %d7
	1: /* for (i = 24-1; i > 0; i--) { */
		movel	%d6, %a1@
		movel	%a0@+, %d0
		movel	%d0, %d1
		swap	%d1
		andiw	#15, %d1
		addw	%d1, %d1
		movew	%pc@(data_FF22AA, %d1:w), %a2@
		movew	#40, %a2@
		movel	%d0, %d1
		lsrw	#8, %d1
		andil	#255, %d1
		divuw	#10, %d1
		addw	%d1, %d1
		movew	%pc@(data_FF22AA, %d1:w), %a2@
		swap	%d1
		addw	%d1, %d1
		movew	%pc@(data_FF22AA, %d1:w), %a2@
		movew	#41, %a2@
		movel	%d0, %d1
		andil	#255, %d1
		muluw	#100, %d1
		divuw	#60, %d1
		andil	#255, %d1
		divuw	#10, %d1
		addw	%d1, %d1
		movew	%pc@(data_FF22AA, %d1:w), %a2@
		swap	%d1
		addw	%d1, %d1
		movew	%pc@(data_FF22AA, %d1:w), %a2@
		addl	%d5, %d6
		dbf	%d7, 1b
	/* } */

	rts
/* ------------------------ */
data_FF22AA:
	.word	0x30, 0x31
	.word	0x32, 0x33
	.word	0x34, 0x35
	.word	0x36, 0x37
	.word	0x38, 0x39
/* ------------------------------------------------------- */
attack_vint:
	m_vint_open
	beqw	2f /* if (m_vint_req) { */
		bsrw	lib_int_disable
		m_vint_vdpwait
		m_vint_dispon
		m_vint_palload

		bsrw	attack_vint_vramcpy

		m_vint_finish
		addqw	#1, SVAR_W_fffa44_l
	2: /* } */

	m_vint_ret
/* ------------------------------------------------------- */
sub_FF2340:
	tstb	(data_FF23D4).l
	bnes	9f /* if (!data_FF23D4) { */
		subqb	#1, (data_FF23D5).l
		andib	#3, (data_FF23D5).l
		bnes	1f
		cmpiw	#0x1B, (data_FF23D6).l
		beqs	1f /* if (!(--data_FF23D5 & 0x03) && data_FF23D6 != 0x1B) { */
			addqw	#1, (data_FF23D6).l
		1: /* } */

		lea	(data_FF23D8).l, %a0
		movew	(data_FF23D6).l, %d7
		1: /* for (i = data_FF23D6; i > 0; i--) { */
			subiw	#16, %a0@
			bpls	2f /* if ((*a0 - 16) > 0) { */
				movew	#0, %a0@
			2: /* } */

			tstw	%a0@+

			dbf	%d7, 1b
		/* } */

		lea	(data_FF23D8).l, %a0
		moveq	#0, %d0
		movew	#((data_FF23D8_end-data_FF23D8)/WORD_SIZE)-1, %d7
		1: /* for (i = ()-1; i > 0; i--) { */
			addw	%a0@+, %d0

			dbf	%d7, 1b
		/* } */
		bnes	9f /* if (!*data_FF23D8) { */
			moveb	#1, (data_FF23D4).l
		/* } */
	9: /* } */

	rts
/* ------------------------ */
attack_vint_vramcpy:
	movew	#0x8F20, VDP_CTRL

	movel	#(VDP_VRAM_WRITE)+((vdp_addr_F400&0x3FFF)<<16)+((vdp_addr_F400>>14)&0x3), VDP_ADDR
	lea	(data_FF23D8).l, %a0
	lea	VDP_DATA, %a1
	movew	#((data_FF23D8_end-data_FF23D8)/WORD_SIZE)-1, %d7
	1: /* for (word of data_FF23D8) { */
		movew	%a0@+, %a1@

		dbf	%d7, 1b
	/* } */
		
	movew	#0x8F02, VDP_CTRL
	rts
/* ------------------------ */
data_FF23D4:
	.byte	0
data_FF23D5:
	.byte	0
data_FF23D6:
	.word	0
/* ------------------------ */
data_FF23D8:
	.word	0x100, 0x100, 0x100, 0x100
	.word	0x100, 0x100, 0x100, 0x100
	.word	0x100, 0x100, 0x100, 0x100
	.word	0x100, 0x100, 0x100, 0x100
	.word	0x100, 0x100, 0x100, 0x100
	.word	0x100, 0x100, 0x100, 0x100
	.word	0x100, 0x100, 0x100, 0x100
data_FF23D8_end:
/* ------------------------------------------------------- */
	.include "src/common/com_req_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_subflag.s"
/* ------------------------------------------------------- */
	.include "src/common/com_yield_subcpu.s"
/* ------------------------------------------------------- */
lib_sys_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x34
	.byte	0x07, 0x78, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x02
	.byte	0x81, 0x3D, 0x00, 0x02
	.byte	0x01, 0x0A, 0x00, 0x00

attack_palette:
	.incbin	"data/attack/attack_palette.bin"
attack_palette_end:

attack_maps:
	.word	(((attack_maps_end-attack_maps_start)/LONG_SIZE)/2)-1
attack_maps_start:
	.long	data_FF26AA, (VDP_VRAM_WRITE)+((vdp_addr_C12A&0x3FFF)<<16)+((vdp_addr_C12A>>14)&0x3)
	.long	data_FF26B2, (VDP_VRAM_WRITE)+((vdp_addr_C1AA&0x3FFF)<<16)+((vdp_addr_C1AA>>14)&0x3)
	.long	data_FF26BB, (VDP_VRAM_WRITE)+((vdp_addr_C22A&0x3FFF)<<16)+((vdp_addr_C22A>>14)&0x3)
	.long	data_FF26C4, (VDP_VRAM_WRITE)+((vdp_addr_C2AA&0x3FFF)<<16)+((vdp_addr_C2AA>>14)&0x3)
	.long	data_FF26CD, (VDP_VRAM_WRITE)+((vdp_addr_C32A&0x3FFF)<<16)+((vdp_addr_C32A>>14)&0x3)
	.long	data_FF26D6, (VDP_VRAM_WRITE)+((vdp_addr_C3AA&0x3FFF)<<16)+((vdp_addr_C3AA>>14)&0x3)
	.long	data_FF26DF, (VDP_VRAM_WRITE)+((vdp_addr_C42A&0x3FFF)<<16)+((vdp_addr_C42A>>14)&0x3)
	.long	data_FF26E8, (VDP_VRAM_WRITE)+((vdp_addr_C4AA&0x3FFF)<<16)+((vdp_addr_C4AA>>14)&0x3)
	.long	data_FF26F1, (VDP_VRAM_WRITE)+((vdp_addr_C52A&0x3FFF)<<16)+((vdp_addr_C52A>>14)&0x3)
	.long	data_FF26FA, (VDP_VRAM_WRITE)+((vdp_addr_C5AA&0x3FFF)<<16)+((vdp_addr_C5AA>>14)&0x3)
	.long	data_FF2703, (VDP_VRAM_WRITE)+((vdp_addr_C62A&0x3FFF)<<16)+((vdp_addr_C62A>>14)&0x3)
	.long	data_FF270C, (VDP_VRAM_WRITE)+((vdp_addr_C6AA&0x3FFF)<<16)+((vdp_addr_C6AA>>14)&0x3)
	.long	data_FF2715, (VDP_VRAM_WRITE)+((vdp_addr_C72A&0x3FFF)<<16)+((vdp_addr_C72A>>14)&0x3)
	.long	data_FF271E, (VDP_VRAM_WRITE)+((vdp_addr_C7AA&0x3FFF)<<16)+((vdp_addr_C7AA>>14)&0x3)
	.long	data_FF2727, (VDP_VRAM_WRITE)+((vdp_addr_C82A&0x3FFF)<<16)+((vdp_addr_C82A>>14)&0x3)
	.long	data_FF2730, (VDP_VRAM_WRITE)+((vdp_addr_C8AA&0x3FFF)<<16)+((vdp_addr_C8AA>>14)&0x3)
	.long	data_FF2739, (VDP_VRAM_WRITE)+((vdp_addr_C92A&0x3FFF)<<16)+((vdp_addr_C92A>>14)&0x3)
	.long	data_FF2742, (VDP_VRAM_WRITE)+((vdp_addr_C9AA&0x3FFF)<<16)+((vdp_addr_C9AA>>14)&0x3)
	.long	data_FF274B, (VDP_VRAM_WRITE)+((vdp_addr_CA2A&0x3FFF)<<16)+((vdp_addr_CA2A>>14)&0x3)
	.long	data_FF2754, (VDP_VRAM_WRITE)+((vdp_addr_CAAA&0x3FFF)<<16)+((vdp_addr_CAAA>>14)&0x3)
	.long	data_FF275D, (VDP_VRAM_WRITE)+((vdp_addr_CB2A&0x3FFF)<<16)+((vdp_addr_CB2A>>14)&0x3)
	.long	data_FF2766, (VDP_VRAM_WRITE)+((vdp_addr_CBAA&0x3FFF)<<16)+((vdp_addr_CBAA>>14)&0x3)
	.long	data_FF276F, (VDP_VRAM_WRITE)+((vdp_addr_CC2A&0x3FFF)<<16)+((vdp_addr_CC2A>>14)&0x3)
	.long	data_FF2778, (VDP_VRAM_WRITE)+((vdp_addr_CCAA&0x3FFF)<<16)+((vdp_addr_CCAA>>14)&0x3)
	.long	data_FF2781, (VDP_VRAM_WRITE)+((vdp_addr_CD2A&0x3FFF)<<16)+((vdp_addr_CD2A>>14)&0x3)
attack_maps_end:

attack_maps2:
	.word	0
	.long	data_FF26AA, (VDP_VRAM_WRITE)+((vdp_addr_C12A&0x3FFF)<<16)+((vdp_addr_C12A>>14)&0x3)
	.word	0
	.long	data_FF26B2, (VDP_VRAM_WRITE)+((vdp_addr_C1AA&0x3FFF)<<16)+((vdp_addr_C1AA>>14)&0x3)
	.word	0
	.long	data_FF26BB, (VDP_VRAM_WRITE)+((vdp_addr_C22A&0x3FFF)<<16)+((vdp_addr_C22A>>14)&0x3)
	.word	0
	.long	data_FF26C4, (VDP_VRAM_WRITE)+((vdp_addr_C2AA&0x3FFF)<<16)+((vdp_addr_C2AA>>14)&0x3)
	.word	0
	.long	data_FF26CD, (VDP_VRAM_WRITE)+((vdp_addr_C32A&0x3FFF)<<16)+((vdp_addr_C32A>>14)&0x3)
	.word	0
	.long	data_FF26D6, (VDP_VRAM_WRITE)+((vdp_addr_C3AA&0x3FFF)<<16)+((vdp_addr_C3AA>>14)&0x3)
	.word	0
	.long	data_FF26DF, (VDP_VRAM_WRITE)+((vdp_addr_C42A&0x3FFF)<<16)+((vdp_addr_C42A>>14)&0x3)
	.word	0
	.long	data_FF26E8, (VDP_VRAM_WRITE)+((vdp_addr_C4AA&0x3FFF)<<16)+((vdp_addr_C4AA>>14)&0x3)
	.word	0
	.long	data_FF26F1, (VDP_VRAM_WRITE)+((vdp_addr_C52A&0x3FFF)<<16)+((vdp_addr_C52A>>14)&0x3)
	.word	0
	.long	data_FF26FA, (VDP_VRAM_WRITE)+((vdp_addr_C5AA&0x3FFF)<<16)+((vdp_addr_C5AA>>14)&0x3)
	.word	0
	.long	data_FF2703, (VDP_VRAM_WRITE)+((vdp_addr_C62A&0x3FFF)<<16)+((vdp_addr_C62A>>14)&0x3)
	.word	0
	.long	data_FF270C, (VDP_VRAM_WRITE)+((vdp_addr_C6AA&0x3FFF)<<16)+((vdp_addr_C6AA>>14)&0x3)
	.word	0
	.long	data_FF2715, (VDP_VRAM_WRITE)+((vdp_addr_C72A&0x3FFF)<<16)+((vdp_addr_C72A>>14)&0x3)
	.word	0
	.long	data_FF271E, (VDP_VRAM_WRITE)+((vdp_addr_C7AA&0x3FFF)<<16)+((vdp_addr_C7AA>>14)&0x3)
	.word	0
	.long	data_FF2727, (VDP_VRAM_WRITE)+((vdp_addr_C82A&0x3FFF)<<16)+((vdp_addr_C82A>>14)&0x3)
	.word	0
	.long	data_FF2730, (VDP_VRAM_WRITE)+((vdp_addr_C8AA&0x3FFF)<<16)+((vdp_addr_C8AA>>14)&0x3)
	.word	0
	.long	data_FF2739, (VDP_VRAM_WRITE)+((vdp_addr_C92A&0x3FFF)<<16)+((vdp_addr_C92A>>14)&0x3)
	.word	0
	.long	data_FF2742, (VDP_VRAM_WRITE)+((vdp_addr_C9AA&0x3FFF)<<16)+((vdp_addr_C9AA>>14)&0x3)
	.word	0
	.long	data_FF274B, (VDP_VRAM_WRITE)+((vdp_addr_CA2A&0x3FFF)<<16)+((vdp_addr_CA2A>>14)&0x3)
	.word	0
	.long	data_FF2754, (VDP_VRAM_WRITE)+((vdp_addr_CAAA&0x3FFF)<<16)+((vdp_addr_CAAA>>14)&0x3)
	.word	0
	.long	data_FF275D, (VDP_VRAM_WRITE)+((vdp_addr_CB2A&0x3FFF)<<16)+((vdp_addr_CB2A>>14)&0x3)
	.word	0
	.long	data_FF2766, (VDP_VRAM_WRITE)+((vdp_addr_CBAA&0x3FFF)<<16)+((vdp_addr_CBAA>>14)&0x3)
	.word	0
	.long	data_FF276F, (VDP_VRAM_WRITE)+((vdp_addr_CC2A&0x3FFF)<<16)+((vdp_addr_CC2A>>14)&0x3)
	.word	0
	.long	data_FF2778, (VDP_VRAM_WRITE)+((vdp_addr_CCAA&0x3FFF)<<16)+((vdp_addr_CCAA>>14)&0x3)
	.word	0
	.long	data_FF2781, (VDP_VRAM_WRITE)+((vdp_addr_CD2A&0x3FFF)<<16)+((vdp_addr_CD2A>>14)&0x3)

data_FF26AA:
	.incbin	"data/attack/data_ff26aa.bin"
data_FF26B2:
	.incbin	"data/attack/data_ff26b2.bin"
data_FF26BB:
	.incbin	"data/attack/data_ff26bb.bin"
data_FF26C4:
	.incbin	"data/attack/data_ff26c4.bin"
data_FF26CD:
	.incbin	"data/attack/data_ff26cd.bin"
data_FF26D6:
	.incbin	"data/attack/data_ff26d6.bin"
data_FF26DF:
	.incbin	"data/attack/data_ff26df.bin"
data_FF26E8:
	.incbin	"data/attack/data_ff26e8.bin"
data_FF26F1:
	.incbin	"data/attack/data_ff26f1.bin"
data_FF26FA:
	.incbin	"data/attack/data_ff26fa.bin"
data_FF2703:
	.incbin	"data/attack/data_ff2703.bin"
data_FF270C:
	.incbin	"data/attack/data_ff270c.bin"
data_FF2715:
	.incbin	"data/attack/data_ff2715.bin"
data_FF271E:
	.incbin	"data/attack/data_ff271e.bin"
data_FF2727:
	.incbin	"data/attack/data_ff2727.bin"
data_FF2730:
	.incbin	"data/attack/data_ff2730.bin"
data_FF2739:
	.incbin	"data/attack/data_ff2739.bin"
data_FF2742:
	.incbin	"data/attack/data_ff2742.bin"
data_FF274B:
	.incbin	"data/attack/data_ff274b.bin"
data_FF2754:
	.incbin	"data/attack/data_ff2754.bin"
data_FF275D:
	.incbin	"data/attack/data_ff275d.bin"
data_FF2766:
	.incbin	"data/attack/data_ff2766.bin"
data_FF276F:
	.incbin	"data/attack/data_ff276f.bin"
data_FF2778:
	.incbin	"data/attack/data_ff2778.bin"
data_FF2781:
	.incbin	"data/attack/data_ff2781.bin"

nem_FF278A:
	.incbin	"data/attack/nem_ff278a.bin"
nem_FF2CD4:
	.incbin	"data/attack/nem_ff2cd4.bin"
map_FF3C1E:
	.incbin	"data/attack/map_ff3c1e.bin"
map_FF3CA6:
	.incbin	"data/attack/map_ff3ca6.bin"
map_FF3F26:
	.incbin	"data/attack/map_ff3f26.bin"
