/* ------------------------------------------------------- */
/* logo_s - the SubCPU portion of the SEGA logo screen     */
/*  this component simply manages the execution of the     */
/*  PCM soundbyte                                          */
/* ------------------------------------------------------- */
	.include "src/inc/s_subreg.i"

	.include "src/inc/s_bios.i"

	.include "src/inc/pcm.i"

	.text
/* ------------------------------------------------------- */
	.equ	BUF_F800,	0xF800
	.equ	BUF_F800_END,	0x10000
logo_s:
	moveb	#0,                     (SUBCPU_MEMMODE_W).w
	movel	#logo_timer_int,        BIOS_TIMERINT
	bset	#SCOM_FLAG_BUSY_BIT,    (SUBCPU_SCOM_FLG_W).w
	bclr	#INT_MASK_GFX_DONE_BIT, (SUBCPU_INT_MASK_W).w
	bclr	#INT_MASK_TIMER_BIT,    (SUBCPU_INT_MASK_W).w
	moveb	#CDC_SUBCPU_READ,       (SUBCPU_CDCMODE_W).w

	lea	BUF_F800, %a0
	movew	#((BUF_F800_END-BUF_F800)/4)-1, %d7
	1: /* for (long of BUF_F800) { */
		movel	#0, %a0@+
		dbf	%d7, 1b
	/* } */

	moveb	#0xff,               (SUBCPU_TIMER_VAL_W).w
	bset	#INT_MASK_TIMER_BIT, (SUBCPU_INT_MASK_W).w

	bclr	#SCOM_FLAG_BUSY_BIT, (SUBCPU_SCOM_FLG_W).w
	2: /* while () { */
		movew	(SUBCPU_CMD_BUFFER_W+2).w, %d0
		beqs	1f /* if () { */
			bsrw	sub_mmd_wait
			movew	#0xb0, PCM_VAR_22
		1: /* } */

		btst	#MCOM_FLAG_DONE_BIT, (SUBCPU_E_FLG_W).w
		beqs	2b
	/* } */

	bset	#SCOM_FLAG_DONE_BIT, (SUBCPU_SCOM_FLG_W).w
	1: /* while () { */
		btst	#MCOM_FLAG_DONE_BIT, (SUBCPU_E_FLG_W).w
		bnes	1b
	/* } */
	bclr	#SCOM_FLAG_DONE_BIT, (SUBCPU_SCOM_FLG_W).w

	rts
/* ------------------------------------------------------- */
	.include "src/common/sub_mmd_wait.s"
/* ------------------------------------------------------- */
logo_timer_int:
	bchg	#0, data_100aa
	beqs	1f /* if (data_100aa & 0x1) { */
		moveml	%d0-%fp, %sp@-
		jsr	PCM_TIMER_ENTRY
		moveml	%sp@+, %d0-%fp
	1: /* } */

    rte
/* ------------------------ */
data_100aa:
	.word	0
/* ------------------------------------------------------- */
