/* ------------------------------------------------------- */
/* come - coming soon screen                               */
/* ------------------------------------------------------- */
	.include "src/inc/vdp.i"
	.include "src/inc/z80.i"

	.include "src/inc/m_vector.i"

	.include "src/inc/m_map.i"
	.include "src/inc/obj_eng.i"
	.include "src/inc/subcall.i"

	.include "src/common/m_vint_bits.s"
	.include "src/common/m_obj_eng.s"

vdp_addr_0000	= 0x0000
vdp_addr_A400	= 0xA400
vdp_addr_C000	= 0xC000
vdp_addr_C040	= 0xC040
vdp_addr_C080	= 0xC080

vdp_vsaddr_0000	= 0x0000

	.section .text.mmd_head
/* ------------------------------------------------------- */
	.byte	0
	.even
	.long	come
	.word	(mmd_warp_size/4)-1
	.long	come
	.long	0
	.long	0
	.align	256, 0
/* ------------------------------------------------------- */
    .section .text.m_lib
/* ------------------------------------------------------- */
	.equ	TIMER_LENGTH, 0x3ff
come:
	movel	#come_vint, M_VECTOR_VINT
	bsrw	com_yield_subcpu

	lea	mmd_ram_arena_l, %a0
	movew	#mmd_ram_arena_size_l-1, %d7
	1: /* for (long of mmd_ram_arena) { */
		movel	#0, %a0@+

		dbf	%d7, 1b
	/* } */

	lea	%pc@(come_init_vdpreg), %a0
	bsrw	lib_sys_init
	movel	#(VDP_VSRAM_WRITE)+((vdp_vsaddr_0000&0x3FFF)<<16)+((vdp_vsaddr_0000>>14)&0x3), VDP_ADDR
	movel	#0, VDP_DATA
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_0000&0x3FFF)<<16)+((vdp_addr_0000>>14)&0x3), VDP_ADDR
	lea	%pc@(come_nemtiles), %a0
	bsrw	lib_nem_vdpdec
	lea	%pc@(come_map0), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C000&0x3FFF)<<16)+((vdp_addr_C000>>14)&0x3), %d0
	movew	#0x1f, %d1
	movew	#0x1b, %d2
	bsrw	lib_map_load1
	lea	%pc@(come_map1), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C040&0x3FFF)<<16)+((vdp_addr_C040>>14)&0x3), %d0
	movew	#0x1f, %d1
	movew	#0x1b, %d2
	bsrw	lib_map_load1
	lea	%pc@(come_map2), %a1
	movel	#(VDP_VRAM_WRITE)+((vdp_addr_C080&0x3FFF)<<16)+((vdp_addr_C080>>14)&0x3), %d0
	movew	#0x1f, %d1
	movew	#0x1b, %d2
	bsrw	lib_map_load1
	movew	#TIMER_LENGTH, m_timer_l

	lea	%pc@(come_palette), %a0
	lea	m_mmd_palette, %a1
	moveq	#((come_palette_end-come_palette)/LONG_SIZE)-1, %d7
	1: /* for (long of come_palette) { */
		movel	%a0@+, %a1@+

		dbf	%d7, 1b
	/* } */

	bset	#m_vint_palfade_bit, m_vint_call_mask
	bsrw	main_wait_int

	1: /* while (!ctrl_start_a_b_c && m_timer) { */
		bsrw	main_wait_int

		moveb	CMD_BUFFER_CTRL, %d0
		andiw	#0b11110000, %d0
		bnes	1f
		tstw	m_timer_l
		bnew	1b
	1: /* } */

	moveq	#SUBCALL_FADERS_FADE, %d0
	bsrw	subcall_wait
	bsrw	lib_palfade_outb
	rts
/* ------------------------------------------------------- */
come_vint:
	m_vint_open
	beqw	9f /* if () { */
		m_vint_dispon
		bsrw	lib_int_disable
		m_vint_vdpwait
		m_vint_palload

		movel	#(VDP_VRAM_WRITE)+((vdp_addr_A400&0x3FFF)<<16)+((vdp_addr_A400>>14)&0x3), VDP_ADDR
		movew	(come_msg_pos).l, VDP_DATA

		m_vint_finish

		tstw	m_timer_l
		beqs	1f /* if (m_timer) { */
			subqw	#1, (come_msg_pos).l    /* m_vint_tick w/ scroll */
			subqw	#1, m_timer_l
		1: /* } */

		addqw	#1, SVAR_W_fffa44_l
	9: /* } */

	m_vint_ret
/* ------------- */
come_msg_pos:
	.word	0x140
	.word	0
/* ------------------------------------------------------- */
	.include "src/common/com_yield_subcpu.s"
/* ------------------------------------------------------- */
	.include "src/common/com_req_subcpu.s"
/* ------------------------------------------------------- */
come_init_vdpreg:
	.byte	0x04, 0x34, 0x30, 0x00
	.byte	0x07, 0x50, 0x00, 0x00
	.byte	0x00, 0x00, 0x00, 0x00
	.byte	0x81, 0x29, 0x00, 0x02
	.byte	0x03, 0x00, 0x00
    .even
/* ------------------------------------------------------- */
come_palette:   /* FF21B8-FF21D8 */
	.incbin	"data/come/come_palette.bin"
come_palette_end:
/* ------------------------------------------------------- */
come_nemtiles:  /* FF21D8-FF26DC */
	.incbin	"data/come/come_nemtiles.bin"
/* ------------------------------------------------------- */
come_map0:      /* FF26DC-FF2DDC */
	.incbin	"data/come/come_map0.bin"
/* ------------------------------------------------------- */
come_map1:      /* FF2DDC-FF34DC */
	.incbin	"data/come/come_map1.bin"
/* ------------------------------------------------------- */
come_map2:      /* FF34DC-FF3BDC */
	.incbin	"data/come/come_map2.bin"
/* ------------------------------------------------------- */
