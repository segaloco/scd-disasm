TARGET		=	m68k-unknown-elf-
AS		=	$(TARGET)as
LD		=	$(TARGET)ld
LDFLAGS		=	-nostdlib

ISO		=	scd.iso

CDHEAD		=	cdhead.bin

CDHEAD_OBJS	=	src/cdhead/sys_head.o \
			src/cdhead/disc_head.o \
			src/cdhead/ip_start.o \
			src/cdhead/ip_main.o \
			src/common/subcall_wait.o \
			src/common/subcpu_int.o \
			src/cdhead/sp_os.o \
			src/cdhead/sp.o \
			src/common/sub_wait_main.o \
			src/common/sub_wait_dmna.o \
			src/common/sub_halt.o

CDHEAD_LDFLAGS	=	$(LDFLAGS) -T src/cdhead/link.ld

IPX		=	iso/IPX___.MMD

IPX_OBJS	=	src/ipx.o \
			src/common/subcpu_int.o \
			src/common/subcall_wait.o

IPX_LDFLAGS	=	$(LDFLAGS) -T src/ipx.ld

PCM		=	iso/PCM000.BIN

PCM_OBJS	=	src/pcm000.o

PCM_LDFLAGS	=	$(LDFLAGS) -T src/pcm000.ld

LOGO_M		=	iso/LOGO_M.MMD

LOGO_M_OBJS	=	src/logo_m.o \
			src/common/lib_palfade.o \
			src/common/lib_nem_dec.o \
			src/common/lib_eni_dec.o \
			src/common/lib_sys_init.o \
			src/common/lib_map_load.o \
			src/common/lib_memcpy.o \
			src/common/lib_int_updown.o \
			src/common/lib_joypad_read.o \
			src/common/main_wait_int.o \
			src/common/subcall_wait.o \
			src/common/logo_wait.o
#			src/common/com_yield_subcpu.o \
#			src/common/com_subflag.o \

LOGO_M_LDFLAGS	=	$(LDFLAGS) -T src/logo_m.ld

LOGO_S		=	iso/LOGO_S.BIN

LOGO_S_OBJS	=	src/logo_s.o
#			src/common/sub_mmd_wait.o

LOGO_S_LDFLAGS	=	$(LDFLAGS) -T src/logo_s.ld

TITLE		=	iso/TITLE_.MMD

TITLE_OBJS	=	src/title.o \
			src/common/lib_cursor.o \
			src/common/lib_palfade.o \
			src/common/lib_memset.o \
			src/common/lib_map_load.o \
			src/common/lib_sys_init.o \
			src/common/lib_nem_dec.o \
			src/common/lib_eni_dec.o \
			src/common/lib_int_updown.o \
			src/common/lib_joypad_read.o \
			src/common/main_wait_int.o \
			src/common/subcall_wait.o \
			src/common/logo_wait.o
#			src/common/com_yield_subcpu.o \
#			src/common/com_req_subcpu.o \
#			src/common/com_subflag.o \

TITLE_LDFLAGS	=	$(LDFLAGS) -T src/title.ld

OPEN_M		=	iso/OPEN_M.MMD

OPEN_M_OBJS	=	src/open_m.o
#			src/common/lib_memset.o \
#			src/common/lib_int_updown.o \
#			src/common/lib_joypad_read.o

OPEN_M_LDFLAGS	=	$(LDFLAGS) -T src/open_m.ld

OPEN_S		=	iso/OPEN_S.BIN

OPEN_S_OBJS	=	src/open_s.o

OPEN_S_LDFLAGS	=	$(LDFLAGS) -T src/open_s.ld

SSEL		=	iso/SSEL__.MMD

SSEL_OBJS	=	src/ssel.o \
			src/common/lib_nem_dec.o \
			src/common/lib_eni_dec.o \
			src/common/lib_int_updown.o \
			src/common/lib_joypad_read.o \
			src/common/main_wait_int.o \
			src/common/subcall_wait.o \
			src/common/logo_wait.o
#			src/common/com_yield_subcpu.o \
#			src/common/com_req_subcpu.o \
#			src/common/com_subflag.o \
#			src/common/lib_palfade.o \
#			src/common/lib_cursor.o \
#			src/common/lib_cursor_move.o \
#			src/common/lib_sys_init.o

SSEL_LDFLAGS	=	$(LDFLAGS) -T src/ssel.ld

WARP		=	iso/WARP__.MMD

WARP_OBJS	=	src/warp.o \
			src/common/lib_rand.o \
			src/common/lib_sys_init.o \
			src/common/lib_int_updown.o \
			src/common/lib_joypad_read.o \
			src/common/main_wait_int.o \
			src/common/subcall_wait.o \
			src/common/logo_wait.o

WARP_LDFLAGS	=	$(LDFLAGS) -T src/warp.ld

COME		=	iso/COME__.MMD

COME_OBJS	=	src/come.o \
			src/common/lib_map_load.o \
			src/common/lib_palfade.o \
			src/common/lib_sys_init.o \
			src/common/lib_nem_dec.o \
			src/common/lib_eni_dec.o \
			src/common/lib_int_updown.o \
			src/common/lib_joypad_read.o \
			src/common/main_wait_int.o \
			src/common/subcall_wait.o \
			src/common/logo_wait.o
#			src/common/com_yield_subcpu.o \
#			src/common/com_req_subcpu.o \

COME_LDFLAGS	=	$(LDFLAGS) -T src/come.ld

ATTACK_M	=	iso/ATTACK.MMD

ATTACK_M_OBJS	=	src/attack_m.o \
			src/common/lib_cursor.o \
			src/common/lib_map_load.o \
			src/common/lib_cursor_move.o \
			src/common/lib_palfade.o \
			src/common/lib_memset.o \
			src/common/lib_sys_init.o \
			src/common/lib_nem_dec.o \
			src/common/lib_eni_dec.o \
			src/common/lib_int_updown.o \
			src/common/lib_joypad_read.o \
			src/common/main_wait_int.o \
			src/common/subcall_wait.o \
			src/common/logo_wait.o
#			src/common/com_req_subcpu.o \
#			src/common/com_subflag.o \
#			src/common/com_yield_subcpu.o

ATTACK_M_LDFLAGS	= $(LDFLAGS) -T src/attack_m.ld

ATTACK_S	=	iso/ATTACK.BIN

ATTACK_S_OBJS	=	src/attack_s.o
#			src/common/sub_mmd_wait.o

ATTACK_S_LDFLAGS	= $(LDFLAGS) -T src/attack_s.ld

R11A		=	iso/R11A__.MMD

R11A_OBJS	=	src/levels/r1/act1/pres.o

R11A_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act1/pres.ld

R11B		=	iso/R11B__.MMD

R11B_OBJS	=	src/levels/r1/act1/past.o

R11B_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act1/past.ld

R11C		=	iso/R11C__.MMD

R11C_OBJS	=	src/levels/r1/act1/f_good.o

R11C_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act1/f_good.ld

R11D		=	iso/R11D__.MMD

R11D_OBJS	=	src/levels/r1/act1/f_bad.o

R11D_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act1/f_bad.ld

R12A		=	iso/R12A__.MMD

R12A_OBJS	=	src/levels/r1/act2/pres.o

R12A_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act2/pres.ld

R12B		=	iso/R12B__.MMD

R12B_OBJS	=	src/levels/r1/act2/past.o

R12B_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act2/past.ld

R12C		=	iso/R12C__.MMD

R12C_OBJS	=	src/levels/r1/act2/f_good.o

R12C_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act2/f_good.ld

R12D		=	iso/R12D__.MMD

R12D_OBJS	=	src/levels/r1/act2/f_bad.o

R12D_LDFLAGS	=	$(LDFLAGS) -T src/levels/r1/act2/f_bad.ld

BINS		=	$(CDHEAD) \
			$(IPX) \
			$(PCM) \
			$(LOGO_M) \
			$(LOGO_S) \
			$(TITLE) \
			$(OPEN_M) \
			$(OPEN_S) \
			$(SSEL) \
			$(WARP) \
			$(COME) \
			$(ATTACK_M) \
			$(ATTACK_S) \
			$(R11A) \
			$(R11B) \
			$(R11C) \
			$(R11D) \
			$(R12A) \
			$(R12B) \
			$(R12C) \
			$(R12D)

OBJS		=	$(CDHEAD_OBJS) \
			$(IPX_OBJS) \
			$(PCM_OBJS) \
			$(LOGO_M_OBJS) \
			$(LOGO_S_OBJS) \
			$(TITLE_OBJS) \
			$(OPEN_M_OBJS) \
			$(OPEN_S_OBJS) \
			$(SSEL_OBJS) \
			$(WARP_OBJS) \
			$(COME_OBJS) \
			$(ATTACK_M_OBJS) \
			$(ATTACK_S_OBJS) \
			$(R11A_OBJS) \
			$(R11B_OBJS) \
			$(R11C_OBJS) \
			$(R11D_OBJS) \
			$(R12A_OBJS) \
			$(R12B_OBJS) \
			$(R12C_OBJS) \
			$(R12D_OBJS)

CDFILES		=	data/cdfiles/ABS.TXT \
			data/cdfiles/BIB.TXT \
			data/cdfiles/CPY.TXT \
			data/cdfiles/OPN.STM

all: $(BINS) $(CDFILES)
	cp $(CDFILES) iso
	mkisofs -o $(ISO) iso/
	dd if=$(CDHEAD) of=$(ISO)

check: $(BINS)
	cmp $(CDHEAD) raw/$(CDHEAD)
	cmp $(IPX)    raw/$(IPX)
	cmp $(LOGO_M) raw/$(LOGO_M)
	cmp $(LOGO_S) raw/$(LOGO_S)
	cmp $(TITLE)  raw/$(TITLE)
	cmp $(PCM)    raw/$(PCM)
	cmp $(OPEN_M) raw/$(OPEN_M)
	cmp $(OPEN_S) raw/$(OPEN_S)
	cmp $(SSEL)   raw/$(SSEL)
	cmp $(WARP)   raw/$(WARP)
	cmp $(COME)   raw/$(COME)
	cmp $(ATTACK_M) raw/$(ATTACK_M)
	cmp $(ATTACK_S) raw/$(ATTACK_S)
	cmp $(R11A)   raw/$(R11A)
	cmp $(R11B)   raw/$(R11B)
	cmp $(R11C)   raw/$(R11C)
	cmp $(R11D)   raw/$(R11D)
	cmp $(R12A)   raw/$(R12A)
	cmp $(R12B)   raw/$(R12B)
	cmp $(R12C)   raw/$(R12C)
	cmp $(R12D)   raw/$(R12D)

cdhead.bin: $(CDHEAD_OBJS)
	$(LD) $(CDHEAD_LDFLAGS) -o $(CDHEAD) $(CDHEAD_OBJS)

iso/IPX___.MMD: $(IPX_OBJS) iso
	mkdir -p iso
	$(LD) $(IPX_LDFLAGS) -o $(IPX) $(IPX_OBJS)

iso/PCM000.BIN: $(PCM_OBJS) iso
	$(LD) $(PCM_LDFLAGS) -o $(PCM) $(PCM_OBJS)

iso/LOGO_M.MMD: $(LOGO_M_OBJS) iso
	$(LD) $(LOGO_M_LDFLAGS) -o $(LOGO_M) $(LOGO_M_OBJS)

iso/LOGO_S.BIN: $(LOGO_S_OBJS) iso
	$(LD) $(LOGO_S_LDFLAGS) -o $(LOGO_S) $(LOGO_S_OBJS)

iso/TITLE_.MMD: $(TITLE_OBJS) iso
	$(LD) $(TITLE_LDFLAGS) -o $(TITLE) $(TITLE_OBJS)

iso/OPEN_M.MMD: $(OPEN_M_OBJS) iso
	$(LD) $(OPEN_M_LDFLAGS) -o $(OPEN_M) $(OPEN_M_OBJS)

iso/OPEN_S.BIN: $(OPEN_S_OBJS) iso
	$(LD) $(OPEN_S_LDFLAGS) -o $(OPEN_S) $(OPEN_S_OBJS)

iso/SSEL__.MMD: $(SSEL_OBJS) iso
	$(LD) $(SSEL_LDFLAGS) -o $(SSEL) $(SSEL_OBJS)

iso/WARP__.MMD: $(WARP_OBJS) iso
	$(LD) $(WARP_LDFLAGS) -o $(WARP) $(WARP_OBJS)

iso/COME__.MMD: $(COME_OBJS) iso
	$(LD) $(COME_LDFLAGS) -o $(COME) $(COME_OBJS)

iso/ATTACK.MMD: $(ATTACK_M_OBJS) iso
	$(LD) $(ATTACK_M_LDFLAGS) -o $(ATTACK_M) $(ATTACK_M_OBJS)

iso/ATTACK.BIN: $(ATTACK_S_OBJS) iso
	$(LD) $(ATTACK_S_LDFLAGS) -o $(ATTACK_S) $(ATTACK_S_OBJS)

iso/R11A__.MMD: $(R11A_OBJS) iso
	$(LD) $(R11A_LDFLAGS) -o $(R11A) $(R11A_OBJS)

iso/R11B__.MMD: $(R11B_OBJS) iso
	$(LD) $(R11B_LDFLAGS) -o $(R11B) $(R11B_OBJS)

iso/R11C__.MMD: $(R11C_OBJS) iso
	$(LD) $(R11C_LDFLAGS) -o $(R11C) $(R11C_OBJS)

iso/R11D__.MMD: $(R11D_OBJS) iso
	$(LD) $(R11D_LDFLAGS) -o $(R11D) $(R11D_OBJS)

iso/R12A__.MMD: $(R12A_OBJS) iso
	$(LD) $(R12A_LDFLAGS) -o $(R12A) $(R12A_OBJS)

iso/R12B__.MMD: $(R12B_OBJS) iso
	$(LD) $(R12B_LDFLAGS) -o $(R12B) $(R12B_OBJS)

iso/R12C__.MMD: $(R12C_OBJS) iso
	$(LD) $(R12C_LDFLAGS) -o $(R12C) $(R12C_OBJS)

iso/R12D__.MMD: $(R12D_OBJS) iso
	$(LD) $(R12D_LDFLAGS) -o $(R12D) $(R12D_OBJS)

iso:
	mkdir -p iso

clean:
	rm -rf $(ISO) iso/ $(OBJS) $(CDHEAD) $(ISO_COOKED)